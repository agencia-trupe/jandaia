var abreSecao = function(target){

	target = target.replace('#','');

	var secao = $("[name='"+target+"']");
	
	if(secao.length){
		$('body, html').animate({
			'scrollTop' : secao.offset().top
		}, 400);
	}else{
		window.location.hash = '';
		abreSecao('home');
	}
}

var abreMensagem = function(texto){
	var mensagem = $("<div class='mensagem-outer'><div class='mensagem-caixa'><div class='mensagem-texto'>"+texto+"</div><a href='#' class='mensagem-ok' title='Fechar aviso'>OK</a></div></div>");
	$('body').append(mensagem);

	$('.mensagem-outer').on('click', 'a.mensagem-ok', function(e){
		e.preventDefault();
		$('.mensagem-outer').remove();
		abreSecao('participe');
	});
}


function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');

    if(cpf == '') return false;

    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
        return false;

    // Valida 1o digito
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return false;

    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;

    return true;
}

var semCPF = 0;

$('document').ready( function(){
	
	var altura_minina = jQuery(window).height();
		
	$(".secao").css("min-height", altura_minina + 30);
	
	$('nav, .detalhe-tema').on('click', 'a', function(e){
		e.preventDefault();

		if(!$(this).hasClass('mn-regulamento')){
			abreSecao($(this).attr('href'));
			window.location.hash = $(this).attr('href');
		}else{
			$.get(BASE+"/regulamento", function(resposta){
				$.fancybox({
					content : resposta
				});
			});
		}
	});

	if(window.location.hash){
		abreSecao(window.location.hash);
	}

	$('#inscricoes-fechadas p.on').on('click', 'a', function(e){
		e.preventDefault();
		abreSecao('premios');
	});

	$('#input-telefone').mask("(99) 9999-9999?9");
	$('#input-cep').mask("99999-999");
	$('#input-cpf').mask("999.999.999-99");
	$('#input-cpfresponsavel').mask("999.999.999-99");
	$('#input-data').mask("99/99/9999");

	$('#textarea-frase').on('keyup paste', function(){
		$('#contador').find('span').html(140 - $('#textarea-frase').val().length);
		if($('#textarea-frase').val().length > 140){
			$('#textarea-frase').val($('#textarea-frase').val().substring(0,140));
		}
	});

	$('#abrir-responsavel').on('click', 'a', function(e){
		e.preventDefault();
		if($('.responsavel.escondido').length){
			$('.responsavel.escondido').removeClass('escondido');
			$('.linha-form.cpf').fadeOut();
			semCPF = 1;
		}
	});

	$('#fechar-responsavel').on('click', 'a', function(e){
		e.preventDefault();		
		$('.responsavel').addClass('escondido');
		$('.linha-form.cpf').fadeIn();
		semCPF = 0;		
	});

	$('.participe').on('submit', 'form', function(e){

		e.preventDefault();

		var erros_validacao = false;

		// Verifica campos obrigatórios
		$("[required]").each( function(){
			if($(this).val() == ''){
				abreMensagem($(this).attr('data-required-msg'));
				erros_validacao = true;
				return false;
			}
		});

		if(erros_validacao) return false;

		// Verifica se utilizou hashtag
		if($('#textarea-frase').val().indexOf("#todentro") === -1){
			abreMensagem($('#textarea-frase').attr('data-required-msg'));
			return false;
		}

		// Verifica e valida CPFs
		// Se for menor: cpf do responśavel, nome do responśavel e data de nascimento são obrigatórios
		// Se for maior: cpf obrigatório
		if(semCPF === 1){
			if($('#input-nomecompletoresponsavel').val() == ''){
				abreMensagem("Informe o Nome Completo do responsável!");
				return false;
			}
			if($('#input-cpfresponsavel').val() == ''){
				abreMensagem("Informe o CPF do responsável!");
				return false;
			}
			if(!validarCPF($('#input-cpfresponsavel').val())){
				abreMensagem("Informe um CPF válido para o responsável!");
				return false;
			}
			if($('#input-data').val() == ''){
				abreMensagem("Informe sua data de nascimento!");
				return false;
			}
		}else{
			if($('#input-cpf').val() == ''){
				abreMensagem("Informe o seu CPF!");
				return false;
			}else{
				if(!validarCPF($('#input-cpf').val())){
					abreMensagem("Informe um CPF válido!");
					return false;
				}
			}
		}

		// Verifica Aceite dos termos
		if(!$("#check-aceite").is(':checked')){
			abreMensagem($("#check-aceite").attr('data-required-msg'));
			return false;
		}

		$('#inscricao').fadeOut();
		abreSecao('participe');
		var loader = $("<div class='loader'><div class='progress'><div></div></div></div>");
		$('.secao.participe .pure-u-13-24').append(loader);
		
		// Envia os dados
		// Se ok mostrar mensagem
		// Se falhou voltar o formulário
		$.post(BASE+'/inscrever', {
			frase : $('#textarea-frase').val(),
			nome : $('#input-nomecompleto').val(),
			email : $('#input-email').val(),
			telefone : $('#input-telefone').val(),
			cep : $('#input-cep').val(),
			semcpf : semCPF,
			cpf : $('#input-cpf').val(),
			cpfresponsavel : $('#input-cpfresponsavel').val(),
			nomeresponsavel : $('#input-nomecompletoresponsavel').val(),
			data : $('#input-data').val(),
			aceite : $('#check-aceite').is(':checked') ? 1 : 0,
			newsletter : $('#check-news:checked').length,
		}, function(retorno){
			if(retorno.erro){
				abreMensagem(retorno.mensagem);
				$('#inscricao').fadeIn();
				$('.loader').remove();
			}else{
				var confirmacao = $("<div class='resposta-inscricao'>Sua participação foi enviada com sucesso! Você receberá uma cópia da sua inscrição em seu e-mail. Boa sorte!</div>");
				$('.loader').remove();
				$('.secao.participe .pure-u-13-24').append(confirmacao);
			}
		});
	});


});
