var formataDataExibicao = function(data){
	var spl = data.split('-');
	var meses = {
	    '01' : 'jan',
	    '02' : 'fev',
	    '03' : 'mar',
	    '04' : 'abr',
	    '05' : 'mai',
	    '06' : 'jun',
	    '07' : 'jul',
	    '08' : 'ago',
	    '09' : 'set',
	    '10' : 'out',
	    '11' : 'nov',
	    '12' : 'dez'
	};
	return spl[2]+' '+meses[spl[1]]+' '+spl[0];
};

var atualizaNumComentariosFB = function(){
	setTimeout( function(){
		$('.num_comentarios').each( function(){
			var parent = $(this);
			var inject = $(this).find('span.fb_comments_count');
			var valor = parseInt(inject.html());
			if(!inject.hasClass('contabilizado')){
				if(valor == 0){
					parent.html("Seja o primeiro a comentar!").addClass('contabilizado');
				}else if(valor == 1){
					parent.html("1 pessoa já comentou! Comente!").addClass('contabilizado');
				}
			}
		});
	}, 1000);
}

$('document').ready( function(){
	
	$('#slides').cycle({
		before : function(currSlideElement, nextSlideElement, options, forwardFlag){
			var ref = $(nextSlideElement).attr('data-id');
			$('#carrossel .centro .pure-g.show').removeClass('show');
			$('#carrossel-'+ref).addClass('show');
		}
	});

	$('#mostrar-menu').click( function(e){
		e.preventDefault();
		$('nav ul').toggleClass('shown');
	});

	$('.alterar-tipo').click( function(e){
		e.preventDefault();
		$(this).parent().parent().find('.opcoes').toggleClass('escondidas');
	});

	$('#mostrar-linhas').click( function(e){
		e.preventDefault();
		$('.lista-linhas').toggleClass('amostra');
	});

	$('#ver-mais-posts').click( function(e){
		e.preventDefault();

		var url = $(this).attr('href');
		var pagina = $(this).attr('data-pagina');
		var cat = $('#ajCat').val();

		var destino = url;
		
		if(cat != '')
			destino += '/'+cat;

		destino += '?page=';
		destino += (pagina);

		$.get(destino, function(retorno){
			
			if(retorno.paginar){
				$('#ver-mais-posts').attr('data-pagina', retorno.proximaPagina);			
			}else{
				$('#ver-mais-posts').hide();
			}

			$('#scroller').remove();

			var html = "<div id='scroller' class='pure-u-1'></div>";
			for (var i = 0; i < retorno.posts.length; i++) {
				html += "<a href='fique-por-dentro/ler/"+retorno.posts[i].slug+"' class='pure-u-1-3 post-thumb' title='"+retorno.posts[i].titulo+"'>";
					html += "<div class='pad'>";
						html += "<div class='imagem'>";
							html += "<img src='assets/images/consumidor/blog/thumbs/"+retorno.posts[i].imagem+"' alt='"+retorno.posts[i].titulo+"'>";
							html += "<div class='data'>"+formataDataExibicao(retorno.posts[i].data)+"</div>";
						html += "</div>";
						html += "<div class='titulo'>";
							html += retorno.posts[i].titulo;
						html += "</div>";
						html += "<div class='num_comentarios'>";
							html +="<fb:comments-count href='"+BASE+"/fique-por-dentro/ler/"+retorno.posts[i].slug+"'></fb:comments-count> pessoas já comentaram. Comente!";
						html += "</div>";
						html += "<div class='compartilhe'>";
							html += "Que tal compartilhar?";
						html += "</div>";
					html += "</div>";
				html += "</a>";
			};
			
			$('.posts .grid').append(html);
			FB.XFBML.parse();
			atualizaNumComentariosFB();
			$('body, html').animate({
				'scrollTop' : $('#scroller').offset().top
			}, 200);

		});
	});

	if($('.num_comentarios').length){
		atualizaNumComentariosFB();
	};

	$('.chamada-temas .box-lista ul li a').click( function(e){
		e.preventDefault();
		$('.chamada-temas .box-lista ul li.escondido').removeClass('escondido');
		$(this).parent().addClass('escondido');
		var slug = $(this).attr('data-slug');
		$('.chamada-temas .tema-thumb').addClass('escondido');
		$('.chamada-temas .tema-'+slug).removeClass('escondido');
	});

	$('#shadow-promo-1d').click( function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		$.fancybox({
			'type' : 'iframe',
			'href' : url,
			'width' : 600,
			'height' : 415
		});
	});

	
	$('#sel-estado').change( function(){
		var id_estado = $(this).val();
	    if(id_estado){
	    	$.getJSON('ajax/pegarCidades/'+id_estado, function(resposta){
	    		$('#sel-cidade').html("<option value=''>Carregando Cidades...</option>");

				var itens = "<option value=''>Todas as Cidades do Estado</option>";
		      	resposta.map( function(a, b){
		        	itens += "<option value='"+a.id+"'>"+a.nome+"</option>";
		      	});
		      	$('#sel-cidade').html(itens);
		    });
	    }
	});

	$('#sel-estado').each( function(i, e){
		if($(this).val() != ''){
			$.getJSON('ajax/pegarCidades/'+$('#sel-estado').val(), function(resposta){
	    		$('#sel-cidade').html("<option value=''>Carregando Cidades...</option>");

				var itens = "<option value=''>Todas as Cidades do Estado</option>";
		      	resposta.map( function(a, b){
		        	itens += "<option value='"+a.id+"'>"+a.nome+"</option>";
		      	});
		      	$('#sel-cidade').html(itens);
		      	if( $('#sel-cidade').attr('data-cidade') )
        			$('#sel-cidade').val($('#sel-cidade').attr('data-cidade'))
		    });
		}
	});

	$('#input-cep').mask("99999-999");

});