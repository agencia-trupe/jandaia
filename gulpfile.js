var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    less = require('gulp-less'),
    cssmin = require('gulp-cssmin');

gulp.task('less', function () {
  return gulp.src('./public/assets/css/representantes/*.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(cssmin())
    .pipe(gulp.dest('./public/assets/css/representantes/'));
});

gulp.task('watch', function() {
    return gulp.watch('./public/assets/css/representantes/*.less', ['less']);
});

gulp.task('default', ['less', 'watch']);
