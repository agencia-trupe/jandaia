<?php

// Namespace
namespace Concurso;

// Core
use View, Input, Response, Tools, Mail;

// Models
use ParticipacoesConcurso, Cadastro;

// BaseClass
use BaseController;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.concurso';

    public function index()
	{
		date_default_timezone_set('America/Sao_Paulo');

		$this->layout->with('seo', array(
			'title' => 'Concurso Cultural #TôDentro de um mundo melhor',
			'description' => 'E aí, tá a fim de GANHAR PRÊMIOS ajudando a escrever um Mundo Melhor para todos? Crie uma frase e participe! Concurso Cultural #TôDentro de um Mundo Melhor – Jandaia',
			'image' => 'facebook-todentro-compartilhe.jpg'
		));

		$datetime_atual = date('Y-m-d H:i:s');
		$etapas = array(
			'1' => array('inicio' => '2013-12-12 11:00:00', 'fim' => '2014-01-10 23:59:00'),
			'2' => array('inicio' => '2014-01-13 11:00:00', 'fim' => '2014-01-31 23:59:00'),
			'3' => array('inicio' => '2014-02-03 11:00:00', 'fim' => '2014-02-21 23:59:00'),
		);

		$simularEtapa = FALSE;

		if($simularEtapa){
			switch($simularEtapa){
				default:
				case '1':
					$view = 'concurso.etapa1';
					break;
				case '1e2':
					$view = 'concurso.entre-etapa1-2';
					break;
				case '2':
					$view = 'concurso.etapa2';
					break;
				case '2e3':
					$view = 'concurso.entre-etapa2-3';
					break;
				case '3':
					$view = 'concurso.etapa3';
					break;
				case 'pos':
					$view = 'concurso.pos-etapa3';
					break;
			}
			$this->layout->content = View::make($view);

		}else{
		
			if($datetime_atual < $etapas['1']['inicio'])
				$this->layout->content = View::make('concurso.etapa1'); // ANTES DA ETAPA 1
			elseif($datetime_atual >= $etapas['1']['inicio'] && $datetime_atual <= $etapas['1']['fim'])
				$this->layout->content = View::make('concurso.etapa1'); // ETAPA 1
			elseif($datetime_atual > $etapas['1']['fim'] && $datetime_atual < $etapas['2']['inicio'])
				$this->layout->content = View::make('concurso.entre-etapa1-2'); // ENTRE ETAPA 1 e 2
			elseif($datetime_atual >= $etapas['2']['inicio'] && $datetime_atual <= $etapas['2']['fim'])
				$this->layout->content = View::make('concurso.etapa2'); // ETAPA 2
			elseif($datetime_atual > $etapas['2']['fim'] && $datetime_atual < $etapas['3']['inicio'])
				$this->layout->content = View::make('concurso.entre-etapa2-3'); // ENTRE ETAPA 2 e 3
			elseif($datetime_atual >= $etapas['3']['inicio'] && $datetime_atual <= $etapas['3']['fim'])
				$this->layout->content = View::make('concurso.etapa3'); // ETAPA 3
			else
				$this->layout->content = View::make('concurso.pos-etapa3'); // PÓS ETAPA 3

		}
	}

	public function inscricao()
	{
		date_default_timezone_set('America/Sao_Paulo');

		// Dados básicos
		$frase = Input::get('frase');
		$nome = Input::get('nome');
		$email = Input::get('email');
		$telefone = Input::get('telefone');
		$cep = Input::get('cep');

		// Identificação
		$semcpf = Input::get('semcpf');
		$cpf = Input::get('cpf');
		$nomeresponsavel = Input::get('nomeresponsavel');
		$cpfresponsavel = Input::get('cpfresponsavel');
		$data = Input::get('data');

		// Checkboxes
		$aceite = Input::get('aceite');
		$newsletter = Input::get('newsletter');

		$datetime_atual = date('Y-m-d H:i:s');
		$etapas = array(
			'1' => array('inicio' => '2013-12-12 11:00:00', 'fim' => '2014-01-10 23:59:00'),
			'2' => array('inicio' => '2014-01-13 11:00:00', 'fim' => '2014-01-31 23:59:00'),
			'3' => array('inicio' => '2014-02-03 11:00:00', 'fim' => '2014-02-21 23:59:00'),
		);
		if($datetime_atual >= $etapas['1']['inicio'] && $datetime_atual <= $etapas['1']['fim']){
			$etapa = 1;
		}elseif($datetime_atual >= $etapas['2']['inicio'] && $datetime_atual <= $etapas['2']['fim']){
			$etapa = 2;
		}elseif($datetime_atual >= $etapas['3']['inicio'] && $datetime_atual <= $etapas['3']['fim']){
			$etapa = 3;
		}else{
			return Response::json(array('erro' => true, 'mensagem' => "A Etapa atual ainda não está aceitando inscrições. Verifique datas e horários no Regulamento."));			
		}

		// Verifica campos obrigatórios
		if($frase == '') return Response::json(array('erro' => true, 'mensagem' => "Informe uma frase com a hashtag #todentro para poder participar!"));
		if(strpos($frase, "#todentro") === FALSE) return Response::json(array('erro' => true, 'mensagem' => "Informe uma frase com a hashtag #todentro para poder participar!"));
		if($nome == '') return Response::json(array('erro' => true, 'mensagem' => "Informe seu Nome Completo!"));
		if($email == '') return Response::json(array('erro' => true, 'mensagem' => "Informe seu e-mail!"));
		if($telefone == '') return Response::json(array('erro' => true, 'mensagem' => "Informe um telefone para contato!"));
		if($cep == '') return Response::json(array('erro' => true, 'mensagem' => "Informe seu CEP!"));
		if($semcpf == 1){
			if($nomeresponsavel == '') return Response::json(array('erro' => true, 'mensagem' => "Informe o Nome Completo do responsável!"));
			if($cpfresponsavel == '') return Response::json(array('erro' => true, 'mensagem' => "Informe o CPF do responsável!"));
			if(!Tools::validarCpf($cpfresponsavel)) return Response::json(array('erro' => true, 'mensagem' => "Informe um CPF válido para o responsável!"));
			if($data == '') return Response::json(array('erro' => true, 'mensagem' => "Informe sua data de nascimento!"));
			if(!Tools::validarData($data)) return Response::json(array('erro' => true, 'mensagem' => "Informe uma data de nascimento válida!"));
		}else{
			if($cpf == '') return Response::json(array('erro' => true, 'mensagem' => "Informe o seu CPF!"));
			if(!Tools::validarCpf($cpf)) return Response::json(array('erro' => true, 'mensagem' => "Informe um CPF válido!"));
		}
		if($aceite == 0) return Response::json(array('erro' => true, 'mensagem' => "É necessário ler e aceitar o regulamento para poder participar!"));

		// Validações Extras
		if($semcpf == 0){
			// 1 participação por cpf
			if(count(ParticipacoesConcurso::where('cpf', '=', $cpf)->where('etapa', '=', $etapa)->get()) > 0) return Response::json(array('erro' => true, 'mensagem' => "Este CPF já está cadastrado!"));
		}else{
			// 3 participações por cpf do responsável
			if(count(ParticipacoesConcurso::where('cpf', '=', $cpfresponsavel)->where('etapa', '=', $etapa)->get()) > 2) return Response::json(array('erro' => true, 'mensagem' => "O CPF do Responsável só pode ser usado em até 3 cadastros!"));
		}

		$participacao = new ParticipacoesConcurso;

		$participacao->etapa = $etapa;
		$participacao->frase = $frase;
		$participacao->nome_completo = $nome;
		$participacao->email = $email;
		$participacao->telefone = $telefone;
		$participacao->cep = $cep;
		$participacao->cpf = $cpf;
		$participacao->semcpf = $semcpf;
		$participacao->nome_responsavel = $nomeresponsavel;
		$participacao->cpf_responsavel = $cpfresponsavel;
		if($data != '')
			$participacao->data_nascimento = Tools::converteData($data);
		$participacao->aceite = $aceite;
		$participacao->newsletter = $newsletter;
		$participacao->data_cadastro = date('Y-m-d');
		$participacao->ip_cadastro = Tools::ip();

		$participacao->save();
		self::enviaEmailConfirmacao($participacao->id);

		if($participacao->newsletter == 1)
			self::cadastrarNewsletter($participacao->nome_completo, $participacao->email);
		
		return Response::json(array("erro" => false));
	}

	private function enviaEmailConfirmacao($id_inscricao)
	{
		date_default_timezone_set('America/Sao_Paulo');

		$inscricao = ParticipacoesConcurso::find($id_inscricao)->toArray();
		Mail::send('emails.concurso', $inscricao, function($message) use ($inscricao)
		{
		    $message->to('sac@jandaia.com', 'SAC Jandaia')
		    		->subject('Inscrição no Concurso #TôDentro')
		    		->cc($inscricao['email'], $inscricao['nome_completo'])
		    		->replyTo($inscricao['email'], $inscricao['nome_completo']);
		});
	}

	private function cadastrarNewsletter($nome, $email)
	{
		$check = Cadastro::where('email', '=', $email)->get();

		if(sizeof($check) == 0 && $email){
			$cadastro = new Cadastro;
			$cadastro->nome = $nome;
			$cadastro->email = $email;
			$cadastro->save();			
		}
	}
}