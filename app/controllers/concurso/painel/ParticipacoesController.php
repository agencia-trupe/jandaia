<?php

namespace Concurso\Painel;

use View, ParticipacoesConcurso;

class ParticipacoesController extends BaseAdminController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.painel.concurso';

	public function index($etapa = false)
	{
		if(!$etapa)
			$this->layout->content = View::make('concurso.painel.participacoes.index')->with('registros', ParticipacoesConcurso::orderBy('nome_completo')->get())->with('etapa', 0);
		else
			$this->layout->content = View::make('concurso.painel.participacoes.index')->with('registros', ParticipacoesConcurso::where('etapa', '=', $etapa)->orderBy('nome_completo')->get())->with('etapa', $etapa);
	}

	public function view($id = false)
	{
		if(!$id)
			return Redirect('painel/participacoes');
		else
			$this->layout->content = View::make('concurso.painel.participacoes.detalhe')->with('registro', ParticipacoesConcurso::find($id));
	}

}