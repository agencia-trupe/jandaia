<?php

// Namespace
namespace Concurso\Painel;

// Core
use View, Input, Hash, Session, Redirect;

// Models
use User;

class UsuariosController extends BaseAdminController {

	protected $layout = 'templates.painel.concurso';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('concurso.painel.usuarios.index')->with('usuarios', User::where('user_painel_concurso', '=', 1)->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('concurso.painel.usuarios.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$usuario = new User;
		$usuario->email = Input::get('email');
		$usuario->username = Input::get('username');
		$usuario->password = Hash::make(Input::get('password'));
		$usuario->user_painel_concurso = 1;
		$usuario->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário criado com sucesso.');

		return Redirect::route('painel.usuarios.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('concurso.painel.usuarios.edit')->with('usuario', User::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$usuario = User::find($id);
		$usuario->email = Input::get('email');
		$usuario->username = Input::get('username');
		if(Input::get('password'))
			$usuario->password = Hash::make(Input::get('password'));
		$usuario->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário alterado com sucesso.');

		return Redirect::route('painel.usuarios.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$usuario = User::find($id);
		$usuario->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário removido com sucesso.');

		return Redirect::route('painel.usuarios.index');
	}

}