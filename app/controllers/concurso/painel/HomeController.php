<?php

namespace Concurso\Painel;

use View;

class HomeController extends BaseAdminController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.painel.concurso';

	public function index()
	{
		$data = array();
		$this->layout->content = View::make('concurso.painel.home', $data);
	}

	public function login()
	{
		return View::make('concurso.painel.login');
	}

}