<?php

namespace Representantes;

use Novidade, BaseController, View;

class NovidadesController extends \Representantes\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.representantes';

	public function index($slug = false)
	{

		$listaNovidades = Novidade::orderBy('data', 'desc')->get();

		if(!$slug){
			$destaqueNovidade = Novidade::orderBy('data', 'desc')->first();
		}else{
			$destaqueNovidade = Novidade::where('slug', '=', $slug)->first();
		}

		$this->layout->content = View::make('representantes.novidades')->with('listaNovidades', $listaNovidades)
																->with('destaqueNovidade', $destaqueNovidade);
	}
}