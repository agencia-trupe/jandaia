<?php
// Namespace
namespace Representantes;

// Core
use Request, Mail, Session, Redirect, View;

// Models
use Novidade;

// BaseClass
//use BaseController;

class ContatoController extends \Representantes\BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.representantes';

	public function index()
	{
		$this->layout->content = View::make('representantes.contato');
	}

	public function enviar()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('sac@jandaia.com', 'SAC Jandaia')
			    		->subject('Contato via site')
			    		->cc('jandaia.cadernos@gmail.com')
			    		->replyTo($data['email'], $data['nome']);
			});
			Session::flash('envio', true);
		}else{
			Session::flash('envio', false);
		}

		return Redirect::to('contato');
	}
}