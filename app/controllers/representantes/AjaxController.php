<?php

// Namespace
namespace Representantes;

// Core
use Request, Input, DB;

// Models

// BaseClass
use Controller;

class AjaxController extends \Controller {

	public function __construct(){

	}

	/**
	 * Atualiza a ordem dos itens da tabela
	 *
	 * @return json
	 */
	function gravaOrdem(){
		/* Só aceita requisições feitas via AJAX */
		if(Request::ajax()){
			$menu = Input::get('data');
			$tabela = Input::get('tabela');

	        for ($i = 0; $i < count($menu); $i++) {
	        	DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));
	        }
	        return json_encode($menu);
    	}else{
    		return "no-ajax";
    	}
	}
}