<?php

// Namespace
namespace Representantes;

// Core
use Request, Session, DB, View;

// Models
use Estado;

// BaseClass
//use BaseController;

class RepresentantesController extends \Representantes\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.representantes';

	public function index($ufestado = false)
	{
		if($ufestado)
			$objEstado = Estado::where('uf', '=', $ufestado)->first();
		else
			$objEstado = false;

		$estado = ($objEstado && !is_null($objEstado)) ? $objEstado->id : Request::get('estado');

		$cidade = Request::get('cidade');

		if($estado && !$cidade){
			$listaRepresentantes = DB::table('representantes')->join('representantes_locais', 'representantes.id', '=', 'representantes_locais.id_representante')
																->where('representantes_locais.id_estado', '=', $estado)
																->get();

			if(sizeof($listaRepresentantes) == 0)
				Session::flash('results-fail', true);

		}elseif(!$estado && $cidade){
			$listaRepresentantes = DB::table('representantes')->join('representantes_locais', 'representantes.id', '=', 'representantes_locais.id_representante')
																->where('representantes_locais.id_cidade', '=', $cidade)
																->get();

			if(sizeof($listaRepresentantes) == 0)
				Session::flash('results-fail', true);

		}elseif($estado && $cidade){
			$listaRepresentantes = DB::table('representantes')->join('representantes_locais', 'representantes.id', '=', 'representantes_locais.id_representante')
																->where('representantes_locais.id_estado', '=', $estado)
																->where('representantes_locais.id_cidade', '=', $cidade)
																->get();
																// Encadear WHERE cidade && estado OR cidade && !estado
			if(sizeof($listaRepresentantes) == 0)
				Session::flash('results-fail', true);

		}else{
			$listaRepresentantes = false;
		}

		if($estado && !$ufestado)
			$ufestado = strtolower(Estado::where('id', '=', $estado)->first()->uf);

		$this->layout->content = View::make('representantes.representantes')->with('estados', Estado::orderBy('nome')->get())
																	->with('listaRepresentantes', $listaRepresentantes)
																	->with('filtroEstado', $estado)
																	->with('filtroCidade', $cidade)
																	->with('marcarEstado', $ufestado);
	}
}