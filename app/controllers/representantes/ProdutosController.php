<?php

// Namespace
namespace Representantes;

// Core
use View, DB, App;

// Models
use Tipo, Linha, Produto;

// BaseClass
//use BaseController;

class ProdutosController extends \Representantes\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.representantes';

    /**
	*	Serão 4 views
	*	1 - Seleção de Tipos & Linhas
	*	2 - Listagem de produtos de um Tipo
	*	3 - Listagem de produtos da Linha (+ Tipo)
	*	4 - Listagem de produtos da Linha (Todos os tipos)
    */
	public function index($tipo = 'linha', $linha = false, $produto = false)
	{
		// Listagem de Tipos para o menu colorido
		$listaTipos = Tipo::orderBy('ordem')->get();
		// Listagem de Linhas para o menu lateral
		$listaLinhas = Linha::orderBy('titulo')->get();
		// Listagem de Produtos
		$listaProdutos = '';

		// Link para outros tipos de produtos da mesma linha
		$outrosProdutos = false;
		// Lista de Produtos da mesma linha, mesmo tipo mas subdivisões diferente
		$listaProdutosMesmoTipoCapas = false;
		$listaProdutosMesmoTipoMiolos = false;

		// Para Tipos de Produtos que não tem Miolos, esticar a caixa Branca de texto
		$esticarCaixa = false;

		// Vamos verificar se o tipo existe
		$objTipo = Tipo::where('slug', '=', $tipo)->first();
		// Se não existir, fazer oq ?
		if(is_null($objTipo)){
			$tipo = 'linha';
		}

		// Vamos verificar se a linha existe
		$objLinha = Linha::where('slug', '=', $linha)->first();
		// Se não existir, fazer oq ?
		if(is_null($objLinha)){
			$linha = false;
		}

		// Verificando se o produto existe
		$objProduto = Produto::where('slug', '=', $produto)->where('publicar_representantes', '=', '1')->first();
		// Se não existir
		if(is_null($objProduto)){
			$produto = false;
		}



		// Determinação de View a ser exibida

		// url: /produtos ou /produtos/linha -> Mostrar seleção de tipo/linha
		if($tipo == 'linha' && !$linha && !$produto)
			$view = "index";


		// url: /produtos/agendas/ -> Mostrar lista de agendas
		elseif($tipo != "linha" && !$linha && !$produto){
			$view = "listar_por_tipo";

			// Listar 16 Produtos de um tipo
			$listaProdutos = Produto::orderBy(DB::raw('RAND()'))->where('publicar_representantes', '=', '1')
																->where('produtos_tipo_id', '=', $objTipo->id)
																->where('miolo', 0)
																->take(16)
																->get();

			// Pegar slugs de Linha e de Tipo para cada produto
			foreach($listaProdutos as $k => $v){
				$this_linha = Linha::find($v->produtos_linha_id);
				$v->linha_slug = is_null($this_linha) ? '' : $this_linha->slug;
				$v->tipo_slug = $objTipo->slug;
			}

$qryLinhasComProdutos = <<<STR
SELECT *
FROM produtos_linhas
WHERE produtos_linhas.id IN(
    SELECT DISTINCT produtos.produtos_linha_id
    FROM produtos
    WHERE produtos.produtos_tipo_id = {$objTipo->id}
    AND produtos.publicar_representantes = '1'
) ORDER BY produtos_linhas.titulo ASC
STR;
			// Lista de Linhas que tem Produtos no mesmo Tipo do Selecionado
			$listaLinhas = DB::select(DB::raw($qryLinhasComProdutos));

		}

		// url: /produtos/papeis/coca-cola -> Mostrar papéis da Coca Cola
		// Se não houver slug de produto, destacar o primeiro
		elseif($tipo != "linha" && $linha){

			/**********************************/
			//		DETALHE DO PRODUTO
			/**********************************/

			$view = "listar_por_tipo_e_linha";

			// Listar Produtos de um tipo e de uma linha

			// Se o produto for um CADERNO (tipo == 11)
			// ordenar resultados pelo tipo, começando pelo tipo do atual
			if($objTipo->id == 11){

				// if(!is_null($objProduto)){
				// 	// Ordenação de cadernos muda de acordo com o selecionado
				// 	switch ($objProduto->tipo_caderno) {
				// 		case 1:
				// 			$str_ordenacao = "1, 2, 3, 4";
				// 			break;
				// 		case 2:
				// 			$str_ordenacao = "2, 1, 3, 4";
				// 			break;
				// 		case 3:
				// 			$str_ordenacao = "3, 1, 2, 4";
				// 			break;
				// 		case 4:
				// 			$str_ordenacao = "4, 1, 2, 3";
				// 			break;
				// 		default:
				// 			$str_ordenacao = "1, 2, 3, 4";
				// 			break;
				// 	}
				// }else{
					// Ordenação Padrão
					// Brochura -> Espiral -> Plus -> Pedagógico
					$str_ordenacao = "2, 5, 6, 7, 3, 1, 4, 8";
				//}

				if(!is_null($objProduto) && $objProduto->produtos_tipo_id == 11){

					// Seleciona CAPAS de cadernos do mesmo tipo para serem mostrados abaixo do detalhe
					$listaProdutosMesmoTipoCapas = Produto::orderBy('ordem', 'ASC')
													->where('publicar_representantes', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('tipo_caderno', '=', $objProduto->tipo_caderno)
													->where('miolo', '=', '0')
													->get();

					// Seleciona MIOLOS de cadernos do mesmo tipo para serem mostrados abaixo do detalhe
					$listaProdutosMesmoTipoMiolos = Produto::orderBy('ordem', 'ASC')
													->where('publicar_representantes', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('tipo_caderno', '=', $objProduto->tipo_caderno)
													->where('miolo', '=', '1')
													->get();

					// Seleciona os demais tipos de caderno da mesma Linha
					$listaProdutos = Produto::orderBy(DB::raw("FIELD(tipo_caderno, $str_ordenacao), miolo, ordem"))
											->where('publicar_representantes', '=', '1')
											->where('produtos_tipo_id', '=', $objTipo->id)
											->where('produtos_linha_id', '=', $objLinha->id)
											->where('tipo_caderno', '!=', $objProduto->tipo_caderno)
											->where('produtos.miolo', '=', 0)
											->get();

				}else{
					$listaProdutos = Produto::orderBy(DB::raw("FIELD(tipo_caderno, $str_ordenacao), miolo, ordem"))
											->where('publicar_representantes', '=', '1')
											->where('produtos_tipo_id', '=', $objTipo->id)
											->where('produtos_linha_id', '=', $objLinha->id)
											->where('produtos.miolo', '=', 0)
											->get();
				}
			}else{
				if(!is_null($objProduto) && $objProduto->produtos_tipo_id == 12){
					$listaProdutosMesmoTipoCapas = Produto::orderBy('ordem', 'ASC')
													->where('publicar_representantes', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('miolo', '=', '0')
													->get();

					$listaProdutosMesmoTipoMiolos = Produto::orderBy('ordem', 'ASC')
													->where('publicar_representantes', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('miolo', '=', '1')
													->get();

					$listaProdutos = FALSE;
				}else{
					$esticarCaixa = TRUE;
					$listaProdutos = Produto::orderBy('miolo', 'ASC')
											->orderBy('ordem', 'ASC')
											->where('publicar_representantes', '=', '1')
											->where('produtos_tipo_id', '=', $objTipo->id)
											->where('produtos_linha_id', '=', $objLinha->id)
											->where('miolo', '=', '0')
											->get();
				}
			}

			// Pegar slugs de Linha e de Tipo para cada produto
			if($listaProdutos){
				foreach($listaProdutos as $k => $v){
					$this_linha = Linha::find($v->produtos_linha_id);
					$v->linha_slug = is_null($this_linha) ? '' : $this_linha->slug;
					$v->tipo_slug = $objTipo->slug;
					if(!$produto && $k == 0){
						$produto = $v;
					}
				}
			}

			// Buscar outros tipos de Produtos da mesma Linha
			$outrosProdutos = DB::table('produtos')
								->select(DB::raw("DISTINCT produtos.produtos_tipo_id"), 'produtos_tipos.slug', 'produtos_tipos.titulo')
								->join('produtos_tipos', 'produtos.produtos_tipo_id', '=', 'produtos_tipos.id')
								->where('produtos.publicar_representantes', '=', '1')
								->where('produtos.produtos_linha_id', '=', $objLinha->id)
								->where('produtos.produtos_tipo_id', '!=', $objTipo->id)
								->where('miolo', '=', '0')
								->get();

$qryLinhasComProdutos = <<<STR
SELECT *
FROM produtos_linhas
WHERE produtos_linhas.id IN(
    SELECT DISTINCT produtos.produtos_linha_id
    FROM produtos
    WHERE produtos.produtos_tipo_id = {$objTipo->id}
    AND produtos.publicar_representantes = '1'
) ORDER BY produtos_linhas.titulo ASC
STR;
			// Lista de Linhas que tem Produtos no mesmo Tipo do Selecionado
			$listaLinhas = DB::select(DB::raw($qryLinhasComProdutos));
		}

		// url: /produtos/linha/coca-cola -> Mostrar todos os produtos da Linha CocaCola
		elseif($tipo == 'linha' && $linha && !$produto){
			$view = "listar_por_linha";

			// Listar Produtos de uma linha e de todos os tipos
			$listaProdutos = DB::table('produtos')
								->join('produtos_tipos', 'produtos.produtos_tipo_id', '=', 'produtos_tipos.id')
								->join('produtos_linhas', 'produtos.produtos_linha_id', '=', 'produtos_linhas.id')
								->orderBy(DB::raw("FIELD(produtos.produtos_tipo_id, 11, 12, 14, 15, 13), produtos.miolo, produtos.ordem"))
								->where('produtos.produtos_linha_id', '=', $objLinha->id)
								->where('produtos.miolo', '=', 0)
								->where('produtos.publicar_representantes', '=', '1')
								->select('produtos.*', 'produtos_tipos.slug as tipo_slug', 'produtos_linhas.slug as linha_slug', 'produtos_tipos.titulo as tipo_titulo')
								->get();
		}

		// url:: /produtos/linha/coca-cola/15-caderno-capa-dura -> Retornar a view do produtos em Ajax
		elseif($tipo == 'linha' && $linha && $produto){
			// View em modal via AJAX foi descontinuada
			//$view = "detalhe_produto_ajax";
			App::abort(404);
		}

		else{
			App::abort(404);
		}

		$this->layout->content = View::make('representantes.produtos.'.$view)->with('tipo', $objTipo)
																	->with('linha', $objLinha)
																	->with('produto', $objProduto)
																	->with('listaTipos', $listaTipos)
																	->with('listaLinhas', $listaLinhas)
																	->with('listaProdutos', $listaProdutos)
																	->with('outrosProdutos', $outrosProdutos)
																	->with('listaProdutosMesmoTipoCapas', $listaProdutosMesmoTipoCapas)
																	->with('listaProdutosMesmoTipoMiolos', $listaProdutosMesmoTipoMiolos)
																	->with('esticarCaixa', $esticarCaixa);
	}

}