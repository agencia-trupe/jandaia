<?php

// Namespace
namespace Representantes;

// Core
use View;

// Models
use Institucional, Certificacoes;

// BaseClass
//use BaseController;

class EmpresaController extends \Representantes\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.representantes';

	public function historico(){
    	$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.texto')
										->with('conteudo', Institucional::where('slug', '=', 'historico')->first())
										->with('certificacoes', FALSE);
	}

	public function melhorescolha()
	{
		$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.melhorescolha');
	}

	public function sustentabilidade(){
		$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.texto')
										->with('conteudo', Institucional::where('slug', '=', 'sustentabilidade')->first())
										->with('certificacoes', FALSE);
	}

	public function certificacoes(){
		$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.texto')
										->with('conteudo', Institucional::where('slug', '=', 'certificacoes')->first())
										->with('qualidade', Institucional::where('slug', '=', 'qualidade-jandaia')->first())
										->with('certificacoes', Certificacoes::orderBy('ordem', 'ASC')->get());
	}

	public function produtos(){
		$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.texto')
										->with('conteudo', Institucional::where('slug', '=', 'produtos-personalizados')->first())
										->with('certificacoes', FALSE);
	}

	public function exportacao(){
		$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.texto')
										->with('conteudo', Institucional::where('slug', '=', 'exportacao')->first())
										->with('certificacoes', FALSE);
	}

	public function lab(){
		$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.lab');
	}

	public function international(){
		$this->layout->css = 'empresa';
		$this->layout->content = View::make('representantes.empresa.international')
										->with('conteudo', Institucional::where('slug', '=', 'exportacao')->first());
	}
}
