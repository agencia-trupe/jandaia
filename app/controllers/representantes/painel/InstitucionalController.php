<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect, File;

// Models
use Institucional;

// Libs
use Tools, Imagine, Image;

class InstitucionalController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('representantes.painel.institucional.index')->with('registros', Institucional::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.institucional.index');		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.institucional.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Redirect::route('painel.institucional.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.institucional.edit')->with('registro', Institucional::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$inst = Institucional::find($id);

		$inst->texto = Input::get('texto');

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/institucional/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			
			$inst->imagem = $filename;

			ini_set('memory_limit','256M');

			if($inst->slug == 'certificacoes')
				Image::make(Input::file('imagem')->getRealPath())->resize(260, null, true, false)->save('assets/images/institucional/'.$filename, 100);			
			else
				Image::make(Input::file('imagem')->getRealPath())->resize(402, null, true, false)->save('assets/images/institucional/'.$filename, 100);			
		}
		
		if(Input::has('remover_imagem') && Input::get('remover_imagem') == 1)
			$inst->imagem = null;

		if(Input::hasFile('imagem_maior')){

			$imagem_maior = Input::file('imagem_maior');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem_maior->getClientOriginalName()).$random.'.'.File::extension($imagem_maior->getClientOriginalName());
			    $file_path = 'assets/images/institucional/'.$filename;
			    $random = '_'.Str::random(6);
			}
			while (File::exists($file_path));
			
			$inst->imagem_maior = $filename;

			ini_set('memory_limit','256M');
			Image::make(Input::file('imagem_maior')->getRealPath())->resize(960, null, true, false)->save('assets/images/institucional/'.$filename, 100);
		}	

		if(Input::has('remover_imagem_maior') && Input::get('remover_imagem_maior') == 1)
			$inst->imagem_maior = null;	

		$inst->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto alterado com sucesso.');

		return Redirect::route('painel.institucional.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::route('painel.institucional.index');
	}

	public function texto()
	{
		$this->layout->content = View::make('representantes.painel.institucional.texto')->with('registro', Institucional::where('slug', '=', 'certificacoes')->first());
	}

	public function updateTexto()
	{
		$inst = Institucional::where('slug', '=', 'certificacoes')->first();
		$inst->texto_extra = Input::get('texto_extra');
		$inst->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto Box alterado com sucesso.');

		return Redirect::route('painel.institucional.index');
	}

}