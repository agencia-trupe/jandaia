<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect, File;

// Models
use Novidade;

// Libs
use Imagine, Tools;

class NovidadesController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('representantes.painel.novidades.index')->with('novidades', Novidade::orderBy('data', 'desc')->paginate(10));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('representantes.painel.novidades.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$novidade = new Novidade;

		// Thumb: 70x70 Original:520x300
		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			//$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/novidades/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/novidades', $filename);
			$novidade->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(520,300);
			$sizeThumb = new Imagine\Image\Box(70,70);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_INSET;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/novidades/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/novidades/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/novidades/thumbs/'.$filename, array('quality' => 100));
		}

		if(Input::hasFile('imagem_destaque')){

			$imagem_destaque = Input::file('imagem_destaque');
			//$filename = Str::random(20) .'.'. $imagem_destaque->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem_destaque->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem_destaque->getClientOriginalName());
			    $file_path = 'assets/images/novidades/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem_destaque->move('assets/images/novidades', $filename);
			$novidade->imagem_destaque = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeThumb = new Imagine\Image\Box(70,70);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_INSET;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/novidades/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/novidades/thumbs/'.$filename, array('quality' => 100));
		}

		$novidade->titulo = Input::get('titulo');
		$novidade->olho = Input::get('olho');
		$novidade->texto = Input::get('texto');
		$novidade->data = Tools::converteData(Input::get('data'));
		$novidade->save();

		$novidade->slug = Str::slug($novidade->id.'_'.$novidade->titulo);
		$novidade->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Novidade criada com sucesso.');

		return Redirect::route('painel.novidades.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.novidades.edit')->with('novidade', Novidade::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$novidade = Novidade::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			//$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/novidades/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/novidades', $filename);
			$novidade->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(520,300);
			$sizeThumb = new Imagine\Image\Box(70,70);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
			$imagine->open('assets/images/novidades/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/novidades/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/novidades/thumbs/'.$filename, array('quality' => 100));
		}

		if(Input::hasFile('imagem_destaque')){

			$imagem_destaque = Input::file('imagem_destaque');
			//$filename = Str::random(20) .'.'. $imagem_destaque->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem_destaque->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem_destaque->getClientOriginalName());
			    $file_path = 'assets/images/novidades/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem_destaque->move('assets/images/novidades', $filename);
			$novidade->imagem_destaque = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeThumb = new Imagine\Image\Box(70,70);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
			$imagine->open('assets/images/novidades/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/novidades/thumbs/'.$filename, array('quality' => 100));
		}

		$novidade->titulo = Input::get('titulo');
		$novidade->olho = Input::get('olho');
		$novidade->texto = Input::get('texto');
		$novidade->data = Tools::converteData(Input::get('data'));
		$novidade->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Novidade alterada com sucesso.');

		return Redirect::route('painel.novidades.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$novidade = Novidade::find($id);
		$novidade->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Novidade removida com sucesso.');

		return Redirect::route('painel.novidades.index');
	}

}