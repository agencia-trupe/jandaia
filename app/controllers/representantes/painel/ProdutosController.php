<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect, File, DB;

// Models
use Produto, Tipo, Linha;

// Libs
use Imagine, Tools;

class ProdutosController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tipo = Input::get('filtra-tipo');
		$linha = Input::get('filtra-linha');

		if($tipo && !$linha)
			$listaDeProdutos = DB::table('produtos')->orderBy('produtos_linha_id', 'asc')->where('produtos_tipo_id', $tipo)->get();
		elseif($linha && !$tipo)
			$listaDeProdutos = DB::table('produtos')->orderBy('produtos_tipo_id', 'asc')->where('produtos_linha_id', $linha)->get();
		elseif($linha && $tipo)
			$listaDeProdutos = DB::table('produtos')->orderBy('ordem', 'asc')->where('produtos_tipo_id', $tipo)->where('produtos_linha_id', $linha)->get();
		else
			$listaDeProdutos = Produto::orderBy('id')->get();

		$this->layout->content = View::make('representantes.painel.produtos.index')->with('produtos', $listaDeProdutos)
																	->with('filtro_linha', $linha)
																	->with('filtro_tipo', $tipo)
																	->with('tipos', Tipo::orderBy('titulo', 'asc')->get())
																	->with('linhas', Linha::orderBy('titulo', 'asc')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('representantes.painel.produtos.form')->with('tipos', Tipo::orderBy('titulo', 'asc')->get())->with('linhas', Linha::orderBy('titulo', 'asc')->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Produto;

		/***********************************/
		/*		Upload de Imagem 	   	   */
		/*   só manter caso exista imagem  */
		/* Thumb:170x210  Ampliada:355x430 */
		/***********************************/
		if(Input::hasFile('imagem')){

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(355,430);
			$sizeThumb = new Imagine\Image\Box(170,210);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_INSET;

			$imagem = Input::file('imagem');
			//$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/produtos/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/produtos', $filename);
			$object->imagem = $filename;
			ini_set('memory_limit','256M');
			$imagine->open('assets/images/produtos/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/produtos/'.$filename, array('quality' => 100))
				    ->thumbnail($sizeThumb, $mode)
				    ->save('assets/images/produtos/thumbs/'.$filename, array('quality' => 100));
		}
		/***********************************/

		$object->titulo = Input::get('titulo');
		$object->produtos_tipo_id = Input::get('produtos_tipo_id');
		$object->produtos_linha_id = Input::get('produtos_linha_id');
		//$object->detalhes = Input::get('detalhes');
		$object->detalhes = get_magic_quotes_gpc() ? stripslashes(Input::get('detalhes')) : Input::get('detalhes');
		$object->miolo = Input::get('miolo');
		$object->tipo_caderno = Input::get('tipo_caderno');

		$object->publicar_cadernos = Input::has('publicar_cadernos') ? Input::get('publicar_cadernos') : '0';
		$object->publicar_representantes = Input::has('publicar_representantes') ? Input::get('publicar_representantes') : '0';

		$object->save();

		$object->slug = Str::slug($object->id.' '.$object->titulo);
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Produto criado com sucesso.');

		return Redirect::route('painel.produtos.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.produtos.edit')->with('produto', Produto::find($id))
																	->with('tipos', Tipo::all())
																	->with('linhas', Linha::all());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Produto::find($id);

		/***********************************/
		/*		Upload de Imagem 	   	   */
		/*   só manter caso exista imagem  */
		/***********************************/
		if(Input::hasFile('imagem')){

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(355,430);
			$sizeThumb = new Imagine\Image\Box(170,210);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_INSET;

			$imagem = Input::file('imagem');
			//$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/produtos/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));

			$imagem->move('assets/images/produtos', $filename);
			$object->imagem = $filename;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/produtos/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/produtos/'.$filename, array('quality' => 100))
				    ->thumbnail($sizeThumb, $mode)
				    ->save('assets/images/produtos/thumbs/'.$filename, array('quality' => 100));
		}
		/***********************************/

		$object->titulo = Input::get('titulo');
		$object->produtos_tipo_id = Input::get('produtos_tipo_id');
		$object->produtos_linha_id = Input::get('produtos_linha_id');
		//$object->detalhes = Input::get('detalhes');
		$object->detalhes = get_magic_quotes_gpc() ? stripslashes(Input::get('detalhes')) : Input::get('detalhes');
		$object->miolo = Input::get('miolo');
		$object->tipo_caderno = Input::get('tipo_caderno');
		$object->slug = Str::slug($object->id.' '.Input::get('titulo'));

		$object->publicar_cadernos = Input::has('publicar_cadernos') ? Input::get('publicar_cadernos') : '0';
		$object->publicar_representantes = Input::has('publicar_representantes') ? Input::get('publicar_representantes') : '0';

		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Produto alterado com sucesso.');

		return Redirect::route('painel.produtos.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Produto::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Produto removido com sucesso.');

		return Redirect::route('painel.produtos.index');
	}

}