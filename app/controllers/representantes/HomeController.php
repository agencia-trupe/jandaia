<?php

// Namespace
namespace Representantes;

// Core
use View, DB, Request, Session, Redirect;

// Models
use Banner, Novidade, Cadastro;

// BaseClass
//use BaseController;

class HomeController extends \Representantes\BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.representantes';

	public function index()
	{
		$this->layout->content = View::make('representantes.home')->with('banners', Banner::orderBy('ordem')->get())
														 		  ->with('novidades', Novidade::orderBy(DB::raw('RAND()'))->limit(3)->get());
	}

	public function cadastro()
	{
		$nome = Request::get('nome');
		$email = Request::get('email');

		$check = Cadastro::where('email', '=', $email)->get();

		if(sizeof($check) == 0 && $email){
			$cad = new Cadastro;
			$cad->nome = $nome;
			$cad->email = $email;
			$cad->save();
			Session::flash('cadastro', true);
		}else{
			Session::flash('cadastro', false);
		}

		return Redirect::to('/');
	}
}