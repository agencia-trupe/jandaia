<?php

// Namespace
namespace Consumidor;

// Core
use View;

// Models

// BaseClass
//use BaseController;

class JandaiaController extends \Consumidor\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.consumidor';

	public function index()
	{
		$this->layout->content = View::make('consumidor.jandaia');
	}
}