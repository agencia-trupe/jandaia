<?php

// Namespace
namespace Consumidor;

// Core
use View, DB;

// Models
use BannerConsumidor, Novidade, Produto, Linha, Blog;

// BaseClass
//use BaseController;

class HomeController extends \Consumidor\BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.consumidor';

	public function index()
	{

		date_default_timezone_set('America/Sao_Paulo');
		$datetime_atual = date('Y-m-d H:i:s');
		$publicacao1D = array('inicio' => '2014-04-17 07:30:00');

		if($datetime_atual > $publicacao1D['inicio']){
			$banner1D = true;
			$carrossel1D = Produto::where('produtos_tipo_id', '=', 11)
									->where('produtos.publicar_cadernos', '=', '1')
									->where('tipo_caderno', '=', 2)
									->where('miolo', '=', 0)
									->where('produtos_linha_id', '=', 74)
									->orderBy(DB::raw('RAND()'))
									->limit(4)
									->get();
		}
		//else{
			$banner1D = false;
			$carrossel1D = false;
		//}

		$banners = BannerConsumidor::orderBy('ordem', 'asc')->get();

		foreach ($banners as $key => $value) {
			$carrosseis[$value->id] = Produto::where('produtos_linha_id', '=', $value->linha)
										->where('produtos.publicar_cadernos', '=', '1')
										->where('produtos_tipo_id', '=', 11)
										->where('tipo_caderno', '=', 2)
										->where('miolo', '=', 0)
										->orderBy(DB::raw('RAND()'))
										->limit(4)
										->get();
		}		

		$carrosselConcurso = Produto::where('produtos_tipo_id', '=', 11)
									->where('produtos.publicar_cadernos', '=', '1')
									->where('tipo_caderno', '=', 2)
									->where('miolo', '=', 0)
									->orderBy(DB::raw('RAND()'))
									->limit(4)
									->get();

		$carrosselYoutube = Produto::where('produtos_tipo_id', '=', 11)
									->where('produtos.publicar_cadernos', '=', '1')
									->where('tipo_caderno', '=', 2)
									->where('miolo', '=', 0)
									->whereIn('produtos_linha_id', Linha::lists('id'))
									->orderBy(DB::raw('RAND()'))
									->limit(4)
									->get();

		foreach ($carrosselYoutube as $key => $value) {
			
		}
		
		$this->layout->content = View::make('consumidor.home')->with('banners', $banners)
															  ->with('carrosseis', $carrosseis)
															  ->with('blog', Blog::orderBy('data', 'desc')->take(3)->get())
															  ->with('carrosselConcurso', $carrosselConcurso)
															  ->with('carrosselYoutube', $carrosselYoutube)
															  ->with('banner1D', $banner1D)
															  ->with('carrossel1D', $carrossel1D);
	}

	public function completo()
	{
		$this->layout->content = View::make('consumidor.completo');
	}
}