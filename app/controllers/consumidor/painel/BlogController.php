<?php

// Namespace
namespace Consumidor\Painel;

// Core
use View, Input, Str, Session, Redirect, File;

// Modelss
use Blog, BlogCategoria;

// Libs
use Imagine, Tools;

class BlogController extends BaseAdminController {

	protected $layout = 'templates.painel.consumidor';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('consumidor.painel.blog.index')->with('blogs', Blog::orderBy('data', 'desc')->paginate(15));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('consumidor.painel.blog.form')
									 ->with('categorias', BlogCategoria::all());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$post = new Blog;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/consumidor/blog/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/consumidor/blog/', $filename);
			$post->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(700,700);
			$sizeThumb = new Imagine\Image\Box(300,300);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/consumidor/blog/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/consumidor/blog/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/consumidor/blog/thumbs/'.$filename, array('quality' => 100));
		}

		$post->titulo = Input::get('titulo');
		$post->data = Tools::converteData(Input::get('data'));
		$post->blog_categorias_id = Input::get('blog_categorias_id');
		$post->texto = Input::get('texto');
		$post->save();

		$post->slug = Str::slug($post->id.'_'.$post->titulo);
		$post->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Post criado com sucesso.');

		return Redirect::route('painel.blog.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('consumidor.painel.blog.edit')->with('post', Blog::find($id))
															   ->with('categorias', BlogCategoria::all());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = Blog::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/consumidor/blog/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/consumidor/blog/', $filename);
			$post->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(700,700);
			$sizeThumb = new Imagine\Image\Box(300,300);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/consumidor/blog/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/consumidor/blog/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/consumidor/blog/thumbs/'.$filename, array('quality' => 100));
		}

		$post->titulo = Input::get('titulo');		
		$post->data = Tools::converteData(Input::get('data'));
		$post->blog_categorias_id = Input::get('blog_categorias_id');
		$post->texto = Input::get('texto');
		$post->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Post alterado com sucesso.');

		return Redirect::route('painel.blog.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Blog::find($id);
		$post->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Post removido com sucesso.');

		return Redirect::route('painel.blog.index');
	}

}