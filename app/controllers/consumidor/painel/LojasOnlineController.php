<?php

namespace Consumidor\Painel;

use View, Input, Str, Session, Thumb, Redirect, Encontrar, EncontrarLocais, Excel, File, Imagine, PHPExcel_IOFactory, chunkReadFilter, LojasOnline;

class LojasOnlineController extends BaseAdminController {

	protected $layout = 'templates.painel.consumidor';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('consumidor.painel.encontrarVirtual.index')->with('registros', LojasOnline::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('consumidor.painel.encontrarVirtual.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new LojasOnline;

		$object->link = Input::get('link');
		$object->titulo = Input::get('titulo');
		
		$imagem = Thumb::make('imagem', 210, 140, 'consumidor/lojas/');
		if($imagem) $object->imagem = $imagem;
		
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Loja inserida com sucesso.');

		return Redirect::route('painel.encontraronline.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Redirect::back();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('consumidor.painel.encontrarVirtual.edit')->with('registro', LojasOnline::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = LojasOnline::find($id);

		$object->link = Input::get('link');
		$object->titulo = Input::get('titulo');
		
		$imagem = Thumb::make('imagem', 210, 140, 'consumidor/lojas/');
		if($imagem) $object->imagem = $imagem;

		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Loja atualizada com sucesso.');

		return Redirect::route('painel.encontraronline.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = LojasOnline::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Lista removida com sucesso.');

		return Redirect::route('painel.encontraronline.index');
	}

}