<?php

// Namespace
namespace Consumidor;

// Core
use View, Request, Mail, Session, Redirect, Linha, Tipo, Estado, Cidade, DB;

// Models

// BaseClass
//use BaseController;

class EncontrarController extends \Consumidor\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.consumidor';

	public function index()
	{
		$this->layout->with('css', 'consumidor/encontrar');

		$this->layout->content = View::make('consumidor.encontrar')->with('listaLinhas', Linha::orderBy('titulo', 'asc')->get())
																	->with('listaTipos', Tipo::all())
																	->with('listaEstados', Estado::all())
																	->with('listaCidades', Cidade::all());
	}

	public function online()
	{
		$this->layout->with('css', 'consumidor/encontrar');

		$this->layout->content = View::make('consumidor.encontrarOnline')->with('listaLinhas', Linha::orderBy('titulo', 'asc')->get())
																	->with('listaTipos', Tipo::all())
																	->with('listaEstados', Estado::all())
																	->with('listaCidades', Cidade::all())
																	->with('resultados', \LojasOnline::orderBy('ordem', 'asc')->get());
	}

	public function buscar()
	{
		$this->layout->with('css', 'consumidor/encontrar');

		$linha = Request::get('linha');
		$tipo = Request::get('tipo');
		$cep = Request::get('cep');
		$estado = Request::get('estado');
		$cidade = Request::get('cidade');

		$objEstado = Estado::find($estado);
		$objCidade = Cidade::find($cidade);
		$objLinha = Linha::where('slug', '=', $linha)->first();
		$objTipo = Tipo::where('slug', '=', $tipo)->first();

		$this->layout->content = View::make('consumidor.encontrar')->with('listaLinhas', Linha::orderBy('titulo', 'asc')->get())
																	->with('listaTipos', Tipo::all())
																	->with('listaEstados', Estado::all())
																	->with('listaCidades', Cidade::all())
																	->with('resultados', $this->busca($objLinha, $objTipo, $cep, $objEstado, $objCidade))
																	->with('objEstado', $objEstado)
																	->with('objCidade', $objCidade)
																	->with('objLinha', $objLinha)
																	->with('objTipo', $objTipo)
																	->with('cepInformado', $cep);
	}

	private function busca($objLinha, $objTipo, $cep, $objEstado, $objCidade){

		$query = "SELECT * FROM onde_encontrar_registros WHERE uf = '".$objEstado->uf."'";

		if(!is_null($objLinha))
			$query .= " AND linha = '".$objLinha->titulo."'";

		if(!is_null($objTipo))
			$query .= " AND tipo = '".$objTipo->slug."'";

		if(!is_null($objCidade))
			$query .= " AND cidade LIKE '%".mb_strtoupper($objCidade->nome)."%'";

		if($cep){
			$query .= " AND cep LIKE '".substr($cep, 0, 2)."%' ORDER BY ABS((". (int) str_replace('-', '', $cep) ." - CAST(REPLACE(cep,'-','') AS SIGNED))) ASC LIMIT 0, 60";
		}else{
			$query .= " ORDER BY cep ASC";
		}

		$registros = DB::select(DB::raw($query));

		$ordenarPorCep = true;
		
		if($cep){

			$origens = array();
			$contador_origens = 0;
			
			foreach ($registros as $key => $value){				

				$value->distancia = 9999999;

				if(!is_null($objCidade))
					$origens[$key] = $objCidade->nome." - ".$objEstado->uf.", ".$value->cep.", Brasil";
				else
					$origens[$key] = $objEstado->uf.", ".$value->cep.", Brasil";

				$contador_origens++;

				if($contador_origens == 25 && $ordenarPorCep){
					$contador_origens = 0;
					$url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=".urlencode(implode("|", $origens))."&destinations=".$cep."&mode=CAR&language=PT&sensor=false";

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_URL, $url);
					$data = curl_exec($ch);
					curl_close($ch);

					$matriz = json_decode($data);
					if($matriz->status != 'OK')
						$ordenarPorCep = false;

					$i = 0;
					foreach ($origens as $key => $value) {
						if(isset($matriz->rows[$i])){
							$registros[$key]->distancia = $matriz->rows[$i]->elements[0]->distance->value;	
							$i++;
							if($i == 24)
								break;
						}
					}
					$origens = array();
				}
			}
			if($contador_origens > 0 && $ordenarPorCep){
				$url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=".urlencode(implode("|", $origens))."&destinations=".$cep."&mode=CAR&language=PT&sensor=false";

				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				$data = curl_exec($ch);
				curl_close($ch);

				$matriz = json_decode($data);
				if($matriz->status != 'OK')
					$ordenarPorCep = false;

				$i = 0;
				foreach ($origens as $key => $value) {
					if(isset($matriz->rows[$i])){
						$registros[$key]->distancia = $matriz->rows[$i]->elements[0]->distance->value;	
						$i++;
						if($i == 24)
							break;
					}
				}
				$origens = array();
			}

			if($ordenarPorCep)
				usort($registros, array($this, "ordenaPorDistancia"));

			/*echo "<pre>";
			echo $query;
			print_r($registros);
			echo "</pre>";
			die();
			*/
		}
		return $registros;
	}

	private static function ordenaPorDistancia($a, $b){
		if($a->distancia == $b->distancia){
			return 0;
		}
		return ($a->distancia < $b->distancia) ? -1 : 1;
	}

}