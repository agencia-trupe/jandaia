<?php

// Namespace
namespace Consumidor;

// Core
use View, Request, Mail, Session, Redirect;

// Models

// BaseClass
//use BaseController;

class ContatoController extends \Consumidor\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.consumidor';

	public function index()
	{
		$this->layout->with('css', 'consumidor/contato');

		$this->layout->content = View::make('consumidor.contato');
	}

	public function enviar()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('sac@jandaia.com', 'SAC Jandaia')
			    		->subject('Contato via site')
			    		->cc('jandaia.cadernos@gmail.com')
			    		->replyTo($data['email'], $data['nome']);
			});
			Session::flash('envio', true);
		}else{
			Session::flash('envio', false);
		}

		return Redirect::to('contato');
	}		
}