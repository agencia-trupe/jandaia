<?php

class LojasOnline extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'lojas_online';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array();

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

	public function scopeOrdenado($query)
    {
        return $this->orderBy('ordem', 'asc');
    }
}
