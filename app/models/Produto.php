<?php

class Produto extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('titulo', 'slug', 'imagem', 'detalhes', 'ordem', 'miolo', 'produtos_tipo_id', 'produtos_linha_id', 'tipo_caderno', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function tipo(){
    	return $this->hasOne('tipo');
    }

    public function linha(){
    	return $this->hasOne('linha');
    }
}