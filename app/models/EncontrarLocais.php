<?php

class EncontrarLocais extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'onde_encontrar_registros';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('lista_id', 'item', 'descricao', 'tipo', 'linha', 'razao_social', 'endereco', 'bairro', 'cep', 'cidade', 'uf', 'estado', 'telefone', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

	public function lista()
    {
        return $this->belongsTo('Encontrar', 'lista_id');
    }
}
