<?php

class Tipo extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos_tipos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('titulo', 'slug', 'cor', 'imagem', 'ordem', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function produtos(){
    	return $this->hasMany('Produto', 'produtos_tipo_id')->take(16);
    }
}
