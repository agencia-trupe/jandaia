<!DOCTYPE html>
<html>
<head>
    <title>Confirmação de Inscrição no Concurso Cultural Jandaia #TôDentro de um mundo melhor</title>
    <meta charset="utf-8">
</head>
<body>
	<h1>
		Confirmação de Inscrição no Concurso Cultural Jandaia #TôDentro de um mundo melhor
	</h1>
	<p>
		Sua participação no Concurso Cultural Jandaia #TôDentro de um mundo melhor foi recebida com sucesso.
	</p>
	<p>
		Segue a confirmação dos dados cadastrados para você acompanhar e torcer!
	</p>
    <p>
    	<strong>Frase: {{ $frase }} <strong> <br>
    	Nome: {{ $nome_completo }} <br>
    	E-mail: {{ $email }} <br>
    	Telefone: {{ $telefone }}<br>
		CEP: {{ $cep }}<br>
		@if($semcpf == 1)
			Participante possui CPF : Não<br>
			Nome do Responsável : {{ $nome_responsavel }}<br>
			CPF do Responsável : {{ $cpf_responsavel }}<br>
			Data de Nascimento : {{ Tools::converteData($data_nascimento) }}<br>
		@else
			Participante possui CPF : Sim<br>
			CPF : {{ $cpf }}<br>
		@endif
		@if($aceite == 1)
			Li e aceito o regulamento : sim<br>
		@else
			Li e aceito o regulamento : não<br>
		@endif
		@if($newsletter == 1)
			Quero receber mais promoções e informações sobre a Jandaia : sim<br>
		@else
			Quero receber mais promoções e informações sobre a Jandaia : não<br>
		@endif
		Data de Cadastro : {{ Tools::converteData($data_cadastro) }}
    </p>
</body>
</html>