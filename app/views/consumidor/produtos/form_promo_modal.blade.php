<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="cadernos jandaia, cadernos, agendas, fichários, etiquetas, bolsas," />

	<title>Jandaia - Cadernos, Mochilas, Fichários e Agendas</title>
	<meta name="description" content="Cadernos Jandaia: Um caderno completo, da primeira até a última folha! - cadernos, agendas, fichários, bolsas e projetos personalizados em papelaria">    	
	<meta property="og:title" content="Jandaia"/>
	<meta property="og:image" content="{{ asset('assets/images/layout/marca-jandaia.jpg'); }}"/>
	<meta property="og:description" content="Cadernos Jandaia: Um caderno completo, da primeira até a última folha! - cadernos, agendas, fichários, bolsas e projetos personalizados em papelaria"/>

    <meta property="og:site_name" content="Cadernos Jandaia"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
  	<meta property="fb:admins" content="100002297057504"/>
  	<meta property="fb:app_id" content="327477457394269"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'reset',
		'fontface/consumidor/stylesheet',
		'consumidor/modal'
	))?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min', 'libs/less-1.3.0.min'))?>
	@else
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min'))?>
	@endif

</head>
<body>
	
	<!-- Facebook SDK -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=327477457394269";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<!-- Conteúdo Principal -->
	<div class="main">
		@if($envio)
			<p class="resposta">
				<span>
					Sua mensagem foi enviada com sucesso.<br>Boa sorte!
				</span>
			</p>
			<input type="button" id="btn-close-fancybox" value="FECHAR">
		@else
			<p>
				Envie sua mensagem com uma<br>
				resposta criativa para a pergunta:
				<span>"POR QUE VOCÊ QUER TANTO IR AO SHOW DO ONE DIRECTION?"</span>
				e concorra a convites pela Rádio Disney!
			</p>
			<form action="produtos/enviarPromoModal" method="post" id="formPromoModal">
				<input type="text" name="nome" placeholder="nome" required data-required-text="O nome é obrigatório!">
				<input type="email" name="email" placeholder="e-mail" required data-required-text="O e-mail é obrigatório!">
				<input type="text" name="telefone" placeholder="telefone" required data-required-text="O telefone é obrigatório!">
				<textarea name="mensagem" placeholder="mensagem" required data-required-text="A mensagem é obrigatória!"></textarea>
				<label>
					<input type="checkbox" value="1" name="inscrever"> Quero receber mais novidades sobre os produtos e promoções da Jandaia.
				</label>
				<input type="submit" value="ENVIAR">
			</form>
		@endif
	</div>

	<!-- Google Analytics -->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-3325882-1']);
		_gaq.push(['_trackPageview']);
		(function() {
		   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		   ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
		})();
	</script>
	
	<div style="display:none">
		<!-- Google Remarketing -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 990611974;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
	</div>
	<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/990611974/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>

	<script type="text/javascript">
	$('document').ready( function(){

		$('#formPromoModal').submit( function(e){

			$('[required]').each( function(){
				var erros_validacao = false;

				// Verifica campos obrigatórios
				$("[required]").each( function(){
					if($(this).val() == ''){
						alert($(this).attr('data-required-text'));
						e.preventDefault();
						erros_validacao = true;
						return false;
					}
				});

				if(erros_validacao){
					return false;
				}
			});
		});
		
		parent.$('#fancybox-content').height($('.main').height()+60);
        parent.$.fancybox.center();

		$('#btn-close-fancybox').click( function(){
			parent.jQuery.fancybox.close();
		});

	});
	</script>
</body>
</html>