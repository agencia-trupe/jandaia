@section('conteudo')
	
	<div class="centro">

		<h1>CONTATO <span>Fale com a nossa equipe. Aceitamos elogios, sugestões, dúvidas, reclamações ou o que você quiser dizer!</span></h1>

		<div class="grid-contato pure-g-r">
			
			<div class="form pure-u-2-5">

				<div class="pad">
				
					@if(Session::has('envio'))
						@if(Session::get('envio'))
							<div class="resposta sucesso" style="text-align:center;">
								Email de contato enviado com sucesso.<br>
								Agradecemos o contato e responderemos assim que possível.
							</div>
						@else
							<div class="resposta falha" style="text-align:center;">
								Erro ao enviar o email.<br>
								Verifique se todos os campos obrigatórios foram preenchidos.
							</div>
						@endif
					@endif

					<form action="{{ URL::route('consumidor.contato.enviar') }}" method="post" id="form-contato">
						
						<label>
							nome
							<input type="text" name="nome" required id="input-nome">
						</label>

						<label>
							e-mail
							<input type="email" name="email" required id="input-email">
						</label>

						<label>
							telefone
							<input type="text" name="telefone">
						</label>

						<label>
							mensagem
							<textarea name="mensagem" id="input-msg" required></textarea>
						</label>

						<input type="submit" value="ENVIAR">

					</form>
				</div>
			</div>

			<div class="ilustracoes pure-u-3-5">
				
				<div class="pad">
					<div class="ilust1">
						<img src="assets/images/consumidor/layout/contato-ilustra1.png" alt="Entre em Contato">
					</div>
					
					<!--
					<div class="press pure-g-r">
						<h3 class="pure-u-1">Assessoria de Imprensa</h3>
						<div class="pure-u-1-3">
							img-marca-assessoria
						</div>
						<div class="pure-u-2-3">
							<p>
								Nome<br>
								Telefone<br>
								<a href="mailto:" title="Envie um e-mail" target="_blank"></a>
							</p>
						</div>
					</div>
					-->

					<div class="ilust2 pure-g-r">
						<div class="pure-u-1-3 pure-hidden-tablet esconder-tablet">
							<img src="assets/images/consumidor/layout/contato-ilustra2.png">
						</div>
						<div class="pure-u-2-3">
							<h1 style="padding-top:10px">VOCÊ SABIA QUE A JANDAIA TEM UM CANAL DIRETO DE ATENDIMENTO ESPECIALMENTE CRIADO PARA VOCÊ?</h1>
							<h3>
								Se preferir um contato mais direto, acesse nosso Serviço de Atendimento ao Cliente: <a href="mailto:sac@jandaia.com" title="Entre em contato!">sac@jandaia.com</a>
							</h3>
							<h2>SAC 0800 165656</h2>							
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>

@stop