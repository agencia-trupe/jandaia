@section('conteudo')

	<div id="slides">

		@if($banner1D)
			<a href="convitesonedirection" target="_blank" data-id="convites1d" title="Confira a lista de ganhadores dos convites para o show do One Direction" style="background-image:url('assets/images/consumidor/campanha/banner-resultado-convites-one-direction.png')">Confira a lista de ganhadores dos convites para o show do One Direction</a>
		@endif

		<!-- <a href="https://www.youtube.com/cadernosjandaia" target="_blank" data-id="youtube" title="Confira as novidades no nosso Canal do Youtube" style="background-image:url('assets/images/consumidor/campanha/banner-youtube-2.jpg')">Confira as novidades no nosso canal do YouTube</a> -->

		@if($banners)
			@foreach($banners as $b)
				<a href="{{ $b->link }}" data-id="{{ $b->id }}" title="{{ $b->titulo }}" style="background-image:url('assets/images/consumidor/banners/{{ $b->imagem }}')">{{ $b->titulo }}</a>
			@endforeach
		@endif

	</div>

	<div id="carrossel">
		@if($carrosseis)
			<div class="centro">

				@if($banner1D)
					<div class="pure-g" id="carrossel-convites1d">
						@foreach($carrossel1D as $produto)
							<a href="produtos/{{Tipo::find($produto->produtos_tipo_id)->slug}}/{{Linha::find($produto->produtos_linha_id)->slug}}/{{$produto->slug}}" title="<?=$produto->titulo?>" class="pure-u-1-4">
								<div class="thumb">
									<img src="assets/images/produtos/thumbs/<?=$produto->imagem?>" alt="<?=$produto->titulo?>">
								</div>
							</a>
						@endforeach
					</div>
				@endif

				<div class="pure-g" id="carrossel-youtube">
					@foreach($carrosselYoutube as $produto)
						<a href="produtos/{{Tipo::find($produto->produtos_tipo_id)->slug}}/{{Linha::find($produto->produtos_linha_id)->slug}}/{{$produto->slug}}" title="<?=$produto->titulo?>" class="pure-u-1-4">
							<div class="thumb">
								<img src="assets/images/produtos/thumbs/<?=$produto->imagem?>" alt="<?=$produto->titulo?>">
							</div>
						</a>
					@endforeach
				</div>

				@foreach ($carrosseis as $id_banner => $carrossel)
					<div class="pure-g" id="carrossel-{{ $id_banner }}">
						@foreach($carrossel as $thumb)
							<a href="produtos/{{Tipo::find($thumb->produtos_tipo_id)->slug}}/{{Linha::find($thumb->produtos_linha_id)->slug}}/{{$thumb->slug}}" title="<?=$thumb->titulo?>" class="pure-u-1-4">
								<div class="thumb">
									<img src="assets/images/produtos/thumbs/<?=$thumb->imagem?>" alt="<?=$thumb->titulo?>">
								</div>
							</a>
						@endforeach
					</div>
				@endforeach

			</div>
		@endif
	</div>

	<div class="centro">
		<div class="pure-g-r">

			<div class="pure-u-1-2">
				<div class="chamada-produtos">
					<ul>
						<li><a href="produtos/cadernos" title="Cadernos Jandaia">Cadernos</a></li>
						<li><a href="produtos/agendas" title="Agendas Jandaia">Agendas</a></li>
						<li><a href="produtos/bolsas-e-ficharios" title="Fichários Jandaia">Fichários</a></li>
						<li><a href="produtos/bolsas-e-ficharios" title="Bolsas Jandaia">Bolsas</a></li>
					</ul>
					<a href="caderno-completo" title="Um caderno COMPLETO, só a Jandaia tem!" id="link-completo">Um caderno COMPLETO, só a Jandaia tem!</a>
				</div>

				<div class="chamada-youtube esconder-tablet pure-hidden-tablet" style="margin-top:43px;">

					<h2>Novidades do nosso canal no YouTube</h2>

					<iframe id="ytplayer" type="text/html" width="470" height="264.375" src="https://www.youtube.com/embed/?listType=playlist&list=PLUF_8R1u6rWrvhTmMKkCup1Q2ZvPti2ZM" frameborder="0" allowfullscreen></iframe>
					<a href="https://www.youtube.com/cadernosjandaia" title="Acesse nosso canal" class="acessar-youtube" target="_blank">acesse nosso canal &raquo; <img src="assets/images/consumidor/layout/icone-youtube.png" alt="YouTube"></a>

				</div>


			</div>

			<div class="pure-u-1-2">
				<div class="chamada-noticias">

					<div class="padding-noticias">

						<a href="fique-por-dentro" title="Acompanhe as novidades no Blog" class="link-topo">
							<span class="pure-g">
								<div class="pure-u-11-24">
									<span>Fique por dentro</span>
								</div>
								<div class="pure-u-13-24">
									<p>Acompanhe todas as novidades que a Jandaia busca pra você, aqui no nosso blog &raquo;</p>
								</div>
							</span>
						</a>

						<ul class="pure-g-r">
							@if($blog)
								@foreach($blog as $b)
									<li class="pure-u-1">
										<a href="fique-por-dentro/ler/{{$b->slug}}" title="{{$b->titulo}}" class="pure-g">
											<div class="pure-u-1-5">
												<img src="assets/images/consumidor/blog/thumbs/{{$b->imagem}}" alt="{{$b->titulo}}">
											</div>
											<div class="pure-u-4-5">
												<p>
													{{$b->titulo}}
												</p>
											</div>
										</a>
									</li>
								@endforeach
							@endif
						</ul>



						<div class="chamada-temas pure-g-r" style="margin-top: 30px;">

							<h1 class="pure-u-1">É seu! Pode baixar!</h1>

							<div class="pure-u-1-4 tema-thumb tema-angrybirds">
								<div class="link-temas">
									<img src="assets/images/consumidor/temas/thumb_wallpaper_angybird1_1024_768.jpg" alt="Tema Angry Birds">
									<div class="botoes">
										<a href="download/tema/wallpaper_angybird1_1024_768.jpg" title="Download da versão 1024 x 768">1024 x 768</a>
									</div>
								</div>
							</div>

							<div class="pure-u-1-4 tema-thumb tema-angrybirds">
								<div class="link-temas">
									<img src="assets/images/consumidor/temas/thumb_wallpaper_angybird2_1024_768.jpg" alt="Tema Angry Birds">
									<div class="botoes">
										<a href="download/tema/wallpaper_angybird2_1024_768.jpg" title="Download da versão 1024 x 768">1024 x 768</a>
									</div>
								</div>
							</div>

							<div class="pure-u-1-4 tema-thumb tema-angrybirds">
								<div class="link-temas">
									<img src="assets/images/consumidor/temas/thumb_wallpaper_angybird3_1024_768.jpg" alt="Tema Angry Birds">
									<div class="botoes">
										<a href="download/tema/wallpaper_angybird3_1024_768.jpg" title="Download da versão 1024 x 768">1024 x 768</a>
									</div>
								</div>
							</div>

							<div class="pure-u-1-4 tema-thumb tema-cocacola escondido">
								<div class="link-temas">
									<img src="assets/images/consumidor/temas/thumb_wallpaper_cocacola1_1024_768.jpg" alt="Tema Coca-Cola">
									<div class="botoes">
										<a href="download/tema/wallpaper_cocacola1_1024_768.jpg" title="Download da versão 1024 x 768">1024 x 768</a>
									</div>
								</div>
							</div>

							<div class="pure-u-1-4 tema-thumb tema-cocacola escondido">
								<div class="link-temas">
									<img src="assets/images/consumidor/temas/thumb_wallpaper_cocacola2_1024_768.jpg" alt="Tema Coca-Cola">
									<div class="botoes">
										<a href="download/tema/wallpaper_cocacola2_1024_768.jpg" title="Download da versão 1024 x 768">1024 x 768</a>
									</div>
								</div>
							</div>

							<div class="pure-u-1-4 tema-thumb tema-cocacola escondido">
								<div class="link-temas">
									<img src="assets/images/consumidor/temas/thumb_wallpaper_cocacola3_1024_768.jpg" alt="Tema Coca-Cola">
									<div class="botoes">
										<a href="download/tema/wallpaper_cocacola3_1024_768.jpg" title="Download da versão 1024 x 768">1024 x 768</a>
									</div>
								</div>
							</div>

							<div class="pure-u-1-4">
								<div class="box-lista">
									<span>Mais temas:</span>
									<ul style="background:none;">
										<li style="background:none;" class="escondido"><a  style="background:none;" href="#" title="Baixar Temas Angry Birds" data-slug="angrybirds">Angry Birds</a></li>
										<li style="background:none;"><a  style="background:none;" href="#" title="Baixar Temas Coca-Cola" data-slug="cocacola">Coca-Cola</a></li>
									</ul>
								</div>
							</div>
						</div>


					</div>

				</div>
			</div>

		</div>
	</div>


@stop