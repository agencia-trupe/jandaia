@section('conteudo')
	
	<div class="centro">

		<h1>ONDE ENCONTRAR <span>Localize a loja mais próxima onde você pode encontrar os Produtos Jandaia!</span></h1>

		<div class="grid-encontrar pure-g-r">
			
			<div class="form pure-u-2-5">

				<div class="pad">
				
					<form action="{{ URL::route('consumidor.encontrar.buscar') }}" method="get" id="form-encontrar">
						
						<label>
							Selecione a linha de produtos Jandaia que você deseja:
							<select name="linha" required>
								<option value=""></option>
								@if($listaLinhas)
									@foreach($listaLinhas as $k => $v)
										<option value="{{$v->slug}}" @if(isset($objLinha) && !is_null($objLinha) && $objLinha->slug == $v->slug) selected @endif>{{ $v->titulo }}</option>
									@endforeach
								@endif
							</select>
						</label>

						<label>
							Selecione o tipo de produto:
							<select name="tipo" required>
								<option value=""></option>
								@if($listaTipos)
									@foreach($listaTipos as $k => $v)
										<option value="{{$v->slug}}" @if(isset($objTipo) && !is_null($objTipo) && $objTipo->slug == $v->slug) selected @endif>{{ $v->titulo }}</option>
									@endforeach
								@endif
							</select>
						</label>

						<label>
							Indique o Estado, Cidade e o CEP de onde deseja encontrar a loja mais próxima:
						</label>

						<label>
							<select name="estado" id="sel-estado" required>
								<option value="">Estado - Selecione (obrigatório)</option>
								@if($listaEstados)
									@foreach($listaEstados as $k => $v)
										<option value="{{$v->id}}" @if(isset($objEstado) && !is_null($objEstado) && $objEstado->id == $v->id) selected @endif>{{$v->nome}}</option>
									@endforeach
								@endif
							</select>
						</label>

						<label>
							<select name="cidade" id="sel-cidade" @if(isset($objCidade) && !is_null($objCidade)) data-cidade="{{$objCidade->id}}" @endif>
								<option value="">Cidade - Selecione</option>
							</select>
						</label>

						<label>
							<input type="text" name="cep" placeholder="CEP" id="input-cep" @if(isset($cepInformado)) value="{{$cepInformado}}" @endif >
						</label>

						<input type="submit" value="CONSULTAR">

					</form>

					<h1 class="laranja">COMPRE ONLINE!</h1>
					<a href="{{URL::route('consumidor.encontrar.online')}}" title="Sites que vendem nossos produtos" class="botao-laranja">SITES QUE VENDEM NOSSOS PRODUTOS &raquo;</a>
				</div>
			</div>

			<div class="ilustracoes pure-u-3-5">
				
				<div class="pad">
					@if(isset($resultados))
						<h2 class="laranja">RESULTADO DA BUSCA DE LOJAS VIRTUAIS</h2>
						@if(sizeof($resultados) > 0)
							<div class="listaLojasVirtuais pure-g-r">
								@foreach($resultados as $k => $v)
									<div class="pure-u-1-2">
										<div class="internalPad">
											<a href="{{$v->link}}" target="_blank" title="{{$v->titulo}}">
												<div class="imagem">
													<img src="assets/images/consumidor/lojas/{{$v->imagem}}" alt="{{$v->titulo}}">
												</div>
												<h2>{{$v->titulo}}</h2>
											</a>					
										</div>					
									</div>
								@endforeach
							</div>
						@else
							<h3>NENHUMA LOJA VIRTUAL ENCONTRADA</h3>
						@endif
					@endif
				</div>

			</div>

		</div>
	</div>

@stop

link
titulo
imagem