@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Categoria
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.categorias.update', $categoria->id), 'files' => false, 'method' => 'put', 'id' => 'form-alter-categorias') ) }}

			<label>Título<br>
			<input type="text" name="titulo" value="{{ $categoria->titulo }}" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop