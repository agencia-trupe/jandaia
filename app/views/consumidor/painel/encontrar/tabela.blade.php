<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	    <title>Painel Administrativo - Concurso #TôDentro Jandaia</title>

		<meta name="robots" content="noindex, nofollow" />
		<meta name="author" content="Trupe Design" />
		<meta name="copyright" content="2013 Trupe Design" />
		<meta name="viewport" content="width=device-width,initial-scale=1">

		<base href="{{ url() }}/">
		<script> var BASE = "{{ url() }}/"</script>

		<style>
			body{
				width:98%;
			}
			body table{
				width:100%;
				font-size:11px;
				border-collapse: collapse;
			}
			body table tr{

			}
			body table tr td{
				font-size:10px;
				padding:2px 0;
				border:1px #CCC solid;
			}
		</style>
	</head>
	<body>
		@if($registros)
			
			<table>

				<thead>
					<tr>
						<th>Tipo</th>
						<th>Linha</th>
						<th>Razão Social</th>
						<th>Endereço</th>
						<th>Bairro</th>
						<th>CEP</th>
						<th>Cidade</th>
						<th>UF</th>
						<th>Telefone</th>
					</tr>
				</thead>

				<tbody>
					@foreach($registros as $value)						
						<tr>
							<td>{{ $value->tipo }}</td>
							<td>{{ $value->linha }}</td>
							<td>{{ $value->razao_social }}</td>
							<td>{{ $value->endereco }}</td>
							<td>{{ $value->bairro }}</td>
							<td>{{ $value->cep }}</td>
							<td>{{ $value->cidade }}</td>
							<td>{{ $value->uf }}</td>
							<td>{{ $value->telefone }}</td>
						</tr>						
					@endforeach
				</tbody>

			</table>

		@endif
	</body>
</html>