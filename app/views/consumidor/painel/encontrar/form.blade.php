@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Lista
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.encontrar.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-lista') ) }}

			<label>Descritivo da Lista (<small><em>será utilizado somente para referência dentro do painel administrativo</em></small>)<br>
			<input type="text" name="descritivo" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<div class="alert alert-info dont-dismiss">
					<h4>Importante</h4>
					<ul>
						<li>Arquivo .CSV com no máximo 10.000 linhas, <strong>separado por vírgula<strong></li>
						<li>Para separar o arquivo : <span class="code">split -d -l 10000 arquivo.csv part (depois renomear com a extensão)</span></li>
						<li>Ordem dos Campos: "CATEGORIA",LINHA,EMPRESA,ENDEREÇO,BAIRRO,CEP,CIDADE,ESTADO,TELEFONE</li>
					</ul>
			</div>
			<label style="width:340px">
				Arquivo<br>
				<input type="file" name="arquivo" required>
			</label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary disable-after-submit')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop