@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Onde Encontrar - Lojas Virtuais <a href="{{ URL::route('painel.encontraronline.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Loja</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

          @if(sizeof($registros) > 0)
      		  
            <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="lojas_online">
              <thead>
                <tr>
                  <th>Ordenar</th>
                  <th>Título</th>
                  <th><i class="icon-cog"></i></th>
                </tr>
              </thead>

              <tbody>
              @foreach ($registros as $registro)

               	<tr class="tr-row" id="row_{{ $registro->id }}">
                  <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
                  <td>{{ $registro->titulo }}</td>                  	    
               		<td class="crud-actions">
                 		<a href="{{ URL::route('painel.encontraronline.edit', $registro->id ) }}" class="btn btn-info">editar</a>

                	   {{ Form::open(array('route' => array('painel.encontraronline.destroy', $registro->id), 'method' => 'delete')) }}
                            <button type="submit" href="{{ URL::route( 'painel.encontraronline.destroy', $registro->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
                     {{ Form::close() }}
              		</td>
                </tr>

              	@endforeach
              </tbody>
            </table>

          @else
          
            <h3>Nenhuma loja encontrada</h3>

          @endif
        

    	</div>
  	</div>

@stop