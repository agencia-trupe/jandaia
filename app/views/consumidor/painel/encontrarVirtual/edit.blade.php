@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Categoria
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.encontraronline.update', $registro->id), 'files' => true, 'method' => 'put', 'id' => 'form-alter-registros') ) }}

			<label>Título<br>
			<input type="text" name="titulo" value="{{ $registro->titulo }}" required required-message="O título é obrigatório!" class="input-xxlarge"></label>
			
			<label>Endereço (URL)<br>
			<input type="text" name="link" value="{{$registro->link}}" required required-message="O endereço é obrigatório!" class="input-xxlarge"></label>

			<label>Imagem<br>
			@if($registro->imagem)
				<img src="assets/images/consumidor/lojas/{{ $registro->imagem }}"><br>
				<a href="assets/images/consumidor/lojas/{{ $registro->imagem }}" title="ver tamanho original" target="_blank">ver tamanho original</a><br>
			@endif
			<input type="file" name="imagem"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop