@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Posts <a href="{{ URL::route('painel.blog.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Post</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed">

          		<thead>
            		<tr>
                        <th>Categoria</th>
                        <th>Título</th>
                        <th>Data</th>
                  		<th>Texto</th>
                  		<th><i class="icon-cog"></i></th>
                	</tr>
          		</thead>

          		<tbody>
            	@foreach ($blogs as $blog)

                	<tr class="tr-row" id="row_{{ $blog->id }}">
                        <td>{{ BlogCategoria::find($blog->blog_categorias_id)->titulo }}</td>
                        <td>{{ $blog->titulo }}</td>
                  	    <td>{{ Tools::converteData($blog->data) }}</td>
                        <td>{{ Str::words( strip_tags($blog->texto), 10 ) }}</td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.blog.edit', $blog->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.blog.destroy', $blog->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.blog.destroy', $blog->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>

        {{ $blogs->links() }}

    	</div>
  	</div>

@stop