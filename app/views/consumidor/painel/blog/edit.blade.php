@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Post
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.blog.update', $post->id), 'files' => true, 'method' => 'put', 'id' => 'form-alter-blog') ) }}

			<label>Título<br>
			<input type="text" name="titulo" value="{{ $post->titulo }}" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<label>Data<br>
			<input type="text" name="data" value="{{ Tools::converteData($post->data) }}" class="datepicker" required required-message="A Data é obrigatória!"></label>

			<label>Categoria<br>
				<select name="blog_categorias_id" required required-message="A Categoria é obrigatória!">
					<option value="">Selecione uma Categoria</option>
					@if($categorias)
					    @foreach($categorias as $categoria)
					    	<option value="{{ $categoria->id }}" @if($categoria->id == $post->blog_categorias_id) selected @endif>{{ $categoria->titulo }}</option>
					    @endforeach
					@endif
				</select>
			</label>

			<label>Imagem<br>
			@if($post->imagem)
				<img src="assets/images/consumidor/blog/thumbs/{{ $post->imagem }}"><br>
			@endif
			<input type="file" name="imagem"></label>

			<label>Texto<br>
			<textarea name="texto" class="grande blog">{{ stripslashes(str_replace('../../', '../../../', $post->texto)) }}</textarea></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop