<!DOCTYPE html>
<html lang="pt-BR" class="login">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Painel Administrativo - Jandaia Cadernos</title>

	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2012 True Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<base href="{{ url() }}/">
	<script> var BASE = "{{ url() }}"</script>

<?= Assets::CSS(array('painel/css/global', 	'jquery-theme-lightness/jquery-ui-1.8.20.custom')) ?>

<?= Assets::JS(array(
	'libs/modernizr-2.0.6.min',
	'libs/jquery-1.8.0.min',
	'plugins/tinymce/tiny_mce',
	'libs/jquery-ui-1.10.1.custom.min',
	'plugins/jquery.ui.datepicker-pt-BR',
	'plugins/jquery.mtz.monthpicker',
	'plugins/jquery.uploadify.min',
	'plugins/bootstrap',
	'plugins/bootbox.min',
	'scripts/painel'))
?>

</head>
	<body>

		<div class="modal" id="modal-login" style="">

			{{ Form::open( array('route' => 'consumidor.painel.auth', 'method' => 'post', 'class' => 'form-horizontal') ) }}

			    <div class="modal-header">
			      	<h3 style="text-align:center">
			        	Painel Administrativo <br> Jandaia Cadernos
			      	</h3>
			    </div>

				<div class="modal-body">

					<fieldset>

						@if(Session::has('login_errors'))
							<div class="alert alert-block alert-error fade in">Usuário ou Senha incorretos</div>
						@endif

				        <div class="control-group">
				        	{{ Form::label('login-username', 'Usuário', array("class" => "control-label")) }}
				          	<div class="controls">
				            	<input class="large validates[required]" required id="login-username" name="username" size="30" type="text" autocomplete="off">
				          	</div>
				        </div>

				        <div class="control-group">
				          	{{ Form::label('login-password', 'Senha', array("class" => "control-label")) }}
				          	<div class="controls">
				            	<input class="large validates[required]" required id="login-password" name="password" size="30" type="password">
				          	</div>
				        </div>

					</fieldset>

				</div>

				<div class="modal-footer">
					{{ Form::submit('Login', array('class' => 'btn btn-primary')) }}
			    </div>

			{{ Form::close() }}

		</div>

	</body>
</html>