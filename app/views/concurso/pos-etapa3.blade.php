@section('conteudo')

<div class="secao home" name="home">

	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home ativo">Concurso #ToDentro</a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">

		<div class="pure-u-1-2">
			<div class="banner-principal">
				<img src="assets/images/concurso/lully-home.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor">
			</div>
		</div>

		<div class="pure-u-1-4">
			<div class="chamada-tema">
				<h1> <span class="menor">NOSSO CONCURSO CULTURAL SE ENCERROU E VOCÊ PODE CONFERIR OS GANHADORES NA PÁGINA ‘PRÊMIOS’. ACOMPANHE A JANDAIA NAS REDES SOCIAIS PARA PARTICIPAR DOS PRÓXIMOS CONCURSOS E ESCREVER SEMPRE UM MUNDO MELHOR PARA TODOS!</span></h1>
				<div class="header-tema">
					CONCURSO FINALIZADO
				</div>
			</div>
		</div>

		<div class="pure-u-1-4">
			<div class="banner-lateral">
				<img src="assets/images/concurso/premios-home.png" alt="Prêmio do Concurso Cultural Jandaia #TôDentro de um mundo melhor">
			</div>
		</div>
	</div>

	<footer>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

<div class="secao participe" name="participe">

	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home"><img src="assets/images/concurso/marca-todentro.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor"></a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe ativo">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">
		
		<div class="pure-u-13-24">
			<div id="inscricoes-fechadas">
				<p>
					As inscrições para o <strong>Concurso Cultural #TôDentro de um Mundo Melhor</strong> se encerraram. Mas você pode continuar pensando em como colaborar com o planeta sempre! Compartilhe suas ideias com amigos, com seus pais e seus professores. Todos podemos Fazer um Mundo Melhor!!!
 				</p>
 				<p class="on">
					Confira as datas de divulgação de resultados e os premiados <a href="#premios" class="link-finalizado" title="Ver Resultados">aqui</a>!
				</p>
				<p>
					Fique sempre de olho no site <a href="http://www.cadernosjandaia.com.br" target="_blank" title="Cadernos Jandaia">cadernosjandaia.com.br</a> para saber dos próximos concursos, e concorrer a prêmios incríveis!!
				</p>
			</div>
		</div>

		<div class="pure-u-11-24">
			<h2 class="participe-premios">
				CONCORRA A PRÊMIOS!
			</h2>
			<div class="tema-do-momento"></div>
			<div class="espacamento"></div>
		</div>
		
	</div>

	<footer>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

<div class="secao premios" name="premios">
	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home"><img src="assets/images/concurso/marca-todentro.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor"></a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios ativo">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">

		<div class="pure-u-5-8">
			<div class="premiacao">
				<p>
					Em cada etapa você pode ganhar:
				</p>
				<p class="detalhe-premio">
					1o lugar <span class="titulo-premio">1 XBOX ONE</span>
				</p>
				<p class="detalhe-premio">
					2o lugar <span class="titulo-premio">1 TABLET</span>
				</p>
				<p class="detalhe-premio">
					3o lugar <span class="titulo-premio">1 SKATE</span>
				</p>
			</div>
		</div>
		<div class="pure-u-3-8">
			<div class="premiados">
				<h2>DIVULGAÇÃO DOS PREMIADOS!</h2>
				<p class="sem-margem">A cada final de etapa do Concurso os premiados são divulgados aqui.</p>				
			</div>
		</div>

		<div class="vencedores pure-u-1" name="vencedores">
			
			<h1>CONHEÇA OS VENCEDORES DA 3&ordf; ETAPA!</h1>
			<h2>É Proibido Poluir</h2>

			<div class="pure-g-r grid-vencedores">
				<div class="pure-u-1-3">
					<h3>1&ordm; LUGAR</h3>
					<p>
						De bicicleta, skate ou caminhando pelo chão,<br>
						cuido do planeta, meu combustível<br>
						é só garrafinha d’água na mão.<br>
						#todentro : abaixo a poluição!
					</p>
					<p class="nome">
						André Augusto da Silva<br>
						Salvador - BA
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>2&ordm; LUGAR</h3>
					<p>
						Pensando no futuro, comprei uma<br>
						bicicleta; ela cuida da minha saúde<br>
						e eu da saúde do planeta;<br>
						minha parte tô fazendo, diga também #todentro
					</p>
					<p class="nome">
						Maria José Cardoso Da Silva<br>
						São Pedro da Aldeia – RJ
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>3&ordm; LUGAR</h3>
					<p>
						De carro queimo dinheiro,<br>
						economizo gordura,<br>
						prejudico o planeta.<br>
						De bicicleta queimo gordura,<br>
						economizo dinheiro,<br>
						ajudo o planeta #todentro
					</p>
					<p class="nome">
						Gisele Christine da Silva<br>
						Praia Grande – SP
					</p>
				</div>
			</div>
		</div>	

		<div class="vencedores pure-u-1" name="vencedores">
			
			<h1>CONHEÇA OS VENCEDORES DA 2&ordf; ETAPA!</h1>
			<h2>Corrente do Bem</h2>

			<div class="pure-g-r grid-vencedores">
				<div class="pure-u-1-3">
					<h3>1&ordm; LUGAR</h3>
					<p>
						Ajudar é cantar pra alguém<br>
						mesmo sendo desafinado,<br>
						fazer o outro saber que é amado.<br>
						O sorriso como talento<br>
						e o amor o epicentro, #todentro!
					</p>
					<p class="nome">
						Karen Keslen Kremes<br>
						Ponta Grossa – PR
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>2&ordm; LUGAR</h3>
					<p>
						Aos pobres alimento,<br>
						Aos doentes dou sustento,<br>
						A quem precisa dedico tempo,<br>
						Às crianças histórias invento,<br>
						E elas respondem: #todentro!
					</p>
					<p class="nome">
						Jennifer Rocha de Oliveira<br>
						Brasília – DF
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>3&ordm; LUGAR</h3>
					<p>
						Eu ajudo o meu amigo a ir para escola<br>
						e voltar para casa com o dinheiro<br>
						do meu lanche.<br>
						A mãe dele não tem condições e moram longe.
						#todentro
					</p>
					<p class="nome">
						Bárbara Marques Ferreira<br>
						Boa Vista – RR
					</p>
				</div>
			</div>
		</div>	


		<div class="vencedores pure-u-1">
			
			<h1>CONHEÇA OS VENCEDORES DA 1&ordf; ETAPA!</h1>
			<h2>Amigos da Natureza</h2>

			<div class="pure-g-r grid-vencedores">
				<div class="pure-u-1-3">
					<h3>1&ordm; LUGAR</h3>
					<p>
						Desperdício não aguento,<br>
						apago a luz do aposento,<br>
						não gasto água muito tempo,<br>
						reciclo com talento.<br>
						Pra cuidar do meio ambiente,<br>
						#todentro !
					</p>
					<p class="nome">
						Náira Ravena A. Araújo<br>
						Salvador – BA
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>2&ordm; LUGAR</h3>
					<p>
						Cada papel jogado no CHÃO<br>
						geraria uma INFRAÇÃO,<br>
						teria que pagar a MULTA:<br>
						Plantar e regar uma MUDA!<br>
						Imagina isso no CENTRO!?<br>
						Eu #todentro !
					</p>
					<p class="nome">
						Patricia Freitas<br>
						Belo Horizonte – MG
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>3&ordm; LUGAR</h3>
					<p>
						Tomo sempre atitude,<br>
						sou um jovem consciente.<br>
						Não poluo as nossas ruas,<br>
						não sujo o ambiente.<br>
						Cuido da natureza,<br>
						como um grande presente!<br>
						#todentro						 
					</p>
					<p class="nome">
						Wallisson Felipe Alves<br>
						Fortaleza – CE
					</p>
				</div>
			</div>

			<h1 class="roxo">PARABÉNS AOS VENCEDORES!!!</h1>			
		</div>			
		
	</div>

	<footer>
		<div class="assinatura pure-g">
			<p class="pure-u-1-2">
				&copy; 2013 Cadernos Jandaia<br>
				Todos os direitos reservados
			</p>
			<p class="pure-u-1-2">
				11 0800 165656<br>
				<a href="mailto:sac@cadernosjandaia.com.br" title="Entre em contato via email">sac@cadernosjandaia.com.br</a>
			</p>
			<div class="pure-u-1">
				<div class="trupe">
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites e comunicação: Trupe Agência Criativa">Criação de sites e comunicação:</a>
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites e comunicação: Trupe Agência Criativa">Trupe Agência Criativa <img src="assets/images/concurso/marca-trupe-kombi.png" alt="Criação de site e comunicação: Trupe Agência Criativa"></a>
					<br>
					<a href="http://ph2.com.br/site/" target="_blank" title="Campanha: Ph2 Full Creativity">Campanha:</a>
					<a href="http://ph2.com.br/site/" target="_blank" title="Campanha: Ph2 Full Creativity"> Ph2full creativity</a>
				</div>
			</div>
		</div>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

@stop