@section('conteudo')

<div class="secao home" name="home">

	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home ativo">Concurso #ToDentro</a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">

		<div class="pure-u-1-2">
			<div class="banner-principal">
				<img src="assets/images/concurso/lully-home.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor">
			</div>
			<div class="infografico">
				<img src="assets/images/concurso/infografico-home.png" alt="Infográfico do Concurso Cultural #TôDentro de um mundo melhor">
			</div>
		</div>

		<div class="pure-u-1-4">
			<div class="chamada-tema">
				<h1>E AÍ, TÁ A FIM DE <span class="roxo">GANHAR PRÊMIOS</span> <span class="menor">AJUDANDO A ESCREVER UM MUNDO</span> MELHOR PRA TODOS?</h1>
				<div class="header-tema">
					TEMA DO MOMENTO &bull; ETAPA 2
				</div>
				<div class="titulo-tema">
					CORRENTE DO BEM
				</div>
				<div class="data-tema">
					de 13 a 31 de jan 2014
				</div>
				<div class="detalhe-tema">
					<p class="bold">
						O que você faz pra ajudar as pessoas?
					</p>
					<p>
						Suas boas ações podem render prêmios pra você. E quem sabe, mudar o mundo!
					</p>
					<a href="#participe" class="link" title="Envie sua frase!">ENVIAR FRASE &raquo;</a>
				</div>
			</div>
		</div>

		<div class="pure-u-1-4">
			<div class="banner-lateral">
				<img src="assets/images/concurso/premios-home.png" alt="Prêmio do Concurso Cultural Jandaia #TôDentro de um mundo melhor">
			</div>
		</div>
	</div>

	<footer>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

<div class="secao participe" name="participe">

	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home"><img src="assets/images/concurso/marca-todentro.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor"></a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe ativo">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">
		
		<div class="pure-u-13-24">
			<form id="inscricao" method="post" action="">
				
				<div class="container-frase">
					<label for="textarea-frase">
						Sua frase de até 140 caracteres com a hashtag #todentro:
					</label>
					<textarea name="frase" id="textarea-frase" required data-required-msg="Informe uma frase com a hashtag #todentro para poder participar!" maxlength="140"></textarea>
					<div id="contador">Restam <span>140</span> caracteres</div>
					<div class="aviso-tema">
						<span>Lembre-se de conferir o Tema do Momento</span>
					</div>
				</div>

				<div class="titulo-dados">
					DADOS DE IDENTIFICAÇÃO:
				</div>
				
				<div class="linha-form">
					<label for="input-nomecompleto">nome completo:</label>
					<input type="text" name="nome_completo" id="input-nomecompleto" required data-required-msg="Informe seu Nome Completo!">
				</div>

				<div class="linha-form">
					<label for="input-email">e-mail:</label>
					<input type="email" name="email" id="input-email" required data-required-msg="Informe seu e-mail!">
				</div>

				<div class="linha-form">
					<label for="input-telefone">telefone:</label>
					<input type="text" name="telefone" id="input-telefone" required data-required-msg="Informe um telefone para contato!">
				</div>

				<div class="linha-form">
					<label for="input-cep">CEP:</label>
					<input type="text" name="cep" id="input-cep" required data-required-msg="Informe seu CEP!">					
				</div>

				<div class="linha-form cpf">
					<label for="input-cpf">CPF:</label>
					<input type="text" name="cpf" id="input-cpf">
					<div id="abrir-responsavel">Se você não possui CPF <a href="#" title="Utilizar o CPF de um Responsável">clique aqui</a>.</div>
				</div>

				<div class="responsavel escondido">
					<p class="laranja">
						Se você não possui CPF pode fornecer o CPF de um responsável.<br>
						Preencha os campos abaixo:
					</p>
					<div class="linha-form">
						<label for="input-nomecompletoresponsavel">nome completo do responsável:</label>
						<input type="text" name="nome_completo_responsavel" id="input-nomecompletoresponsavel">
					</div>
					<div class="linha-form">
						<label for="input-cpfresponsavel">CPF do responsável:</label>
						<input type="text" name="cpf_responsavel" id="input-cpfresponsavel">
					</div>
					<div class="linha-form">
						<label for="input-data">sua data de nascimento:</label>
						<input type="text" name="data_nascimento" id="input-data">
					</div>

					<div id="fechar-responsavel"><a href="#" title="FECHAR esta sessão e voltar ao formulário original.">FECHAR esta sessão e voltar ao formulário original.</a></div>
				</div>

				@include('concurso.regulamento')
				
				<div class="checkboxes">
					<label><input type="checkbox" name="aceite_regulamento" id="check-aceite" value="1" required data-required-msg="É necessário ler e aceitar o regulamento para poder participar!"> Li e aceito o regulamento.</label>
					<label><input type="checkbox" name="inscricao_newsletter" id="check-news" value="1"> Quero receber mais promoções e informações sobre a Jandaia.</label>
				</div>

				<input type="submit" value="ENVIAR">

			</form>
		</div>

		<div class="pure-u-11-24">
			<h2 class="participe-premios">
				CONCORRA A PRÊMIOS!
			</h2>
			<div class="tema-do-momento">
				<p class="titulo-tema">
					CORRENTE DO BEM
				</p>
				<p class="data-tema">
					de 13 a 31 de jan 2014
				</p>
				<p class="descricao-tema">
					O que você faz pra ajudar as pessoas?</strong>
					<br>
					Suas boas ações podem render prêmios pra você. E quem sabe, mudar o mundo!
				</p>
				<p class="rosa">
					CRIE SUA FRASE BASEADA NO TEMA ACIMA E ENVIE AGORA!
				</p>
			</div>
			<div class="lully">
				<img src="assets/images/concurso/lully-participe.png" alt="Participe do concurso cultural Jandaia #Tôdentro de um mundo melhor">
			</div>
		</div>
		
	</div>

	<footer>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

<div class="secao premios" name="premios">
	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home"><img src="assets/images/concurso/marca-todentro.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor"></a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios ativo">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">

		<div class="pure-u-5-8">
			<div class="premiacao">
				<p>
					Em cada etapa você pode ganhar:
				</p>
				<p class="detalhe-premio">
					1o lugar <span class="titulo-premio">1 XBOX ONE</span>
				</p>
				<p class="detalhe-premio">
					2o lugar <span class="titulo-premio">1 TABLET</span>
				</p>
				<p class="detalhe-premio">
					3o lugar <span class="titulo-premio">1 SKATE</span>
				</p>
			</div>
		</div>
		<div class="pure-u-3-8">
			<div class="resultados">
				<h2>CONFIRA OS RESULTADOS!</h2>
				<p class="sem-margem">O Concurso tem 3 etapas e 3 datas de divulgação de resultados. Acompanhe:</p>
				<p>
					<span class="titulo">ETAPA 1</span>
					<br>
					<span class="destaque">INSCRIÇÕES:</span> de 12 dez 2013 a 10 jan 2014
					<br>
					<span class="destaque">Divulgação de Resultados: 17 jan 2014</span>
				</p>
				<p>
					<span class="titulo branco">ETAPA 2</span>
					<br>
					<span class="destaque">INSCRIÇÕES:</span> de 13 a 31 jan 2014
					<br>
					<span class="destaque">Divulgação de Resultados:</span> <span class="destaque branco">7 fev 2014</span>
				</p>
				<p>
					<span class="titulo">ETAPA 3</span>
					<br>
					<span class="destaque">INSCRIÇÕES:</span> de 3 a 21 fev 2014
					<br>
					<span class="destaque">Divulgação de Resultados: 27 fev 2014</span>
				</p>
			</div>
			<div class="premiados">
				<h2>DIVULGAÇÃO DOS PREMIADOS!</h2>
				<p class="sem-margem">A cada final de etapa do Concurso os premiados serão divulgados aqui.</p>
				<p>
					<span class="destaque">FIQUE ATENTO!</span><br>
					O próximo resultado será divulgado em <br><span class="branco">7 fev 2014</span>.
				</p>
			</div>
		</div>

		<div class="vencedores pure-u-1" name="vencedores">
			
			<h1>CONHEÇA OS VENCEDORES DA 1&ordf; ETAPA!</h1>
			<h2>Amigos da Natureza</h2>

			<div class="pure-g-r grid-vencedores">
				<div class="pure-u-1-3">
					<h3>1&ordm; LUGAR</h3>
					<p>
						Desperdício não aguento,<br>
						apago a luz do aposento,<br>
						não gasto água muito tempo,<br>
						reciclo com talento.<br>
						Pra cuidar do meio ambiente,<br>
						#todentro !
					</p>
					<p class="nome">
						Náira Ravena A. Araújo<br>
						Salvador – BA
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>2&ordm; LUGAR</h3>
					<p>
						Cada papel jogado no CHÃO<br>
						geraria uma INFRAÇÃO,<br>
						teria que pagar a MULTA:<br>
						Plantar e regar uma MUDA!<br>
						Imagina isso no CENTRO!?<br>
						Eu #todentro !
					</p>
					<p class="nome">
						Patricia Freitas<br>
						Belo Horizonte – MG
					</p>
				</div>
				<div class="pure-u-1-3">
					<h3>3&ordm; LUGAR</h3>
					<p>
						Tomo sempre atitude,<br>
						sou um jovem consciente.<br>
						Não poluo as nossas ruas,<br>
						não sujo o ambiente.<br>
						Cuido da natureza,<br>
						como um grande presente!<br>
						#todentro						 
					</p>
					<p class="nome">
						Wallisson Felipe Alves<br>
						Fortaleza – CE
					</p>
				</div>
			</div>

			<h1 class="roxo">PARABÉNS AOS VENCEDORES!!!</h1>
			<h1 class="rosa">PARTICIPE VOCÊ TAMBÉM!</h1>
		</div>		
		
	</div>

	<footer>
		<div class="assinatura pure-g">
			<p class="pure-u-1-2">
				&copy; 2013 Cadernos Jandaia<br>
				Todos os direitos reservados
			</p>
			<p class="pure-u-1-2">
				11 0800 165656<br>
				<a href="mailto:sac@cadernosjandaia.com.br" title="Entre em contato via email">sac@cadernosjandaia.com.br</a>
			</p>
			<div class="pure-u-1">
				<div class="trupe">
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites e comunicação: Trupe Agência Criativa">Criação de sites e comunicação:</a>
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites e comunicação: Trupe Agência Criativa">Trupe Agência Criativa <img src="assets/images/concurso/marca-trupe-kombi.png" alt="Criação de site e comunicação: Trupe Agência Criativa"></a>
					<br>
					<a href="http://ph2.com.br/site/" target="_blank" title="Campanha: Ph2 Full Creativity">Campanha:</a>
					<a href="http://ph2.com.br/site/" target="_blank" title="Campanha: Ph2 Full Creativity"> Ph2full creativity</a>
				</div>
			</div>
		</div>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

@stop