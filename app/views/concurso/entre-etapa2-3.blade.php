@section('conteudo')

<div class="secao home" name="home">

	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home ativo">Concurso #ToDentro</a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">

		<div class="pure-u-1-2">
			<div class="banner-principal">
				<img src="assets/images/concurso/lully-home.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor">
			</div>
			<div class="infografico">
				<img src="assets/images/concurso/infografico-home.png" alt="Infográfico do Concurso Cultural #TôDentro de um mundo melhor">
			</div>
		</div>

		<div class="pure-u-1-4">
			<div class="chamada-tema">
				<h1>E AÍ, TÁ A FIM DE <span class="roxo">GANHAR PRÊMIOS</span> <span class="menor">AJUDANDO A ESCREVER UM MUNDO</span> MELHOR PRA TODOS?</h1>
				<div class="header-tema">
					TEMA DO MOMENTO &bull; ETAPA 3
				</div>
				<div class="titulo-tema">
					É PROIBIDO POLUIR
				</div>
				<div class="data-tema">
					de 3 a 21 de fev 2014
				</div>
				<div class="detalhe-tema sem-botao">
					<p class="bold">
						CARRO + ÔNIBUS + TRÂNSITO = POLUIÇÃO DO AR
					</p>
					<p>
						Sugira um jeito criativo de ir pra escola ou andar pela cidade sem fazer mal ao planeta.
					</p>
				</div>
			</div>
		</div>

		<div class="pure-u-1-4">
			<div class="banner-lateral">
				<img src="assets/images/concurso/premios-home.png" alt="Prêmio do Concurso Cultural Jandaia #TôDentro de um mundo melhor">
			</div>
		</div>
	</div>

	<footer>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

<div class="secao participe" name="participe">

	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home"><img src="assets/images/concurso/marca-todentro.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor"></a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe ativo">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">
		
		<div class="pure-u-13-24">
			<div id="inscricoes-fechadas">
				<p>
					As inscrições para a ETAPA 2 se encerraram.<br>
					O resultado será divulgado dia <span class="branco">7 fev 2014</span>.
				</p>
				 
				<p>
					Você pode se inscrever para a próxima ETAPA a partir de <span class="branco">3 fev 2014</span>, quando as inscrições estarão abertas para a ETAPA 3.					
				</p>
				 
				<p>
					O tema da próxima ETAPA é:
				</p>
				
				<p>
					<strong>É PROIBIDO POLUIR</strong> <br>
					de 3 a 21 de fev 2014 <br>
					CARRO + ÔNIBUS + TRÂNSITO = POLUIÇÃO DO AR <br>
					Sugira um jeito criativo de ir pra escola ou andar pela cidade sem fazer mal ao planeta.
				</p>
			</div>
		</div>

		<div class="pure-u-11-24">
			<h2 class="participe-premios">
				CONCORRA A PRÊMIOS!
			</h2>
			<div class="tema-do-momento"></div>
			<div class="espacamento"></div>
		</div>
		
	</div>

	<footer>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

<div class="secao premios" name="premios">
	<header>
		<div class="social">
			<div class="box">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-layout="button_count" style="margin-right:15px;" data-action="like" data-show-faces="false" data-share="false"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
			</div>
		</div>

		<nav>
			<ul>
				<li><a href="#home" title="Página Inicial do Concurso" class="mn-home"><img src="assets/images/concurso/marca-todentro.png" alt="Concurso Cultural Jandaia - #TôDentro de um mundo melhor"></a></li>
				<li><a href="#regulamento" title="Regulamento" class="mn-regulamento">Regulamento</a></li>
				<li><a href="#participe" title="Participe!" class="mn-participe">Participe!</a></li>
				<li><a href="#premios" title="Prêmios" class="mn-premios ativo">Prêmios</a></li>
			</ul>
		</nav>
	</header>

	<div class="centro pure-g-r">

		<div class="pure-u-5-8">
			<div class="premiacao">
				<p>
					Em cada etapa você pode ganhar:
				</p>
				<p class="detalhe-premio">
					1o lugar <span class="titulo-premio">1 XBOX ONE</span>
				</p>
				<p class="detalhe-premio">
					2o lugar <span class="titulo-premio">1 TABLET</span>
				</p>
				<p class="detalhe-premio">
					3o lugar <span class="titulo-premio">1 SKATE</span>
				</p>
			</div>
		</div>
		<div class="pure-u-3-8">
			<div class="resultados">
				<h2>CONFIRA OS RESULTADOS!</h2>
				<p class="sem-margem">O Concurso tem 3 etapas e 3 datas de divulgação de resultados. Acompanhe:</p>
				<p>
					<span class="titulo">ETAPA 1</span>
					<br>
					<span class="destaque">INSCRIÇÕES:</span> de 12 dez 2013 a 10 jan 2014
					<br>
					<span class="destaque">Divulgação de Resultados: 17 jan 2014</span>
				</p>
				<p>
					<span class="titulo">ETAPA 2</span>
					<br>
					<span class="destaque">INSCRIÇÕES:</span> de 13 a 31 jan 2014
					<br>
					<span class="destaque">Divulgação de Resultados: 7 fev 2014</span>
				</p>
				<p>
					<span class="titulo branco">ETAPA 3</span>
					<br>
					<span class="destaque">INSCRIÇÕES:</span> de 3 a 21 fev 2014
					<br>
					<span class="destaque">Divulgação de Resultados:</span> <span class="destaque branco">27 fev 2014</span>
				</p>
			</div>
			<div class="premiados">
				<h2>DIVULGAÇÃO DOS PREMIADOS!</h2>
				<p class="sem-margem">A cada final de etapa do Concurso os premiados serão divulgados aqui.</p>
				<p>
					<span class="destaque">FIQUE ATENTO!</span><br>
					O próximo resultado será divulgado em <br><span class="branco">27 fev 2014</span>.
				</p>
			</div>
		</div>
		
	</div>

	<footer>
		<div class="assinatura pure-g">
			<p class="pure-u-1-2">
				&copy; 2013 Cadernos Jandaia<br>
				Todos os direitos reservados
			</p>
			<p class="pure-u-1-2">
				11 0800 165656<br>
				<a href="mailto:sac@cadernosjandaia.com.br" title="Entre em contato via email">sac@cadernosjandaia.com.br</a>
			</p>
			<div class="pure-u-1">
				<div class="trupe">
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites e comunicação: Trupe Agência Criativa">Criação de sites e comunicação:</a>
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites e comunicação: Trupe Agência Criativa">Trupe Agência Criativa <img src="assets/images/concurso/marca-trupe-kombi.png" alt="Criação de site e comunicação: Trupe Agência Criativa"></a>
					<br>
					<a href="http://ph2.com.br/site/" target="_blank" title="Campanha: Ph2 Full Creativity">Campanha:</a>
					<a href="http://ph2.com.br/site/" target="_blank" title="Campanha: Ph2 Full Creativity"> Ph2full creativity</a>
				</div>
			</div>
		</div>
		<div class="promocao">
			Promovido por: <a href="http://cadernosjandaia.com.br" title="Cadernos Jandaia"><img src="assets/images/concurso/marca-jandaia.png" alt="Cadernos Jandaia"></a>
		</div>
	</footer>
</div>

@stop