@extends('templates.representantes')

@section('conteudo')
	<div class="espacador">
		<h1>Quer saber como produzimos os nossos cadernos?</h1>
		<h2>No vídeo abaixo você vai conhecer as instalações da fábrica da Jandaia e entender um pouco mais sobre o processo de fabricação dos nossos produtos. Clique e assista!</h2>
		<iframe width="644" height="420" src="//www.youtube.com/embed/DWIHCx6uajA?autoplay=1" frameborder="0" allowfullscreen></iframe>
	</div>
@stop