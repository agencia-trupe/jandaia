@section('conteudo')

<div id="banners">
	<div id="fsc">
		<a href="empresa" title="Conheça a história da Jandaia">
			<img src="assets/images/home/bannerHome-60anos.png">
		</a>
	</div>
	<div id="slides">
		<div id="pager"></div>
		<div class="animate">
			<a href="empresa/lab" title="Jandaia Lab" class="slide escondido-desktop">
				<img src="assets/images/banners/banner-jandaialab.png" alt="Jandaia Lab">
				<h1>JANDAIA LAB</h1>
				<h2>De olho em tudo. CONHEÇA!</h2>
			</a>
			@foreach ($banners as $banner)
				<a href="{{ $banner->link }}" title="{{ $banner->titulo }}" class="slide">
					<img src="assets/images/banners/{{ $banner->imagem }}" alt="{{ $banner->titulo }}">
					<h1>{{ $banner->titulo }}</h1>
					<h2>{{ $banner->subtitulo }}</h2>
				</a>
			@endforeach
		</div>
	</div>
	<div id="banners-laterais">
		<a href="http://jandaia.com/catalogo-virtual/" target="_blank" title="Simule o seu pedido agora!" class="primeiro">
			<img src="assets/images/home/bannerHome-catalogo-virtual.png" alt="Simule o seu pedido agora!">
			<span>SIMULE SEU <strong>PEDIDO AGORA</strong></span>
		</a>
		<a href="empresa/lab" title="Conheça o Jandaia LAB" class="segundo">
			<img src="assets/images/layout/1-lab.jpg" alt="Conheça o Jandaia LAB">
			<div class="texto">
				DE OLHO EM TUDO
				<span>CONHEÇA!</span>
			</div>
		</a>
		<a href="representantes" title="Encontre o representante mais próximo de você" class="terceiro">
			Encontre o representante mais próximo de você
		</a>
	</div>
</div>

<div id="conteudo-home">
	<div class="coluna-esquerda">

		{{--
		<div id="sociais">
			<span>ACOMPANHE A JANDAIA</span>
			<a href="https://www.facebook.com/cadernosjandaia" target="_blank" title="Facebook" class="link-fb">Facebook</a>
			<a href="http://www.youtube.com/user/cadernosjandaia" target="_blank" title="Youtube" class="link-yt">Youtube</a>
			<a href="https://instagram.com/cadernosjandaia/" target="_blank" title="Instagram" class="link-ig">Instagram</a>
			<a href="https://plus.google.com/117280432477296799371" target="_blank" class="link-gp" title="Google+" rel="publisher">Google+</a>
			<div class="like">
				<div class="fb-like" data-href="https://www.facebook.com/cadernosjandaia" data-width="130" data-layout="button_count" data-show-faces="false" data-send="false"></div>
			</div>
		</div>
		--}}

		<div id="destaque">
			<!--
			<img src="assets/images/home/banner-video.png" style="display:block; margin:10px auto 20px auto; max-width: 100%;" />
			<h1>
				COLEÇÃO 2017
			</h1>
			<h2>
				CONFIRA A APRESENTAÇÃO OFICIAL EM VÍDEO
			</h2>
			-->
			<iframe width="450" height="253" src="//www.youtube.com/embed/wkd4zv-WpvA" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>

	<aside>

		<form action="http://fvendas.bignardi.com.br/fvweb/Default.jsp" method="post" id="login-form">
			<div class="coluna-esquerda-lecom">
				<span>área do representante</span><br>
				<input type="text" name="sUsuario" placeholder="usuário" required>
				<input type="password" name="sSenha" placeholder="senha" required>
				<input type="submit" value="entrar">
				<img src="assets/images/home/arearep-marcalecom.png" alt="Lecom SalesForce">
			</div>
			<div class="coluna-direita-lecom">
				<img src="assets/images/home/arearep-marcaquizz.png" alt="Quiz Saiba Mais com Jandaia">
				<div class="texto">
					<p>Treinamento exclusivo para representantes</p>
					<a href="http://www.jandaia.com/quizrepresentantes" target="_blank" title="Jogue Agora">JOGUE AGORA</a>
				</div>
			</div>
		</form>

		<a href="http://jandaia.com/catalogo-virtual/" target="_blank" title="Simule seu pedido agora com o novo catálogo digital" class="aside-banner">
			<img src="assets/images/home/banner-catalogo-virtual.png" alt="Simule seu pedido agora com o novo catálogo digital">
		</a>
	</aside>

	@if ($novidades)
	<div id="lista-novidades-horizontal">
	@foreach ($novidades as $novidade)
		<a href="novidades/{{$novidade->slug}}" title="{{$novidade->titulo}}">
			@if ($novidade->imagem_destaque)
				<div class="imagem">
					<img src="assets/images/novidades/thumbs/{{ $novidade->imagem_destaque }}" alt="{{$novidade->titulo}}">
				</div>
			@endif
			<h3>{{$novidade->titulo}}</h3>
			<p>{{$novidade->olho}}</p>
		</a>
	@endforeach
	</div>
	@endif
</div>

@if(Session::has('cadastro'))
	@if(Session::get('cadastro'))
		<div class="resposta sucesso">
			Email cadastrado com sucesso.
		</div>
	@else
		<div class="resposta falha">
			Email já está cadastrado em nossa base.
		</div>
	@endif
@endif

<div id="form-newsletter">
	<span>QUERO RECEBER NOVIDADES</span>
	<form action="{{ URL::route('representantes.cadastro') }}" id="form-cadastro" method="post">
		<input type="text" name="nome" placeholder="nome" required id="input-nome">
		<input type="email" name="email" placeholder="e-mail" required id="input-email">
		<input type="submit" value="OK">
	</form>
</div>

@stop
