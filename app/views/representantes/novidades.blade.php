@section('conteudo')

	<div class="conteudo">
		<section>

			<h1>NOVIDADES</h1>

			@if($destaqueNovidade)
				<div class="detalhes">
					<h2>{{ $destaqueNovidade->titulo }}</h2>

					@if($destaqueNovidade->imagem)
						<div class="imagem">
							<img src="assets/images/novidades/{{$destaqueNovidade->imagem}}" alt="{{$destaqueNovidade->titulo}}">
						</div>
					@endif

					@if($destaqueNovidade->olho)
						<div class="olho">
							{{ $destaqueNovidade->olho }}
						</div>
					@endif

					{{ $destaqueNovidade->texto }}
				</div>
			@endif

		</section>

		<aside>
			@if($listaNovidades)
				<ul>
				@foreach($listaNovidades as $novidade)
					<li>
						<a href="novidades/{{ $novidade->slug }}" title="{{ $novidade->titulo }}" @if($novidade->slug==$destaqueNovidade->slug) class='ativo' @endif>
							<div class="data">{{ Tools::converteData($novidade->data) }}</div>
							<div class="titulo">{{ $novidade->titulo }}</div>
						</a>
					</li>
				@endforeach
				</ul>
			@endif
		</aside>
	</div>

@stop