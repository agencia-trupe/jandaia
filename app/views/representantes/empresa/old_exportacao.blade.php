@section ('conteudo')

<section>
	<div class="imagens">
		<img src="assets/images/empresa/placeholder400x600.jpg">
	</div>
	<div class="texto">
		<h1>EXPORTAÇÃO</h1>
		<p>
			Cadernos Brasileiros são os mais utilizados no exterior
		</p>
		<p>
			Aumento da exportação cresceu 154%, o que resulta em recorde de vendas e garantia de novos empregos.
		</p>
		<p>
			O Brasil vive desde o ano passado um período muito favorável à exportação de cadernos para outros países, como Reino Unido, Alemanha, Hong Kong, Canadá, França e Estados Unidos.
		</p>
		<p>
			Isso porque os americanos, que representam 90% na quantidade de exportação, não conseguem atender a grande demanda de seu país e acharam no Brasil cadernos com qualidade, durabilidade, variedade e preço justo. “Atualmente o Brasil vive uma fase melhor que países como Tailândia, Índia e China”, afirma Ivan Bignardi - diretor de marketing do grupo.
		</p>
		<p>
			Este ano já foram exportados, entre cadernos, blocos de anotações e fichas, mais de 10 mil toneladas de papéis, o que resulta em um aumento de 154% e em fabricação acelerada de caderno o ano inteiro, já que o calendário escolar dos americanos começa no meio do ano.
		</p>
		<p>
			Para Ivan, “todo esse progresso é importante para o país e para as exportadoras, pois, além de alavancar as vendas, geramos empregos e estampamos o nome do nosso Brasil afora”.
		</p>
	</div>
</section>

@stop