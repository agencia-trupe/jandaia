@section ('conteudo')

<section>
	<div class="imagens">
		<div class="fsc">
			FSC: O Grupo Bignardi, detentora das marcas Jandaia e Bignardi Papéis conta desde o ano 2009 com o selo FSC® – Forest Stewardship Council® – em grande parcela de seus produtos, entre cadernos, agendas, utilidades e papéis que levam a marca Jandaia. Conhecido no mundo todo, o FSC (traduzido como Conselho de Manejo Florestal) é uma organização não governamental presente em mais de 50 países ao redor do mundo. Com ele, a empresa garante que as matérias primas de origem florestal que compõe seus produtos provém de florestas onde a gestão é considerada responsável pelo FSC.
		</div>
	</div>
	<div class="texto">
		<h1>CERTIFICAÇÕES</h1>
		<p>
			O Grupo Bignardi com as divisões Jandaia, Bignardi Papéis e Atacadão Jandaia trabalha em busca de constantes melhorias em seus processos. Tudo que é criado tem como objetivo ser inovador, sua linha de produção segue constantemente os rigorosos padrões de qualidade para satisfazer as expectativas de seus clientes. Preocupados com o meio ambiente e a sustentabilidade, buscamos certificados ambientais que garantam que as fontes de matérias-primas utilizadas em nossas unidades são provenientes de empresas certificadas nos pilares da qualidade, no respeito ao meio ambiente e em aspectos sociais. E, para isso, o Grupo conta com as seguintes certificações:
		</p>
	</div>
	<div class="certificacoes">
		<div class="cert">
			<div class="imagem">
				<img src="assets/images/empresa/certificacoes/iso9001.jpg" alt="Jandaia - Certificação ISO 9001" style="padding-top:16px;">
			</div>
			<p>
				<strong>IS0 9001:</strong> Norma internacional que fornece requisitos para o Sistema de Gestão da Qualidade ( SGQ ) das organizações. Eles conferem mais credibilidade em relação à capacidade da empresa de fornecer regularmente produtos e serviços que atendam às necessidades e expectativas dos clientes, e estão em conformidade com as leis e regulamentos aplicáveis.				
			</p>
		</div>
		<div class="cert">
			<div class="imagem">
				<img src="assets/images/empresa/certificacoes/cerflor.jpg" alt="Jandaia - Certificação Cerflor">
			</div>
			<p>
				<strong>Cerflor:</strong> O Programa Brasileiro de Certificação Florestal certifica o manejo florestal e a cadeia de custódia, segundo o atendimento dos critérios e indicadores estabelecidos pela Associação Brasileira de Normas Técnicas ( ABNT ) e integradas ao Sistema Brasileiro de Avaliação da conformidade e ao Inmetro.
			</p>
		</div>
		<div class="cert">
			<div class="imagem">
				<img src="assets/images/empresa/certificacoes/inmetro.jpg" alt="Jandaia - Certificação INMETRO">
			</div>
			<p>
				<strong>INMETRO (Instituto Nacional de Metrologia, Normalização e Qualidade Industrial):</strong> esse órgão tem como função estimular e fiscalizar as empresas nacionais, para aumentar sua produtividade e melhorar a qualidade de seus produtos e serviços, o que resulta em confiança, inovação e competitividade no País.
			</p>
		</div>
		<div class="cert">
			<div class="imagem">
				<img src="assets/images/empresa/certificacoes/abrinq.jpg" alt="Jandaia - Certificação Abrinq">
			</div>
			<p>
				<strong>Abrinq (Fundação da Associação Brasileira dos Fabricantes de Brinquedo):</strong> a entidade foi fundada em 1990 para defender os direitos de cidadania da criança e do adolescente. Hoje é mantida por empresas nacionais e internacionais que lutam por seus interesses. A cada ano, o Grupo Bignardi reassume os dez compromissos pregados pela instituição com a infância e a adolescência.
			</p>
		</div>
	</div>
</section>

@stop
