@section('conteudo')

	<div class="lab-2017">
		<div class="centro">
			<div class="lab-2017-left">
				<img src="assets/images/lab/jandaiaLab-titulo2017.png" alt="">

				<p>Há 60 anos, a Jandaia está um passo à frente, trazendo para o mercado o que será tendência e irá agradar em cheio aos seus consumidores.</p>
				<p>Grande parte deste sucesso se deve ao intenso trabalho do Jandaia Lab, um verdadeiro laboratório de pesquisas que identifica as licenças que serão sucesso antes mesmo delas serem lançadas comercialmente. Foi assim com Frozen e tantas outras capas de cadernos que a Jandaia leva até seus clientes sempre em primeira mão.</p>
				<p>Os produtos Jandaia são produzidos com a máxima qualidade, inovação e respeito ao meio ambiente. Possuem detalhes como relevo, glitter, verniz e outros acabamentos que agregam valor ao produto e o seu cliente percebe todo esse carinho na hora da compra.</p>
				<p>Portanto, quando você escolhe Cadernos Jandaia tem a certeza de que há um grande grupo pensando em lhe oferecer os melhores produtos que resultarão em ótimas vendas.</p>
			</div>

			<div class="lab-2017-right">
				<img src="assets/images/lab/jandaiaLab-img2017.png" alt="">
			</div>
		</div>
	</div>

	<!-- <div id="faixa-branca-lab">

		<div class="centro">

			<img src="assets/images/lab/marca-jandaia-lab.png" id="imagem-topo-lab" alt="Jandaia Lab">

			<img src="assets/images/lab/titulo-jandaia-lab.png" id="imagem-chamada-lab" alt="É um pássaro? É um avião? Afinal, o que é esse tal de Jandaia Lab?">

			<p class="island-p">Um belo dia você acorda e tem na sua frente a maior coleção de ideias super incríveis na forma de cadernos. Como é que tudo isso tomou forma? Simples: é o Jandaia Lab em ação.</p>

			<div class="box azul-claro">

				<div class="innerbox vermelho">
					<img src="assets/images/lab/jandaialab-img1-1.png" alt="">
					<p>
						Jandaia Lab é um ambiente de<br>
						pesquisa e criação com equipes <br>
						conectadas no mundo, on line, <br>
						24 horas e nas nuvens.
					</p>
				</div>

				<div class="innerbox azul">
					<img src="assets/images/lab/jandaialab-img2.png" alt="">
					<p>
						Jandaia Lab é um time que reúne <br>
						centenas de talentos de marketing, <br>
						design gráfico, moda & tendências <br>
						de consumo, ilustradores <br>
						e criativos de muitas tribos.
					</p>
				</div>

				<p class="inner-p">Esse povo todo abastece a gente com informações sobre o que crianças e jovens, estudantes e profissionais, mamães e papais estão vendo, comendo, usando e curtindo no mundo inteiro.</p>

			</div>

			<div class="box fundo-branco">

				<p class="azul">Jandaia Lab mostra o que vai estar entre as coisas mais desejadas pelo público daqui a 6 meses.</p>

				<p class="laranja">Jandaia Lab sabe com antecedência qual será o blockbuster da próxima temporada. O personagem que vai acontecer, os filmes que vão bombar, as cores que as meninas vão curtir, os carros e motos que os meninos vão preferir.</p>

				<p class="rosa">Jandaia Lab antecipou, por exemplo, o sucesso espetacular da franquia Angry Birds. Quando eles eram só uns birds normais, a gente já sabia que a coisa ia acontecer e saímos na frente. Jandaia Lab põe os cadernos Jandaia à frente. À frente do tempo, da concorrência, do lojista e até do consumidor.</p>

				<p class="verde">Jandaia Lab está de olho em tudo<br> e em todos os lugares.</p>

			</div>

		</div>

	</div> -->

@stop
