@section ('conteudo')

<section>

	<div class="texto">

		<h1>FOREIGN TRADE</h1>

		<p>
			Each year Bignardi Group reafirm itself as the largest exporter of notebooks from Brazil, taking its brands and products for various markets throughout the Americas, Europe and Africa.
		</p>

		<div style="margin:30px 0; height:1px; width:100%; background:#a2cf6d;"></div>

		<h1>EL COMERCIO EXTERIOR</h1>

		<p>
			El Grupo Bignardi se ha consolidado como el mayor exportador de cuadernos de Brasil, teniendo sus marcas y productos para diferentes mercados en América, Europa y África.
		</p>
	</div>

	<div class="imagens">
		<img src="assets/images/institucional/mapa-exportacao-international.png" style="width:402px;">
	</div>

	<div class="linksExportacao" style="margin-top:60px;">
		<p>
			Learn more:
		</p>
		<a href="download/presentacion-jandaia.pdf" title="DOWNLOAD Grupo Bignardi Presentation [spanish]" class="download-btn">
			<strong>DOWNLOAD</strong>
			Grupo Bignardi Presentation [spanish]
		</a>
		<a href="download/presentation-jandaia.pdf" title="DOWNLOAD Grupo Bignardi Presentation [english]" class="download-btn">
			<strong>DOWNLOAD</strong>
			Grupo Bignardi Presentation [english]
		</a>
		<a href="download/CATALOGO_VA_2016.pdf" title="DOWNLOAD 2016 Product Catalog" class="download-btn">
			<strong>DOWNLOAD</strong>
			2016 Product Catalog
		</a>
		<a href="http://issuu.com/thiagodias51/docs/catalogo-jandaia-2016-final-low.com_cabf243f5504b3" title="VISUALIZAÇÃO Visualize o Catálogo Online" target="_blank" class="visualize-btn">
			<strong>VIEW</strong>
			View the Online Catalog
		</a>
	</div>

	<div class="linksVideoExportacao">
		<a href="http://www.youtube.com/embed/q9ix0g9km8Q?wmode=opaque&autoplay=1" title="Institucional">
			<img src="assets/images/empresa/exportacoes/video-thumb1-institucional.png" alt="Institucional">
			<div class="texto">Institutional Video</div>
		</a>
		<a href="http://www.youtube.com/embed/jWgGx2vl33s?wmode=opaque&autoplay=1" title="Bignardi Papéis">
			<img src="assets/images/empresa/exportacoes/video-thumb2-bignardipapeis.png" alt="Bignardi Papéis">
			<div class="texto">Bignardi Papéis</div>
		</a>
		<a href="http://www.youtube.com/embed/DWIHCx6uajA?wmode=opaque&autoplay=1" title="Fabricação dos Cadernos">
			<img src="assets/images/empresa/exportacoes/video-thumb3-fabricacaocadernos.png" alt="Fabricação dos Cadernos">
			<div class="texto">Jandaia</div>
		</a>
		<a href="http://www.youtube.com/embed/DGE-1TKa1uI?wmode=opaque&autoplay=1" title="Veja como são feitos">
			<img src="assets/images/empresa/exportacoes/video-thumb4-comosaofeitos.png" alt="Veja como são feitos">
			<div class="texto">Jandaia Promotional Video</div>
		</a>
		<a href="http://www.youtube.com/embed/OZTskpKCnFU?wmode=opaque&autoplay=1" title="Collection 2016">
			<img src="assets/images/empresa/exportacoes/video-thumb5-colecao2016.png" alt="Collection 2016">
			<div class="texto">Collection 2016</div>
		</a>
	</div>

	<img src="assets/images/institucional/produtos-exportacaopng.png" alt="{{$conteudo->titulo}}" class="img-area">

</section>

@stop