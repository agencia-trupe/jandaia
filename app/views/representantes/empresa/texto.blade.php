@section ('conteudo')

<section>

	<div class="texto">

		<h1>{{ mb_strtoupper($conteudo->titulo) }}</h1>

		{{$conteudo->texto}}
	</div>

	<div class="imagens">

		@if($conteudo->slug == 'certificacoes')

			<div class="fsc">
				<img src="assets/images/institucional/{{$conteudo->imagem}}" alt="Jandaia - Certificações">
				{{ $conteudo->texto_extra }}
			</div>

		@else

			@if($conteudo->imagem)
				<img src="assets/images/institucional/{{$conteudo->imagem}}">
		    @endif

		@endif

	</div>

	@if($conteudo->slug == 'historico')

		<div class="infografico">

			<div id="timeline">

			   	<ul id="dates">
					<li><a href="#" data-target="1956">1956</a></li>
					<li><a href="#" data-target="1959">1959</a></li>
					<li><a href="#" data-target="1972">1972</a></li>
					<li><a href="#" data-target="1987">1987</a></li>
					<li><a href="#" data-target="1993">1993</a></li>
					<li><a href="#" data-target="1994">1994</a></li>
					<li><a href="#" data-target="2006">2006</a></li>
					<li><a href="#" data-target="2008">2008</a></li>
					<li><a href="#" data-target="2009">2009</a></li>
					<li><a href="#" data-target="2010-1">2010</a></li>
					<li><a href="#" data-target="2010-2">2010</a></li>
					<li><a href="#" data-target="2010-3">2010</a></li>
					<li><a href="#" data-target="2011-1">2011</a></li>
					<li><a href="#" data-target="2011-2">2011</a></li>
					<li><a href="#" data-target="2013-1">2013</a></li>
					<li><a href="#" data-target="2013-2">2013</a></li>
					<li><a href="#" data-target="2016">2016</a></li>
			   	</ul>

			   	<ul id="issues">
					<li id="1956">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-1956.png" alt="História Jandaia - 1956"></div>
							<h2>1956</h2>
							<p>
								José Bignardi Neto inicia a produção de cadernos e listas telefônicas em sua pequena gráfica, na rua Jandaia, no centro de São Paulo.
							</p>
						</div>
					</li>
					<li id="1959">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-1959.png" alt="História Jandaia - 1959"></div>
							<h2>1959</h2>
							<p>
								A gráfica é transferida para um terreno mais amplo na Vila Guilherme, Zona Norte de São Paulo.
							</p>
						</div>
					</li>
					<li id="1972">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-1972.png" alt="História Jandaia - 1972"></div>
							<h2>1972</h2>
							<p>
								Aquisição da fábrica de papel Gordinho Braune, localizada em Jundiaí.
							</p>
						</div>
					</li>
					<li id="1987">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-1987.png" alt="História Jandaia - 1987"></div>
							<h2>1987</h2>
							<p>
								Após anos de crescimento, a gráfica Jandaia é transferida para um terreno de 20 mil m2 na cidade de Caieiras - SP
							</p>
						</div>
					</li>
					<li id="1993">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-1993.png" alt="História Jandaia - 1993"></div>
							<h2>1993</h2>
							<p>
								Nasce o Atacado Jandaia, especializado em materiais de escritório e escolar localizado na cidade de São Paulo.
							</p>
						</div>
					</li>
					<li id="1994">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-1994.png" alt="História Jandaia - 1994"></div>
							<h2>1994</h2>
							<p>
								Primeiros investimentos relevantes na modernização do parque gráfico, triplicando sua capacidade produtiva.
							</p>
						</div>
					</li>
					<li id="2006">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2006.png" alt="História Jandaia - 2006"></div>
							<h2>2006</h2>
							<p>
								A Jandaia completa 50 anos de vida!
							</p>
						</div>
					</li>
					<li id="2008">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2008.png" alt="História Jandaia - 2008"></div>
							<h2>2008</h2>
							<p>
								Inicio da segunda fase de investimentos na modernização de maquinário, aumentando ainda mais a capacidade produtiva dos produtos Jandaia.
							</p>
						</div>
					</li>
					<li id="2009">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2009.png" alt="História Jandaia - 2009"></div>
							<h2>2009</h2>
							<p>
								Conquista do selo de certificação FSC® para toda a linha de produtos Jandaia
							</p>
						</div>
					</li>
					<li id="2010-1">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2010.png" alt="História Jandaia - 2010"></div>
							<h2>2010</h2>
							<p>
								Conquista do selo Iso 9001
							</p>
						</div>
					</li>
					<li id="2010-2">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2010b.png" alt="História Jandaia - 2010"></div>
							<h2>2010</h2>
							<p>
								Conquista do Prêmio Fernando Pini com o caderno da licença O Pequeno Príncipe.
							</p>
						</div>
					</li>
					<li id="2010-3">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2010c.png" alt="História Jandaia - 2010"></div>
							<h2>2010</h2>
							<p>
								A Jandaia é premiada como empresa licenciada internacional do ano pela Coca-Cola, pela qualidade dos nosso produtos.
							</p>
						</div>
					</li>
					<li id="2011-1">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2011.png" alt="História Jandaia - 2011"></div>
							<h2>2011</h2>
							<p>
								A Jandaia recebe mais um prêmio internacional da Coca-Cola, pela inovação tecnológica de nossos produtos.
							</p>
						</div>
					</li>
					<li id="2011-2">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2011b.png" alt="História Jandaia - 2011"></div>
							<h2>2011</h2>
							<p>
								Mais um prêmio conquistado. Desta vez, como melhor desenvolvimento de produto pela Revista da Papelaria.
							</p>
						</div>
					</li>
					<li id="2013-1">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2013a.png" alt="História Jandaia - 2013"></div>
							<h2>2013</h2>
							<p>
								Conquista de prêmio Mattel - melhor desenvolvimento de produto da linha Max Steel
							</p>
						</div>
					</li>
					<li id="2013-2">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2013b.png" alt="História Jandaia - 2013"></div>
							<h2>2013</h2>
							<p>
								Participação na Office Paper Brasil 2013
							</p>
						</div>
					</li>
					<li id="2016">
						<div class="card">
							<div class="imagem"><img src="assets/images/timeline/img-2016.png" alt="História Jandaia - 2016"></div>
							<h2>2016</h2>
							<p>
								O Grupo Bignardi completa 60 anos!<br>
								E a Jandaia consolida-se como a maior exportadora de cadernos do Brasil.
							</p>
						</div>
					</li>
			   	</ul>

			   	<a href="#" id="next">+</a> <!-- optional -->
			   	<a href="#" id="prev">-</a> <!-- optional -->
			</div>

		</div>

	@elseif($conteudo->slug == 'certificacoes')

		@if($certificacoes)
			<div class="certificacoes">
				@foreach($certificacoes as $k => $v)
					<div class="cert">
						<div class="imagem">
							<img src="assets/images/certificacoes/{{$v->imagem}}" alt="{{$v->titulo}}">
						</div>
						{{$v->texto}}
					</div>
				@endforeach
			</div>
		@endif

	@elseif($conteudo->slug == 'exportacao')

		<div class="linksExportacao">
			<p>
				Saiba mais:
			</p>
			<a href="download/presentacion-jandaia.pdf" title="DOWNLOAD Apresentação do Grupo Bignardi [espanhol]" class="download-btn">
				<strong>DOWNLOAD</strong>
				Apresentação do Grupo Bignardi [espanhol]
			</a>
			<a href="download/presentation-jandaia.pdf" title="DOWNLOAD Apresentação do Grupo Bignardi [inglês]" class="download-btn">
				<strong>DOWNLOAD</strong>
				Apresentação do Grupo Bignardi [inglês]
			</a>
			<a href="download/CATALOGO_JANDAIA_2017.pdf" title="DOWNLOAD Catálogo de Produtos 2017" class="download-btn">
				<strong>DOWNLOAD</strong>
				Catálogo de Produtos 2017
			</a>
			<a href="http://issuu.com/thiagodias51/docs/catalogo-jandaia-2016-final-low.com_cabf243f5504b3" title="VISUALIZAÇÃO Visualize o Catálogo Online" target="_blank" class="visualize-btn">
				<strong>VISUALIZAÇÃO</strong>
				Visualize o Catálogo Online
			</a>
		</div>

		<script src="http://www.youtube.com/player_api"></script>
		<div class="linksVideoExportacao">
			<a href="http://www.youtube.com/embed/q9ix0g9km8Q?wmode=opaque&autoplay=1" title="Institucional">
				<img src="assets/images/empresa/exportacoes/video-thumb1-institucional.png" alt="Institucional">
				<div class="texto">Institucional do Grupo</div>
			</a>
			<a href="http://www.youtube.com/embed/jWgGx2vl33s?wmode=opaque&autoplay=1" title="Bignardi Papéis">
				<img src="assets/images/empresa/exportacoes/video-thumb2-bignardipapeis.png" alt="Bignardi Papéis">
				<div class="texto">Bignardi Papéis</div>
			</a>
			<a href="http://www.youtube.com/embed/DWIHCx6uajA?wmode=opaque&autoplay=1" title="Fabricação dos Cadernos">
				<img src="assets/images/empresa/exportacoes/video-thumb3-fabricacaocadernos.png" alt="Fabricação dos Cadernos">
				<div class="texto">Jandaia</div>
			</a>
			<a href="http://www.youtube.com/embed/DGE-1TKa1uI?wmode=opaque&autoplay=1" title="Veja como são feitos">
				<img src="assets/images/empresa/exportacoes/video-thumb4-comosaofeitos.png" alt="Veja como são feitos">
				<div class="texto">Jandaia promocional</div>
			</a>
			<a href="http://www.youtube.com/embed/wkd4zv-WpvA?wmode=opaque&autoplay=1" title="Coleção 2017">
				<img src="assets/images/empresa/exportacoes/video-thumb5-colecao2017.png" alt="Coleção 2017">
				<div class="texto">Coleção 2017</div>
			</a>
		</div>

		@if($conteudo->imagem_maior)
			<img src="assets/images/institucional/{{$conteudo->imagem_maior}}" alt="{{$conteudo->titulo}}" class="img-area">
		@endif

	@else

		@if($conteudo->imagem_maior)
			<img src="assets/images/institucional/{{$conteudo->imagem_maior}}" alt="{{$conteudo->titulo}}" class="img-area">
		@endif

	@endif

	@if(isset($qualidade))
		<div class="certificacoes-qualidade">
			<div class="texto">
				<h1>{{ mb_strtoupper($qualidade->titulo) }}</h1>

				{{$qualidade->texto}}
			</div>

			<div class="imagens">
				@if($qualidade->imagem)
					<img src="assets/images/institucional/{{$qualidade->imagem}}">
			    @endif
			</div>
		</div>
	@endif

</section>

@stop
