@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Institucional - Certificações <a href="{{ URL::route('painel.certificacoes.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Certificação</a>
    	</h2>
        <a href="painel/institucional" class="btn" style="float:none;" title="Voltar">Voltar</a>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="certificacoes">

          		<thead>
            		<tr>
                        <th>Ordenar</th>
              			<th>Título</th>
              			<th>Texto</th>
                        <th>Imagem</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($certificacoes as $value)

                	<tr class="tr-row" id="row_{{ $value->id }}">
                        <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                  		<td>{{ $value->titulo }}</td>
                  		<td>{{ Str::words( strip_tags($value->texto), 10) }}</td>
                        <td><img src="assets/images/certificacoes/{{$value->imagem}}"></td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.certificacoes.edit', $value->id ) }}" class="btn btn-primary">editar</a>
                            
                            {{ Form::open(array('route' => array('painel.certificacoes.destroy', $value->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.certificacoes.destroy', $value->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
                            {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop