@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Texto Institucional
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.institucional.update', $registro->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-txt') ) }}

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" disabled value="{{ $registro->titulo }}" class="input-xxlarge"></label>

			<label>Imagem Lateral<br>
			@if($registro->imagem)
				<img src="assets/images/institucional/{{ $registro->imagem }}"><br>				
			@endif
			<input type="file" name="imagem"></label>
			
			@if($registro->imagem)
				<label><input type="checkbox" name="remover_imagem" value="1"> Remover Imagem Lateral</label><br><br>
			@endif

			@if($registro->slug == 'sustentabilidade' || $registro->slug == 'qualidade-jandaia' || $registro->slug == 'produtos-personalizados' || $registro->slug == 'exportacao')

				<label>Imagem Maior<br>
				@if($registro->imagem_maior)
					<img src="assets/images/institucional/{{ $registro->imagem_maior }}" style="max-width:400px"><br>
					<a href="assets/images/institucional/{{ $registro->imagem_maior }}" target="_blank" title="Ver tamanho original">ver tamanho original</a><br>
				@endif
				<input type="file" name="imagem_maior"></label>
				
				@if($registro->imagem_maior)
					<label><input type="checkbox" name="remover_imagem_maior" value="1"> Remover Imagem Maior</label><br><br>
				@endif
			
			@else
				
				<div class="alert alert-info" style="width:400px;">Esta página não possui o campo <strong>Imagem Maior</strong></div><br><br>	

			@endif

			<label>Texto<br>
			<textarea name="texto" class="grande completo">{{ $registro->texto }}</textarea></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop