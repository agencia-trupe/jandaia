@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Institucional
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed">

          		<thead>
            		<tr>
              			<th>Título</th>
              			<th>Texto</th>
                    <th>Extra</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($registros as $value)

                	<tr class="tr-row">
                  		<td>{{ $value->titulo }}</td>
                  		<td>{{ Str::words( strip_tags($value->texto), 20) }}</td>
                      <td class="crud-actions">
                        @if($value->slug == 'certificacoes')
                          <a href="{{ URL::action('Representantes\Painel\InstitucionalController@texto') }}" title="Texto Box" class="btn btn-mini btn-info">Texto Box</a>
                          <a href="{{ URL::route('painel.certificacoes.index') }}" title="Certificações" class="btn btn-mini btn-info">Certificações</a>
                        @endif
                      </td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.institucional.edit', $value->id ) }}" class="btn btn-primary">editar</a>
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop