@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Banner
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.banners.update', $banner->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-banner') ) }}

			<label>Imagem<br>
			@if($banner->imagem)
				<img src="assets/images/banners/{{ $banner->imagem }}"><br>
			@endif
			<input type="file" name="imagem"></label>

			<label>Título<br>
			<input type="text" name="titulo" required value="{{ $banner->titulo }}" class="input-xxlarge"></label>

			<label>Subtítulo<br>
			<input type="text" name="subtitulo" value="{{ $banner->subtitulo }}" class="input-xxlarge"></label>

			<label>Link<br>
			<input type="text" name="link" required value="{{ $banner->link }}" class="input-xxlarge"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop