@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Representante
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.representantes.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-representantes') ) }}

			<label>Empresa<br>
			<input type="text" name="empresa" class="input-xxlarge"></label>

			<label>Texto<br>
			<textarea name="texto" class="input-xxlarge basico">Contato: <br>Tel.: <br>Cel.: <br>Fax: <br>Endereço: </textarea></label>

			<label>E-mail<br>
			<input type="email" name="email" class="input-xxlarge"></label>

			<label>CEP<br>
			<input type="text" name="cep" class="input-xxlarge" required required-message="O CEP é obrigatório!"></label>

			<label>Região<br>
			<input type="text" name="regiao" required required-message="A região é obrigatória!" class="input-xxlarge"></label>

			<hr>

			Área de Atuação<br><br>

			<div class="form-inline cidades-forms">

				<span class="labels-originais">

					<label>Estado<br>
					<select name="representantes_estado_id[]" class="seleciona_estado" required required-message="O estado é obrigatório!">
						<option value="">Selecione um Estado</option>
						@foreach ($estados as $estado)
							<option value="{{ $estado->id }}">{{ $estado->uf.' - '.$estado->nome }}</option>
						@endforeach
					</select></label>

					<label>Cidade<br>
					<select name="representantes_cidade_id[]" class="seleciona_cidade">
						<option value="">Selecione uma Cidade</option>
					</select></label>
					<label>
						<a href="#" class="btn btn-mini btn-danger btn-remover" title="remover"><i class="icon-remove-sign icon-white"></i> Remover</a>
					</label>
				</span>

				<a href="#" class="btn btn-mini btn-primary" id="acrescentar-cidade"><i class="icon-plus-sign icon-white"></i> Acrescentar Estado/Cidade</a> <br>

			</div>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop