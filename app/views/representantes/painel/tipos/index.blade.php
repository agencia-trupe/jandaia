@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Produtos - Tipos
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="produtos_tipos">

          		<thead>
            		<tr>
                    <th>Ordenar</th>
              			<th>Título</th>
              			<th>Cor</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($tipos as $tipo)

                	<tr class="tr-row" id="row_{{ $tipo->id }}">
                      <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                  		<td>{{ $tipo->titulo }}</td>
                  		<td><div style="width:30px; height:30px; background:{{ $tipo->cor }};"></div></td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.tipos.edit', $tipo->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.tipos.destroy', $tipo->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.tipos.destroy', $tipo->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop