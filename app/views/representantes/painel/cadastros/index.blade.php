@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Cadastros <a href="{{ URL::route('painel.cadastros.download') }}" class="btn btn-success"><i class="icon-download-alt"></i> Exportar Cadastros</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed">

          		<thead>
            		<tr>
              			<th>Nome</th>
              			<th>E-mail</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($cadastros as $usuario)

                	<tr class="tr-row">
                  		<td>{{ $usuario->nome }}</td>
                  		<td>{{ $usuario->email }}</td>
                  		<td class="crud-actions">
                    		{{ Form::open(array('route' => array('painel.cadastros.destroy', $usuario->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.cadastros.destroy', $usuario->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                        {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>

            @if($cadastros->links())
                {{ $cadastros->links() }}
            @endif

    	</div>
  	</div>

@stop