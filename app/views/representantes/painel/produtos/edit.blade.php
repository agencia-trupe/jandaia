@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Produto
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.produtos.update', $produto->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-produto') ) }}

			<label>Tipo de produto<br>
			<select name="produtos_tipo_id" required required-message="O tipo de produto é obrigatório!" id="sel-tipo-produto">
				<option value="">Selecione um tipo</option>
				@foreach ($tipos as $tipo)
					<option value="{{ $tipo->id }}" @if($tipo->id == $produto->produtos_tipo_id) selected @endif>{{ $tipo->titulo }}</option>
				@endforeach
			</select></label>

			<label>Linha do produto<br>
			<select name="produtos_linha_id" required required-message="A linha do produto é obrigatória!">
				<option value="">Selecione uma Linha</option>
				@foreach ($linhas as $linha)
					<option value="{{ $linha->id }}" @if($linha->id == $produto->produtos_linha_id) selected @endif>{{ $linha->titulo }}</option>
				@endforeach
			</select></label>

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge" value="{{ $produto->titulo }}"></label>

			<label>Imagem<br>
			@if($produto->imagem)
				<img src="assets/images/produtos/{{ $produto->imagem }}"><br>
			@endif
			<input type="file" name="imagem"></label>

			<label>Detalhes<br>
			<textarea name="detalhes" class="input-xxlarge basico">{{ $produto->detalhes }}</textarea></label>

			<div class="cadernos-only">
				Tipo de Caderno:<br>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="2" @if($produto->tipo_caderno == 2) checked @endif> Espiral (grupo 1)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="5" @if($produto->tipo_caderno == 5) checked @endif> Espiral (grupo 2)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="6" @if($produto->tipo_caderno == 6) checked @endif> Espiral (grupo 3)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="7" @if($produto->tipo_caderno == 7) checked @endif> Espiral (grupo 4)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="3" @if($produto->tipo_caderno == 3) checked @endif> Plus</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="1" @if($produto->tipo_caderno == 1) checked @endif> Brochura</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="4" @if($produto->tipo_caderno == 4) checked @endif> Pedagógico</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="8" @if($produto->tipo_caderno == 8) checked @endif> Cartográficos</label>
			</div>
			<div class="sel-miolos">
				<br>Tipo de Registro:<br>
				<label class="no-margin"><input type="radio" name="miolo" value="0" @if($produto->miolo == 0) checked @endif> Capa</label>
				<label class="no-margin"><input type="radio" name="miolo" value="1" @if($produto->miolo == 1) checked @endif> Miolo</label>
			</div>

			<hr>
			<div class="well">
				<h3>Publicação</h3>
				<label>
					<input type="checkbox" value="1" style="margin:0 5px 0 0;" @if($produto->publicar_representantes == '1') checked @endif name="publicar_representantes"> publicar produto no site <strong>Jandaia.com</strong>
				</label>
				<label>
					<input type="checkbox" value="1" style="margin:0 5px 0 0;" @if($produto->publicar_cadernos == '1') checked @endif name="publicar_cadernos"> publicar produto no site <strong>Cadernos Jandaia</strong>
				</label>
			</div>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop