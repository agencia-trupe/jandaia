@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Produto
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.produtos.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-produto') ) }}

			<label>Tipo de produto<br>
			<select name="produtos_tipo_id" required required-message="O tipo de produto é obrigatório!" id="sel-tipo-produto">
				<option value="">Selecione um tipo</option>
				@foreach ($tipos as $tipo)
					<option value="{{ $tipo->id }}">{{ $tipo->titulo }}</option>
				@endforeach
			</select></label>

			<label>Linha do produto<br>
			<select name="produtos_linha_id" required required-message="A linha do produto é obrigatória!">
				<option value="">Selecione uma Linha</option>
				@foreach ($linhas as $linha)
					<option value="{{ $linha->id }}">{{ $linha->titulo }}</option>
				@endforeach
			</select></label>

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<label>Imagem<br>
			<input type="file" name="imagem" required required-message="A imagem é obrigatória!"></label>

			<label>Detalhes<br>
			<textarea name="detalhes" class="input-xxlarge basico"></textarea></label>

			<div class="cadernos-only">
				Tipo de Caderno:<br>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="2" checked> Espiral (grupo 1)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="5"> Espiral (grupo 2)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="6"> Espiral (grupo 3)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="7"> Espiral (grupo 4)</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="3"> Plus</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="1"> Brochura</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="4"> Pedagógico</label>
				<label class="no-margin"><input type="radio" name="tipo_caderno" value="8"> Cartográficos</label>
			</div>
			<div class="sel-miolos">
				<br>Tipo de Registro:<br>
				<label class="no-margin"><input type="radio" name="miolo" value="0" checked> Capa</label>
				<label class="no-margin"><input type="radio" name="miolo" value="1"> Miolo</label>
			</div>

			<hr>
			<div class="well">
				<h3>Publicação</h3>
				<label>
					<input type="checkbox" checked value="1" style="margin:0 5px 0 0;" name="publicar_representantes"> publicar produto no site <strong>Jandaia.com</strong>
				</label>
				<label>
					<input type="checkbox" checked value="1" style="margin:0 5px 0 0;" name="publicar_cadernos"> publicar produto no site <strong>Cadernos Jandaia</strong>
				</label>
			</div>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop