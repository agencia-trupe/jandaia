@section ('conteudo')

<aside>

</aside>

<section>

	<h1 class="azul">BUSCAR POR TIPO DE PRODUTO</h1>

	@if($listaTipos)
		<ul id="links-tipos">
		@foreach($listaTipos as $val)
			<li>
				<a href="produtos/{{ $val->slug }}" title="{{ $val->titulo }}" class="cor-produto-{{$val->id}}">
					<div class="titulo">{{ $val->titulo }}</div>
					<div class="imagem im-{{ $val->id }}"></div>
				</a>
			</li>
		@endforeach
		</ul>
	@endif

	<h1 class="branco espac-top">BUSCAR POR LINHA</h1>
	@if($listaLinhas)
		<ul id="links-linhas">

			<?php $num_colunas = 3 ?>

			<?php $por_coluna = ceil(sizeof($listaLinhas)/$num_colunas); ?>

			@foreach($listaLinhas as $indice => $val)

				@if($indice%$por_coluna == 0 && $indice > 0)
					</ul><ul id="links-linhas">
				@endif

				<li>
					<a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a>
				</li>
			@endforeach
		</ul>
	@endif

	<h1 class="branco espac-top">
		BUSCAR POR PALAVRA-CHAVE
		<form action="{{ URL::route('representantes.busca') }}" method="post" id="form-busca">
			<input type="text" name="termo" required>
			<input type="submit" value="buscar" title="Buscar">
		</form>
	</h1>

</section>

@stop