@section ('conteudo')

<aside>

	<h1 class="azul">LINHAS</h1>

	@if($listaLinhas)
		<ul id="menu-linhas">
		@foreach($listaLinhas as $val)
			@if(!is_null($linha) && $val->slug == $linha->slug)
				<li><a href="produtos/linha/{{ $val->slug }}" class="ativo" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a></li>
			@else
				<li><a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a></li>
			@endif
		@endforeach
		</ul>
	@endif

</aside>

<section>

	@if($listaTipos)
		<div id="seleciona-tipo" class="opcoes-escondidas">

			@if(!is_null($tipo))
				<a href="produtos/{{ $tipo->slug }}{{ is_null($linha) ? '' : '/'.$linha->slug }}" title="Exibindo {{$tipo->titulo}}" class="cor-produto-{{$tipo->id}} selecionado">exibindo <strong>{{ $tipo->titulo }}</strong> <i></i></a>
			@else
				<a href="produtos/linha{{ is_null($linha) ? '' : '/'.$linha->slug }}" title="Exibindo Linha" class="cor-produto-0 selecionado">exibindo <strong>LINHA</strong> <i></i></a>
			@endif

			@foreach($listaTipos as $val)
				<a href="produtos/{{ $val->slug }}" title="Exibir {{$val->titulo}}" class="cor-produto-{{$val->id}}">exibir <strong>{{ $val->titulo }}</strong></a>
			@endforeach
		</div>
	@endif

	<form action="{{ URL::route('representantes.busca') }}" method="post" id="form-busca" class="topo">
		<span>BUSCA</span>
		<input type="text" name="termo" class="reduzido" required placeholder="PALAVRA-CHAVE">
		<input type="submit" value="buscar" title="Buscar">
	</form>

	<div id="produtos" class="por_linha">

		<h1 class="azul fs24 espac-bot-red">Linha <strong>{{ $linha->titulo }} &bull;</strong> <span class="menor">todos os produtos da linha</span></h1>

		@if($linha->texto_promocional)
			<h2 class="promo">{{ $linha->texto_promocional }}</h2>
		@endif

		@if($listaProdutos)

				<?php $tipo_atual = ''; ?>
				@foreach($listaProdutos as $key => $prod)

					@if($tipo_atual != $prod->tipo_slug.$prod->miolo)
						@if($key > 0)
							</span>
						@endif
						<h1 class="branco espac-top-med espac-bot-med">{{ $prod->tipo_titulo }}</h1>
						<span class="por_linha">
						<?php $tipo_atual = $prod->tipo_slug.$prod->miolo ?>
					@endif

					<a href="produtos/{{ $prod->tipo_slug }}/{{ $prod->linha_slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
						<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
						<div class="overlay"></div>
					</a>
				@endforeach

		@endif

		<div>
			<a href="produtos" title="Voltar para home de produtos" class="outros-tipos voltar">&laquo; voltar para home de produtos</a>
		</div>
	</div>

</section>

@stop