<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Painel Administrativo - Concurso #TôDentro Jandaia</title>

	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2013 Trupe Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<base href="{{ url() }}/">
	<script> var BASE = "{{ url() }}"</script>

<?= Assets::CSS(array(
	'painel/css/global',
	'jquery-theme-lightness/jquery-ui-1.8.20.custom'))
?>

<?= Assets::JS(array(
	'libs/modernizr-2.0.6.min',
	'libs/jquery-1.8.0.min',
	'plugins/tinymce/tiny_mce',
	'libs/jquery-ui-1.10.1.custom.min',
	'plugins/jquery.ui.datepicker-pt-BR',
	'plugins/jquery.mtz.monthpicker',
	'plugins/jquery.uploadify.min',
	'plugins/bootstrap',
  'plugins/bootbox.min',
	'scripts/painel'))
?>

</head>
	<body>

	<div class="navbar ">
  		<div class="navbar-inner">

      			<a href="{{ URL::route('concurso.painel.home') }}" class="brand">Concurso #TôDentro</a>

      			<ul class="nav">

        			<li @if(Route::currentRouteName() == 'concurso.painel.home') class='active' @endif><a href="{{ URL::route('concurso.painel.home') }}">Início</a></li>

              <li @if(str_is('concurso.painel.participacoes*', Route::currentRouteName())) class='active' @endif><a href="{{ URL::route('concurso.painel.participacoes') }}">Participações</a></li>

              <li class="dropdown @if(strpos(Route::currentRouteName(), 'painel.usuarios') !== FALSE)active@endif">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          				<ul class="dropdown-menu">
            				<li><a href="{{ URL::route('painel.usuarios.index') }}">Usuários</a></li>
            				<li><a href="{{ URL::route('concurso.painel.off') }}">Logout</a></li>
          				</ul>
        			</li>

      			</ul>
  		</div>
	</div>

		@yield('conteudo')

		<div id="footer">
    		<hr>
    		<div class="inner">
      			<div class="container">
        			<p class="right">
          				<a href="#">Voltar ao topo</a>
        			</p>
        			<p>
          				&copy; <a href="http://www.trupe.net" target="_blank">Criação de Sites</a> : <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
        			</p>
      			</div>
    		</div>
  		</div>

	</body>
</html>