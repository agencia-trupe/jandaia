<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="favicon-cadernosjandaia.ico">
    <meta name="keywords" content="cadernos jandaia, cadernos, agendas, fichários, etiquetas, bolsas," />

    @if(isset($seo) && $seo)
		<title>Cadernos Jandaia - {{ trim(strip_tags($seo['title'])) }}</title>
    	<meta property="og:title" content="Cadernos Jandaia - {{ trim(strip_tags($seo['title'])) }}"/>
    	@if(strpos($seo['image'], 'blog') !== FALSE)
    		<meta property="og:image" content="{{ asset('assets/images/consumidor/blog/'.$seo['image']); }}"/>
    	@else
    		<meta property="og:image" content="{{ asset('assets/images/produtos/'.$seo['image']); }}"/>
    	@endif
		<meta name="description" content="Cadernos Jandaia - {{ trim(strip_tags($seo['description'])) }}">
    	<meta property="og:description" content="Cadernos Jandaia - {{ trim(strip_tags($seo['description'])) }}"/>
    @else
    	<title>Cadernos Jandaia - Materiais Escolares, Cadernos, Agendas, Fichários e Bolsas escolares.</title>
    	<meta property="og:title" content="Cadernos Jandaia - Materiais Escolares, Cadernos, Agendas, Fichários e Bolsas escolares."/>
    	<meta property="og:image" content="{{ asset('assets/images/layout/marca-jandaia.jpg'); }}"/>
		<meta name="description" content="Cadernos Jandaia - Cadernos, Agendas, Fichários, Bolsas e Materiais Escolares para todos os estilos. Alta qualidade premiada. Produto brasileiro.">
    	<meta property="og:description" content="Cadernos Jandaia - Cadernos, Agendas, Fichários, Bolsas e Materiais Escolares para todos os estilos. Alta qualidade premiada. Produto brasileiro."/>
    @endif

    <meta property="og:site_name" content="Cadernos Jandaia"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
  	<meta property="fb:admins" content="100002297057504"/>
  	<meta property="fb:app_id" content="327477457394269"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'reset',
		'fontface/consumidor/stylesheet',
		'fancybox/fancybox_radius',
		'consumidor/base',
		'pure/pure-css'
	))?>

	@if(Route::currentRouteName() == 'consumidor.home')
		<?=Assets::CSS(array('consumidor/home'))?>
	@else
		<?=Assets::CSS(array('consumidor/internas'))?>
	@endif

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min', 'libs/less-1.3.0.min'))?>
	@else
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min'))?>
	@endif

	<!-- Google Analytics -->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-3325882-1']);
		_gaq.push(['_trackPageview']);
		(function() {
		   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		   ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
		})();
	</script>

	<script type="application/ld+json">
	{ "@context" : "http://schema.org",
	  "@type" : "Organization",
	  "name" : "Cadernos Jandaia",
	  "url" : "http://jandaia.com/",
	  "logo": "http://jandaia.com/assets/images/layout/marca-jandaia.jpg",
	  "sameAs" : [ "https://www.facebook.com/cadernosjandaia",
	    "http://www.youtube.com/user/cadernosjandaia",
	    "https://instagram.com/cadernosjandaia/",
	    "https://plus.google.com/117280432477296799371"]
	}
	</script>

	<!-- Pixel Tag -->
	<script language='JavaScript1.1' src='http://pixel.mathtag.com/event/js?mt_id=914980&mt_adid=156365&v1=&v2=&v3=&s1=&s2=&s3='></script>

</head>
<body>

	<!-- Facebook SDK -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=327477457394269";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<div class="centro">

		<header class="pure-g">
			<div class="pure-u-7-24">
				<div id="link-logo">
					<a href="{{ URL::route('consumidor.home') }}" title="Página Inicial">
						<img src="assets/images/consumidor/layout/marca-jandaia.png" alt="Cadernos Jandaia">
					</a>
				</div>
			</div>
			<nav class="pure-u-17-24">

				<a href="#" id="mostrar-menu" class="ativo">MENU</a>

				<ul class="superior">
					<li class="item-menu-sup"><a href="{{ URL::route('consumidor.home') }}" @if(str_is('consumidor.home', Route::currentRouteName())) class="ativo" @endif title="Página Inicial">HOME</a></li>
					<li class="item-menu-sup"><a href="{{ URL::route('consumidor.jandaia') }}" @if(str_is('consumidor.jandaia', Route::currentRouteName())) class="ativo" @endif title="A Jandaia">A JANDAIA</a></li>
					<li class="item-menu-sup"><a href="representantes" title="Lojistas" target="_blank">LOJISTAS</a></li>
					<li class="item-menu-sup"><a href="{{ URL::route('consumidor.encontrar') }}" @if(str_is('consumidor.encontrar*', Route::currentRouteName())) class="ativo" @endif title="Onde Encontrar">ONDE ENCONTRAR</a></li>
					<li class="item-menu-sup"><a href="{{ URL::route('consumidor.contato') }}" @if(str_is('consumidor.contato', Route::currentRouteName())) class="ativo" @endif title="Contato">CONTATO</a></li>
				</ul>

				<ul class="inferior">
					<div class="pure-g">
						<li class="pure-u-1-5"><a class="mn @if(str_is('consumidor.produtos', Route::currentRouteName())) ativo @endif" href="{{ URL::route('consumidor.produtos') }}" title="Nossos Produtos ">PRODUTOS</a></li>
						<li class="pure-u-2-5"><a class="mn @if(str_is('consumidor.blog*', Route::currentRouteName())) ativo @endif" href="{{ URL::route('consumidor.blog') }}" title="Fique por Dentro">FIQUE POR DENTRO</a></li>
						<li class="pure-u-1-5"><a class="mn @if(str_is('consumidor.promocoes', Route::currentRouteName())) ativo @endif" href="{{ URL::route('consumidor.promocoes') }}" title="Promoções">PROMOÇÕES</a></li>
						<li class="social pure-u-1-5">
							<div class="pad">
								<div class="pure-g">
									<a href="https://www.youtube.com/cadernosjandaia" target="_blank" class="link-yt pure-u-1-5" title="Youtube">You Tube</a>
									<a href="https://www.facebook.com/cadernosjandaia" target="_blank" class="link-fb pure-u-1-5" title="Facebook">Facebook</a>
									<!-- <a href="http://www.pinterest.com/Jandaia/" target="_blank" class="link-pt pure-u-1-5" title="Pinterest">Pinterest</a> -->
									<a href="https://plus.google.com/117280432477296799371" target="_blank" class="link-gp pure-u-1-5" title="Google+" rel="publisher">Google+</a>
									<a href="https://instagram.com/cadernosjandaia/" target="_blank" class="link-ig pure-u-1-5" title="Instagram">Instagram</a>
								</div>
							</div>
						</li>
					</div>
				</ul>
			</nav>
		</header>

	</div>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('consumidor.', '', Route::currentRouteName()) }}">
		@yield('conteudo')
	</div>

	<footer>

		<div class="links-grupo pure-g">
			<span class="pure-u-1">Jandaia é uma marca registrada do Grupo Bignardi. Conheça-nos:</span>

			<a href="http://www.bignardi.com.br/" target="_blank" title="Bignardi" class="pure-u-1-4 link-bignardi">Bignardi</a>

			<a href="http://www.gbmillennium.com.br" target="_blank" title="Bignardi Papéis" class="pure-u-1-4 link-bignardi-papeis">Bignardi Papéis</a>

			<a href="http://jandaia.com/" title="Jandaia" class="pure-u-1-4 link-jandaia">Jandaia</a>

			<a href="http://cadernosjandaia.com.br" target="_blank" title="Jandaia Cadernos" class="pure-u-1-4 link-jandaia-cadernos">Jandaia Cadernos</a>

		</div>
		<div class="assinatura">
			SAC: 0800-165656 &bull; <a href="mailto:sac@jandaia.com" title="Envie um email">sac@jandaia.com</a> | &copy; <?= Date('Y')?> - Todos os direitos reservados.<br>
			<a class="link-trupe" href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">criação de sites:</a> <a class="link-trupe" href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Trupe Agência Criativa</a>
		</div>
	</footer>

	<?=Assets::JS(array(
		'plugins/cycle',
		'plugins/fancybox',
		'plugins/jquery.maskedinput.min',
		'scripts/consumidor'
	))?>


	<!-- Google Remarketing -->
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 990611974;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		var google_conversion_format = 3;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/990611974/?value=0&amp;guid=ON&amp;script=0"/></div></noscript>

</body>
</html>