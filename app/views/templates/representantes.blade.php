<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="favicon-jandaia.ico">

    <title>Jandaia – Site para o lojista</title>
	<meta name="description" content=" Site para o lojista. Jandaia é uma marca de cadernos, agendas, fichários, bolsas e outros produtos de papelaria.">
    <meta name="keywords" content="cadernos jandaia, cadernos, agendas, fichários, etiquetas, bolsas," />
    <meta property="og:title" content="Jandaia – Site para o lojista"/>
    <meta property="og:site_name" content="Jandaia"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="{{ asset('assets/images/layout/marca-jandaia.jpg'); }}"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:description" content=" Site para o lojista. Jandaia é uma marca de cadernos, agendas, fichários, bolsas e outros produtos de papelaria."/>

  	<meta property="fb:admins" content="100002297057504"/>
  	<meta property="fb:app_id" content="580310192049536"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>

	<?=Assets::CSS(array(
		'reset',
		'representantes/base',
		'fontface/stylesheet',
		'fancybox/fancybox',
		'representantes/'.str_replace('representantes.', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array('representantes/'.$css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min', 'libs/less-1.3.0.min'))?>
	@else
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min'))?>
	@endif

	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-44518347-1']);
		_gaq.push(['_trackPageview']);
		(function() {
		   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		   ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
		})();
	</script>

	<script type="application/ld+json">
	{ "@context" : "http://schema.org",
	  "@type" : "Organization",
	  "name" : "Cadernos Jandaia",
	  "url" : "http://jandaia.com/",
	  "logo": "http://jandaia.com/assets/images/layout/marca-jandaia.jpg",
	  "sameAs" : [ "https://www.facebook.com/cadernosjandaia",
	    "http://www.youtube.com/user/cadernosjandaia",
	    "https://instagram.com/cadernosjandaia/",
	    "https://plus.google.com/117280432477296799371"]
	}
	</script>
</head>
	<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=580310192049536";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

		<div id="faixa-branca" class="{{ 'faixa-'.str_replace('.', '-', Route::currentRouteName()) }}"></div>
		<div class="centro">

			<nav>
				<a href="{{ URL::route('representantes.home') }}" title="Página Inicial" class="logo-link"><img src="assets/images/layout/marca-jandaia.png" alt="Jandaia"></a>

				<ul class="escondido-mobile">
					<li class="first"><a href="{{ URL::route('representantes.home') }}" title="Página Inicial" @if(Route::currentRouteName()=='home') class="ativo" @endif>Página Inicial</a></li>
					<li class="menu">
						<a href="empresa" title="Empresa" @if(str_is('representantes.empresa*', Route::currentRouteName())) class="ativo" @endif>EMPRESA</a>
						<ul class="submenu @if(str_is('representantes.empresa*', Route::currentRouteName())) aberto @endif">
							<li><a href="empresa/historico" title="Histórico" @if(str_is('representantes.empresa.historico', Route::currentRouteName())) class="ativo" @endif>Histórico</a></li>
							<li><a href="empresa/melhor-escolha" title="Melhor Escolha" @if(str_is('representantes.empresa.melhorescolha', Route::currentRouteName())) class="ativo" @endif>MELHOR ESCOLHA</a></li>
							<li><a href="empresa/lab" title="Jandaia LAB" @if(str_is('representantes.empresa.lab', Route::currentRouteName())) class="ativo" @endif>JANDAIA LAB</a></li>
							<li><a href="empresa/sustentabilidade" title="Sustentabilidade" @if(str_is('representantes.empresa.sustentabilidade', Route::currentRouteName())) class="ativo" @endif>Sustentabilidade</a></li>
							<li><a href="empresa/certificacoes-e-qualidade" title="Certificações e Qualidade" @if(str_is('representantes.empresa.certificacoes', Route::currentRouteName())) class="ativo" @endif>Certificações e Qualidade</a></li>
							<li><a href="empresa/produtos-personalizados" title="Produtos Personalizados" @if(str_is('representantes.empresa.produtos', Route::currentRouteName())) class="ativo" @endif>Produtos Personalizados</a></li>
							<li><a href="empresa/exportacao" title="Exportação" @if(str_is('representantes.empresa.exportacao', Route::currentRouteName())) class="ativo" @endif>Exportação</a></li>
						</ul>
					</li>
					<li class="menu"><a href="produtos" title="Produtos" @if(Route::currentRouteName()=='representantes.produtos') class="ativo" @endif>PRODUTOS</a></li>
					<li class="menu"><a href="representantes" title="Representantes" @if(Route::currentRouteName()=='representantes.representantes') class="ativo" @endif>REPRESENTANTES</a></li>
					<li class="menu"><a href="novidades" title="Novidades" @if(Route::currentRouteName()=='representantes.novidades') class="ativo" @endif>NOVIDADES</a></li>
					<li class="menu"><a href="contato" title="Contato" @if(Route::currentRouteName()=='representantes.contato') class="ativo" @endif>CONTATO</a></li>
				</ul>
				<div id="sociais">
					<a href="https://www.facebook.com/cadernosjandaia" target="_blank" title="Facebook" class="link-fb">Facebook</a>
					<a href="http://www.youtube.com/user/cadernosjandaia" target="_blank" title="Youtube" class="link-yt">Youtube</a>
					<a href="https://instagram.com/cadernosjandaia/" target="_blank" title="Instagram" class="link-ig">Instagram</a>
					<a href="https://plus.google.com/117280432477296799371" target="_blank" class="link-gp" title="Google+" rel="publisher">Google+</a>
				</div>
			</nav>

			<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
				@yield('conteudo')
			</div>
		</div>

		<footer>
			<div id="faixa-azul"></div>
			<div class="links-grupo">
				<span>Jandaia é uma marca registrada do Grupo Bignardi. Conheça-nos:</span>

				<a href="http://www.bignardi.com.br/" target="_blank" title="Bignardi" class="link-bignardi">Bignardi</a>

				<div class="separador"></div>

				<a href="http://www.gbmillennium.com.br/" target="_blank" title="Bignardi Papéis" class="link-bignardi-papeis">Bignardi Papéis</a>

				<div class="separador"></div>

				<a href="http://jandaia.com/" title="Jandaia" class="link-jandaia">Jandaia</a>

				<div class="separador"></div>

				<a href="http://cadernosjandaia.com.br" target="_blank" title="Jandaia Cadernos" class="link-jandaia-cadernos">Jandaia Cadernos</a>

			</div>
			<div class="assinatura">
				SAC: 0800-165656 &bull; <a href="mailto:sac@jandaia.com" title="Envie um email">sac@jandaia.com</a> | &copy; <?= Date('Y')?> - Todos os direitos reservados.<br>
				<a class="link-trupe" href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">criação de sites:</a> <a class="link-trupe" href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Trupe Agência Criativa</a>
			</div>
		</footer>

		<?=Assets::JS(array(
			'plugins/jquery.timelinr-0.9.53',
			'plugins/cycle',
			'plugins/fancybox',
			'scripts/representantes'
		))?>
	</body>
</html>
