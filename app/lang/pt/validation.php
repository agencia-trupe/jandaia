<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => ":attribute deve ser aceito.",
	"active_url"       => ":attribute não é uma URL válida.",
	"after"            => ":attribute precisa ser uma data após :date.",
	"alpha"            => ":attribute só pode conter letras.",
	"alpha_dash"       => ":attribute só pode conter letras, número e traços.",
	"alpha_num"        => ":attribute só pode conter letras e números.",
	"array"            => ":attribute deve ser um array.",
	"before"           => ":attribute deve ser uma data antes de :date.",
	"between"          => array(
		"numeric" => ":attribute deve estar entre :min - :max.",
		"file"    => ":attribute deve ter entre :min - :max kilobytes.",
		"string"  => ":attribute deve ter entre :min - :max caracteres.",
		"array"   => ":attribute deve possuir entre :min e :max items.",
	),
	"confirmed"        => ":attribute e a confirmação não coincidem.",
	"date"             => ":attribute não é uma data válida.",
	"date_format"      => ":attribute deve estar no formato :format.",
	"different"        => ":attribute e :other devem ser diferentes.",
	"digits"           => ":attribute deve ter :digits dígitos.",
	"digits_between"   => ":attribute deve ter entre :min e :max dígitos.",
	"email"            => ":attribute deve ser um email.",
	"exists"           => ":attribute é inválido.",
	"image"            => ":attribute deve ser uma imagem.",
	"in"               => ":attribute é um valor inválido.",
	"integer"          => ":attribute deve ser um inteiro.",
	"ip"               => ":attribute deve ser um endereço de IP válido.",
	"max"              => array(
		"numeric" => "O campo :attribute não pode ser maior que :max.",
		"file"    => "O campo :attribute não pode ser maior que :max kilobytes.",
		"string"  => "O campo :attribute não pode ser maior que :max caracteres.",
		"array"   => "O campo :attribute não pode ter mais que :max items.",
	),
	"mimes"            => "O arquivo :attribute deve ser do(s) seguinte(s) tipo(s): :values.",
	"min"              => array(
		"numeric" => "O campo :attribute deve ser no mínimo :min.",
		"file"    => "O campo :attribute deve possuir no mínimo :min kilobytes.",
		"string"  => "O campo :attribute deve possuir no mínimo :min caracteres.",
		"array"   => "O campo :attribute deve possuir no mínimo :min items.",
	),
	"not_in"           => ":attribute inválido.",
	"numeric"          => ":attribute deve ser um  número.",
	"regex"            => "O formato de :attribute é inválido.",
	"required"         => "O campo :attribute é obrigatório.",
	"required_if"      => "O campo :attribute é obrigatório quando :other é :value.",
	"required_with"    => "O campo :attribute é obrigatório quando :values está presente.",
	"required_without" => "O campo :attribute é obrigatório quando :values não está presente.",
	"same"             => ":attribute e :other não coincidem.",
	"size"             => array(
		"numeric" => "O :attribute deve ser no mínimo :size.",
		"file"    => "O :attribute deve ter :size kilobytes.",
		"string"  => "O :attribute deve ter :size caracteres.",
		"array"   => "O :attribute deve conter :size items.",
	),
	"unique"           => "O :attribute já está em uso.",
	"url"              => "O formato de :attribute é inválido.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
