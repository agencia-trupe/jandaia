<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|	  Ordenação das Rotas:
| 1 - Rotas do site de Representantes (1ROTREP)
| 2 - Rotas do site de Consumidores (2ROTCON)
| 3 - Rotas do site Concurso To Dentro (3ROTTOD)
| 4 - Rotas para os Painéis Administrativos (4ROTPAI)
|
*/

// LOCAL
// $dominioRepresentantes = "jandaia";
// $dominioCadernos = "cadernosjandaia";
// $dominioConcurso = "todentro";

// TESTE
// $dominioRepresentantes = "jandaia.test.10.68.204.118.xip.io";
// $dominioCadernos = "cadernosjandaia.test.10.68.204.118.xip.io";
// $dominioConcurso = "todentro.test.10.68.204.118.xip.io";

// PRODUÇÃO
$dominioRepresentantes = "jandaia.com";
$dominioCadernos = "cadernosjandaia.com.br";
$dominioConcurso = "concursotodentro.com.br";

/*
|--------------------------------------------------------------------------
| Rotas para o site : REPRESENTANTES (1ROTREP)
|--------------------------------------------------------------------------
| Rotas que integram o funcionamento do site principal 'Representantes'
| o 'Representantes' atua como o site principal
*/
Route::group(array('domain' => $dominioRepresentantes), function()
{
	/* Views estáticas adicionais */
	Route::get('campanha', function(){
		return View::make('representantes.campanha');
	});
	Route::get('fabricacao', function(){
		return View::make('representantes.fabricacao');
	});

	/* HOME */
	Route::get('/', array('as' => 'representantes.home', 'uses' => 'Representantes\HomeController@index'));
	Route::post('cadastro', array('as' => 'representantes.cadastro', 'uses' => 'Representantes\HomeController@cadastro'));

	/* EMPRESA */
	Route::get('empresa', function(){ return Redirect::to('empresa/historico'); });
	Route::get('empresa/historico', array('as' => 'representantes.empresa.historico', 'uses' => 'Representantes\EmpresaController@historico'));
	Route::get('empresa/melhor-escolha', array('as' => 'representantes.empresa.melhorescolha', 'uses' => 'Representantes\EmpresaController@melhorescolha'));
	Route::get('empresa/sustentabilidade', array('as' => 'representantes.empresa.sustentabilidade', 'uses' => 'Representantes\EmpresaController@sustentabilidade'));
	Route::get('empresa/certificacoes-e-qualidade', array('as' => 'representantes.empresa.certificacoes', 'uses' => 'Representantes\EmpresaController@certificacoes'));
	Route::get('empresa/produtos-personalizados', array('as' => 'representantes.empresa.produtos', 'uses' => 'Representantes\EmpresaController@produtos'));
	Route::get('empresa/exportacao', array('as' => 'representantes.empresa.exportacao', 'uses' => 'Representantes\EmpresaController@exportacao'));
	Route::get('empresa/lab', array('as' => 'representantes.empresa.lab', 'uses' => 'Representantes\EmpresaController@lab'));
	Route::get('international', array('as' => 'representantes.international', 'uses' => 'Representantes\EmpresaController@international'));

	Route::get('download/{arquivo?}', function($arquivo = false){

		if(!$arquivo)
			App::abort('404');
		else{

			$pathToFile = app_path().'/internal_files/arquivos/'.$arquivo;

			if(file_exists($pathToFile))
				return Response::download($pathToFile);
			else
				App::abort('404');
		}
	});

	/* PRODUTOS */
	Route::get('produtos/{tipo?}/{linha?}/{produto?}', array('as' => 'representantes.produtos', 'uses' => 'Representantes\ProdutosController@index'));

	/* BUSCA DE PRODUTOS */
	Route::any('busca', array('as' => 'representantes.busca', function(){

		$termo = Input::get('termo');

		if(!$termo){
			return Redirect::to('produtos');
		}else{

			$resultados = DB::table('produtos_linhas')
								->orderBy('produtos_linhas.titulo')
								->join('produtos', 'produtos_linhas.id', '=', 'produtos.produtos_linha_id')
								->where('produtos_linhas.titulo', 'like', "%$termo%")
								->orWhere('produtos.detalhes', 'like', "%$termo%")
								->orWhere('produtos.titulo', 'like', "%$termo%")
								->get();

			if(count($resultados) == 0){

				return View::make('representantes.produtos.busca')->with('mensagem', "Nenhuma linha encontrada com o termo : <strong>$termo</strong>")
														 ->with('listaTipos', Tipo::orderBy('titulo')->get())
														 ->with('listaLinhas', Linha::orderBy('titulo')->get())
														 ->with('resultados', false)
														 ->with('termo', $termo)
														 ->with('css', 'produtos');

			}elseif(count($resultados) == 1){

				return Redirect::to('produtos/linha/'.$resultados[0]->slug);

			}else{

				return View::make('representantes.produtos.busca')->with('mensagem', "Mais de 1 linha encontrada com o termo <strong>$termo</strong>")
														->with('listaTipos', Tipo::orderBy('titulo')->get())
														->with('listaLinhas', Linha::orderBy('titulo')->get())
														->with('resultados', $resultados)
														->with('termo', $termo)
														->with('css', 'produtos');
			}
		}
	}));


	/* REPRESENTANTES */
	Route::any('representantes/{estado?}', array('as' => 'representantes.representantes', 'uses' => 'Representantes\RepresentantesController@index'));

	/* NOVIDADES */
	Route::get('novidades/{slug?}', array('as' => 'representantes.novidades', 'uses' => 'Representantes\NovidadesController@index'));

	/* CONTATO */
	Route::get('contato', array('as' => 'representantes.contato', 'uses' => 'Representantes\ContatoController@index'));
	Route::post('contato', array('as' => 'representantes.contato.enviar', 'uses' => 'Representantes\ContatoController@enviar'));

	/* CONSUMIDOR - Acesso ao site Cadernos Jandaia */
	Route::get('consumidor', function(){
		//return Redirect::to('http://www.cadernosjandaia.com.br');
	});

	/*
	|--------------------------------------------------------------------------
	| Rotas do Painel de Administração
	|--------------------------------------------------------------------------|
	*/

	// Página Inicial do Painel
	// Se estiver logado mostra a view
	// Se não estiver redireciona para painel/login
	Route::get('painel', array('before' => 'auth', 'as' => 'representantes.painel.home', 'uses' => 'Representantes\Painel\HomeController@index'));

	// Página de Login
	Route::get('painel/login', array('as' => 'representantes.painel.login', 'uses' => 'Representantes\Painel\HomeController@login'));

	// Autenticação do Login (via POST)
	Route::post('painel/login',  array('as' => 'representantes.painel.auth', function(){

		$authvars = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'user_painel_concurso' => '0'
		);
		if(Auth::attempt($authvars)){
			return Redirect::to('painel');
		}else{
			Session::flash('login_errors', true);
			return Redirect::to('painel/login');
		}
	}));

	// Ação de logout
	Route::get('painel/logout', array('as' => 'representantes.painel.off', function(){
		Auth::logout();
		return Redirect::to('painel');
	}));

	/* Ajax de ordenação de resultados do painel */
	Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Representantes\AjaxController@gravaOrdem'));
	/* Busca Assíncrona de cidades para /fornecedores DEPRECATED */
	Route::get('ajax/pegarCidades/{id}', function($id){
		return Response::json(Cidade::where('estado', '=', $id)->get());
	});
});
/*
|--------------------------------------------------------------------------
| Fim das Rotas para o site : REPRESENTANTES
|--------------------------------------------------------------------------
*/




/*
|--------------------------------------------------------------------------
| Rotas para o site : CADERNOS/CONSUMIDOR (2ROTCON)
|--------------------------------------------------------------------------
| Rotas que integram o funcionamento do site secundário 'Cadernos' ou 'Consumidor'
| o 'Consumidor' atua como o site secundário voltado para o consumidor final
*/

Route::group(array('domain' => $dominioCadernos), function()
{
	Route::get('/', array('as' => 'consumidor.home', 'uses' => 'Consumidor\HomeController@index'));
	Route::get('jandaia', array('as' => 'consumidor.jandaia', 'uses' => 'Consumidor\JandaiaController@index'));

	Route::get('fique-por-dentro/{slug?}', array('as' => 'consumidor.blog', 'uses' => 'Consumidor\BlogController@index'));
	Route::get('fique-por-dentro/ler/{slug}', array('as' => 'consumidor.blog.detalhes', 'uses' => 'Consumidor\BlogController@detalhes'));

	Route::get('1D', function(){ return Redirect::to('produtos/linha/74-one-direction'); });
	Route::get('1d', function(){ return Redirect::to('produtos/linha/74-one-direction'); });
	Route::get('produtos/formPromoModal', array('as' => 'produtos.form_promo_modal', 'uses' => 'Consumidor\ProdutosController@formPromoModal'));
	Route::post('produtos/enviarPromoModal', array('as' => 'produtos.enviar_promo_modal', 'uses' => 'Consumidor\ProdutosController@enviarPromoModal'));

	Route::get('caderno-completo', array('as' => 'consumidor.completo', 'uses' => 'Consumidor\HomeController@completo'));

	Route::get('produtos/{tipo?}/{linha?}/{produto?}', array('as' => 'consumidor.produtos', 'uses' => 'Consumidor\ProdutosController@index'));

	Route::get('contato', array('as' => 'consumidor.contato', 'uses' => 'Consumidor\ContatoController@index'));
	Route::post('contato', array('as' => 'consumidor.contato.enviar', 'uses' => 'Consumidor\ContatoController@enviar'));

	Route::get('promocoes', array('as' => 'consumidor.promocoes', 'uses' => 'Consumidor\PromocoesController@index'));

	Route::get('encontrar', array('as' => 'consumidor.encontrar', 'uses' => 'Consumidor\EncontrarController@index'));
	Route::get('encontrar/online', array('as' => 'consumidor.encontrar.online', 'uses' => 'Consumidor\EncontrarController@online'));
	Route::get('encontrar/buscar', array('as' => 'consumidor.encontrar.buscar', 'uses' => 'Consumidor\EncontrarController@buscar'));

	Route::get('download/{dir?}/{arquivo?}', function($dir = false, $arquivo = false){

		if(!$dir || !$arquivo)
			App::abort('404');
		else{

			switch ($dir) {
				case 'tema':
					$pathToFile = 'assets/images/consumidor/temas/'.$arquivo;
					break;

				default:
					$pathToFile = 'foo/bar';
			}

			if(file_exists($pathToFile))
				return Response::download($pathToFile);
			else
				App::abort('404');

		}
	});

	Route::get('confirmacao', function(){
		return Response::view('templates.successpage.consumidor');
	});

	Route::get('ajax/pegarCidades/{id}', function($id){
		return Response::json(Cidade::where('estado', '=', $id)->get());
	});

	Route::get('representantes', function(){
		return Redirect::to('http://www.jandaia.com');
	});
	/*
	|--------------------------------------------------------------------------
	| Rotas do Painel de Administração
	|--------------------------------------------------------------------------|
	*/

	// Página Inicial do Painel
	// Se estiver logado mostra a view
	// Se não estiver redireciona para painel/login
	Route::get('painel', array('before' => 'auth', 'as' => 'consumidor.painel.home', 'uses' => 'Consumidor\Painel\HomeController@index'));

	// Página de Login
	Route::get('painel/login', array('as' => 'consumidor.painel.login', 'uses' => 'Consumidor\Painel\HomeController@login'));

	// Autenticação do Login (via POST)
	Route::post('painel/login',  array('as' => 'consumidor.painel.auth', function(){
		$authvars = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'user_painel_concurso' => '0'
		);
		if(Auth::attempt($authvars)){
			return Redirect::to('painel');
		}else{
			Session::flash('login_errors', true);
			return Redirect::to('painel/login');
		}
	}));

	// Ação de logout
	Route::get('painel/logout', array('as' => 'consumidor.painel.off', function(){
		Auth::logout();
		return Redirect::to('painel');
	}));

	/* Ajax de ordenação de resultados do painel */
	Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Consumidor\AjaxController@gravaOrdem'));

});
/*
|--------------------------------------------------------------------------
| Fim das Rotas para o site : CADERNOS/CONSUMIDOR
|--------------------------------------------------------------------------
*/




/*
|--------------------------------------------------------------------------
| Rotas para o site : CONCURSO TO DENTRO (3ROTTOD)
|--------------------------------------------------------------------------
| Rotas que integram o funcionamento do site do concurso Tô Dentro
|
*/
Route::group(array('domain' => $dominioConcurso), function()
{
	Route::get('/', array('as' => 'concurso.home', 'uses' => 'Concurso\HomeController@index'));

	Route::get('/regulamento', function(){
		return View::make('concurso.regulamento');
	});

	Route::post('/inscrever', array('as' => 'concurso.inscricao', 'uses' => 'Concurso\HomeController@inscricao'));

	/*
	|--------------------------------------------------------------------------
	| Rotas do Painel de Administração
	|--------------------------------------------------------------------------|
	*/

	// Página Inicial do Painel
	// Se estiver logado mostra a view
	// Se não estiver redireciona para painel/login
	Route::get('painel', array('before' => 'auth', 'as' => 'concurso.painel.home', 'uses' => 'Concurso\Painel\HomeController@index'));

	// Página de Login
	Route::get('painel/login', array('as' => 'concurso.painel.login', 'uses' => 'Concurso\Painel\HomeController@login'));

	// Autenticação do Login (via POST)
	Route::post('painel/login',  array('as' => 'concurso.painel.auth', function(){
		$authvars = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'user_painel_concurso' => '1'
		);
		if(Auth::attempt($authvars)){
			return Redirect::to('painel');
		}else{
			Session::flash('login_errors', true);
			return Redirect::to('painel/login');
		}
	}));

	// Ação de logout
	Route::get('painel/logout', array('as' => 'concurso.painel.off', function(){
		Auth::logout();
		return Redirect::to('painel');
	}));

	/* Ajax de ordenação de resultados do painel */
	Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Concurso\AjaxController@gravaOrdem'));
});
/*
|--------------------------------------------------------------------------
| Fim das Rotas para o site : CADERNOS/CONSUMIDOR
|--------------------------------------------------------------------------
*/




/*
|--------------------------------------------------------------------------
| Rotas para os Controllers RESTFUL dos painéis - (4ROTPAI)
|--------------------------------------------------------------------------
*/
if(Request::server('HTTP_HOST') == $dominioRepresentantes){

	/* Seções do Painel */
	Route::group(array('domain' => $dominioRepresentantes, 'prefix' => 'painel', 'before' => 'auth'), function()
	{
		Route::get('institucional/texto', array('as' => 'painel.institucional.texto', 'uses' => 'Representantes\Painel\InstitucionalController@texto'));
		Route::put('institucional/updateTexto', array('as' => 'painel.institucional.updateTexto', 'uses' => 'Representantes\Painel\InstitucionalController@updateTexto'));

	    Route::resource('usuarios', 'Representantes\Painel\UsuariosController');
	    Route::resource('banners', 'Representantes\Painel\BannersController');
	    Route::resource('novidades', 'Representantes\Painel\NovidadesController');
	    Route::resource('tipos', 'Representantes\Painel\TiposController');
	    Route::resource('linhas', 'Representantes\Painel\LinhasController');
	    Route::resource('produtos', 'Representantes\Painel\ProdutosController');
	    Route::resource('representantes', 'Representantes\Painel\RepresentantesController');
		Route::resource('institucional', 'Representantes\Painel\InstitucionalController');
		Route::resource('certificacoes', 'Representantes\Painel\CertificacoesController');
		Route::resource('cadastros', 'Representantes\Painel\CadastrosController');
		Route::get('painel/cadastros/download', array('as' => 'painel.cadastros.download', 'uses' => 'Representantes\Painel\CadastrosController@download'));
	});

}elseif(Request::server('HTTP_HOST') == $dominioCadernos){

	/* Seções do Painel */
	Route::group(array('domain' => $dominioCadernos, 'prefix' => 'painel', 'before' => 'auth'), function()
	{
	    Route::resource('usuarios', 'Consumidor\Painel\UsuariosController');
	    Route::resource('banners', 'Consumidor\Painel\BannersController');
	    Route::resource('blog', 'Consumidor\Painel\BlogController');
	    Route::resource('categorias', 'Consumidor\Painel\BlogCategoriasController');
	    Route::resource('encontrar', 'Consumidor\Painel\EncontrarController');
	    Route::resource('encontraronline', 'Consumidor\Painel\LojasOnlineController');
	});

}elseif(Request::server('HTTP_HOST') == $dominioConcurso){

	/* Seções do Painel */
	Route::group(array('domain' => $dominioConcurso, 'prefix' => 'painel', 'before' => 'auth'), function()
	{
	    Route::resource('usuarios', 'Concurso\Painel\UsuariosController');
	    Route::get('participacoes/{etapa?}', array('as' => 'concurso.painel.participacoes', 'uses' => 'Concurso\Painel\ParticipacoesController@index'));

	    Route::get('participacoes/detalhes/{id?}', array('as' => 'concurso.painel.participacoes.detalhes', 'uses' => 'Concurso\Painel\ParticipacoesController@view'));

	    Route::get('download/{etapa?}', function($etapa = false){

	    	if($etapa){
	    		$resultados = ParticipacoesConcurso::where('etapa', '=', $etapa)->orderBy('nome_completo')
																	    		->select('etapa', 'frase', 'nome_completo', 'email', 'telefone', 'cep', 'cpf', 'nome_responsavel', 'cpf_responsavel', 'data_nascimento', 'aceite', 'newsletter', 'data_cadastro', 'ip_cadastro')
																	    		->get()
																	    		->toArray();

				$pathToFile = public_path() . '/assets/temp/participacoes_todentro_etapa_'.$etapa.'_'.date('d-m-Y-H-i-s').'.xls';
	    	}else{
	    		$resultados = ParticipacoesConcurso::orderBy('nome_completo')
	    											->select('etapa', 'frase', 'nome_completo', 'email', 'telefone', 'cep', 'cpf', 'nome_responsavel', 'cpf_responsavel', 'data_nascimento', 'aceite', 'newsletter', 'data_cadastro', 'ip_cadastro')
										    		->get()
										    		->toArray();

	    		$pathToFile = public_path() . '/assets/temp/participacoes_todentro_todas_as_etapas_'.date('d-m-Y-H-i-s').'.xls';
	    	}

	    	$titulos = array(
				'etapa' => 'Etapa',
	            'frase' => 'Frase',
	            'nome_completo' => 'Nome Completo',
	            'email' => 'E-mail',
	            'telefone' => 'Telefone',
	            'cep' => 'CEP',
	            'cpf' => 'CPF',
	            'nome_responsavel' => 'Nome do Responsável',
	            'cpf_responsavel' => 'CPF do Responsável',
	            'data_nascimento' => 'Data de Nascimento',
	            'aceite' => 'Aceite dos termos',
	            'newsletter' => 'Receber Newsletter',
	            'data_cadastro' => 'Data de Cadastro',
	            'ip_cadastro' => 'IP de cadastro',
		    );
	    	array_unshift($resultados, $titulos);
    		Excel::fromArray($resultados)->save($pathToFile);
			return Response::download($pathToFile);
	    });
	});
}
/*
|--------------------------------------------------------------------------
| Fim das Rotas para os Controller RESTFUL
|--------------------------------------------------------------------------
*/
