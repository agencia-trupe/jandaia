<?php

class ProdutosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('produtos')->delete();
        $produtos = [
            [
                'titulo' => 'Caderno Plus Espiral',
                'slug' => 'caderno-plus-espiral',
                'imagem' => 'capa1.jpg',
                'detalhes' => '<p>Ac, adipiscing vut vel pulvinar, rhoncus non elementum, turpis?</p>',
                'ordem' => '0',
                'miolo' => '0',
                'produtos_tipo_id' => '0',
                'produtos_linha_id' => '1',
				'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Caderno Plus Capa Dura',
                'slug' => 'caderno-plus-capa-dura',
                'imagem' => 'capa2.jpg',
                'detalhes' => '<p>Ac, adipiscing vut vel pulvinar, rhoncus non elementum, turpis?</p>',
                'ordem' => '1',
                'miolo' => '0',
                'produtos_tipo_id' => '1',
                'produtos_linha_id' => '1',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Caderno Plus Espiral Miolo',
                'slug' => 'caderno-plus-espiral-miolo',
                'imagem' => 'miolo1.jpg',
                'detalhes' => '<p>Ac, adipiscing vut vel pulvinar, rhoncus non elementum, turpis?</p>',
                'ordem' => '0',
                'miolo' => '1',
                'produtos_tipo_id' => '2',
                'produtos_linha_id' => '1',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Caderno Plus Capa Dura Miolo',
                'slug' => 'caderno-plus-capa-dura-miolo',
                'imagem' => 'miolo2.jpg',
                'detalhes' => '<p>Ac, adipiscing vut vel pulvinar, rhoncus non elementum, turpis?</p>',
                'ordem' => '1',
                'miolo' => '1',
                'produtos_tipo_id' => '3',
                'produtos_linha_id' => '2',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ]
        ];

        DB::table('produtos')->insert($produtos);
    }

}