<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('BannersTableSeeder');
		// $this->call('NovidadesTableSeeder');
		// $this->call('ProdutosLinhasTableSeeder');
		// $this->call('ProdutosTableSeeder');
		// $this->call('ProdutosTiposTableSeeder');
		// $this->call('UsersTableSeeder');
		// $this->call('UsersConcursoTableSeeder');
		// $this->call('RepresentantesEstadosTableSeeder');
		// $this->call('RepresentantesCidadesTableSeeder');
		// $this->call('RepresentantesTableSeeder');
		$this->call('InstitucionalTableSeeder');
	}

}