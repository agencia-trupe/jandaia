<?php

class InstitucionalTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'titulo' => 'Histórico',
            	'slug' => 'historico',
            	'texto' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vero repellat impedit esse animi molestias cum a consectetur! Velit, voluptatem voluptate provident minima eius perferendis architecto vero hic ad consequatur!',
            	'imagem' => 'imagem.jpg'
            ],
            [
            	'titulo' => 'Sustentabilidade',
            	'slug' => 'sustentabilidade',
            	'texto' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vero repellat impedit esse animi molestias cum a consectetur! Velit, voluptatem voluptate provident minima eius perferendis architecto vero hic ad consequatur!',
            	'imagem' => 'imagem.jpg'
            ],
            [
            	'titulo' => 'Certificações',
            	'slug' => 'certificacoes',
            	'texto' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vero repellat impedit esse animi molestias cum a consectetur! Velit, voluptatem voluptate provident minima eius perferendis architecto vero hic ad consequatur!',
            	'imagem' => 'imagem.jpg'
            ],
            [
            	'titulo' => 'Qualidade Jandaia',
            	'slug' => 'qualidade-jandaia',
            	'texto' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vero repellat impedit esse animi molestias cum a consectetur! Velit, voluptatem voluptate provident minima eius perferendis architecto vero hic ad consequatur!',
            	'imagem' => 'imagem.jpg'
            ],
            [
            	'titulo' => 'Produtos Personalizados',
            	'slug' => 'produtos-personalizados',
            	'texto' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vero repellat impedit esse animi molestias cum a consectetur! Velit, voluptatem voluptate provident minima eius perferendis architecto vero hic ad consequatur!',
            	'imagem' => 'imagem.jpg'
            ],
            [
            	'titulo' => 'Exportação',
            	'slug' => 'exportacao',
            	'texto' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vero repellat impedit esse animi molestias cum a consectetur! Velit, voluptatem voluptate provident minima eius perferendis architecto vero hic ad consequatur!',
            	'imagem' => 'imagem.jpg'
            ],            
        ];

        DB::table('institucional')->insert($data);
    }

}