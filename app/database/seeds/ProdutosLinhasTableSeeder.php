<?php

class ProdutosLinhasTableSeeder extends Seeder {

    public function run()
    {
        DB::table('produtos_linhas')->delete();
        $linhas = [
            [
            	'titulo' => 'Alice',
                'slug' => 'alice',
                'detalhe_titulo' => 'Alice 2014',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Coca Cola',
                'slug' => 'coca-cola',
                'detalhe_titulo' => 'Coca Cola 2014',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Ed Hardy',
                'slug' => 'ed-hardy',
                'detalhe_titulo' => 'Ed Hardy 2014',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Naruto',
                'slug' => 'naruto',
                'detalhe_titulo' => 'Naruto 2014',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ]
        ];

        DB::table('produtos_linhas')->insert($linhas);
    }

}