<?php

class ProdutosTiposTableSeeder extends Seeder {

    public function run()
    {
        DB::table('produtos_tipos')->delete();
        $tipos = [
            [
            	'titulo' => 'Cadernos',
                'slug' => 'cadernos',
                'cor' => '#DC0045',
                'imagem' => 'tipos_cadernos.jpg',
                'ordem' => '0',
				'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Agendas',
                'slug' => 'agendas',
                'cor' => '#BA2B00',
                'imagem' => 'tipos_agendas.jpg',
                'ordem' => '1',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Gifts',
                'slug' => 'gifts',
                'cor' => '#FF9941',
                'imagem' => 'tipos_gifts.jpg',
                'ordem' => '2',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Fichários',
                'slug' => 'ficharios',
                'cor' => '#2CB072',
                'imagem' => 'tipos_ficharios.jpg',
                'ordem' => '3',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Bolsas',
                'slug' => 'bolsas',
                'cor' => '#0096DC',
                'imagem' => 'tipos_bolsas.jpg',
                'ordem' => '4',
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ]
        ];

        DB::table('produtos_tipos')->insert($tipos);
    }

}