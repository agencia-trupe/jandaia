<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInstitucionalTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('institucional', function(Blueprint $table)
		{
			$table->string('imagem_maior')->nullable()->after('imagem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('institucional', function(Blueprint $table)
		{
			$table->dropColumn('imagem_maior');
		});
	}

}