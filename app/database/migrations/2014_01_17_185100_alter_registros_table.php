<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRegistrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('onde_encontrar_registros', function(Blueprint $table)
		{
			$table->integer('id_listas')->default(0)->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('onde_encontrar_registros', function(Blueprint $table)
		{
			$table->dropColumn('id_listas');
		});
	}

}