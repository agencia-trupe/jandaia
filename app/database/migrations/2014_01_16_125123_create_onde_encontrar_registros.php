<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOndeEncontrarRegistros extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('onde_encontrar_registros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('item');
			$table->string('descricao');
			$table->string('tipo');
			$table->string('linha');
			$table->string('razao_social');
			$table->string('endereco');
			$table->string('bairro');
			$table->string('cep');
			$table->string('cidade');
			$table->string('uf');
			$table->string('estado');
			$table->string('telefone');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('onde_encontrar_registros');
	}

}
