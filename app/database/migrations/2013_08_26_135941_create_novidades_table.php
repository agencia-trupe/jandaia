<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novidades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('slug')->unique();
			$table->string('imagem');
			$table->text('olho');
			$table->text('texto');
			$table->date('data');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novidades');
	}

}
