<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInstitucionalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('institucional', function(Blueprint $table)
		{
			$table->text('texto_extra')->nullable()->after('texto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('institucional', function(Blueprint $table)
		{
			$table->dropColumn('texto_extra');
		});
	}

}