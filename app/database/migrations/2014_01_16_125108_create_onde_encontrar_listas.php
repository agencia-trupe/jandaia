<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOndeEncontrarListas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('onde_encontrar_listas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('descritivo');
			$table->string('arquivo');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('onde_encontrar_listas');
	}

}
